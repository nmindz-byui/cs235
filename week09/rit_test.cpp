/***********************************************************************
* Program:
*    Assignment 09, Binary Search Trees
*    Brother Kirby, CS 235
* Author:
*    Evandro Camargo, Alexandre Paixao & Caleb Georgeson
* Summary: 
*    This is a test driver program to test iterator/reverse_iterators
*    which were implemented as part of the Week 09 assignment.
************************************************************************/

#include <iostream>      // for CIN and COUT
#include <string>        // for STRING
#include <cassert>       // for ASSERTS
#include "bst.h"         // for BST class which should be in bst.h
using namespace std;
using namespace custom;

int main()
{
   try
   {
      BST <char> tree;
      BST <char> :: iterator it;
      BST <char> :: reverse_iterator rit;

      // Fill the tree
      cout << "Fill the tree with: G F J A H N E I L O C K M B D P\n";
      tree.insert('G');
      tree.insert('F'); //                       G
      tree.insert('J'); //          +------------+------------+
      tree.insert('A'); //          F                         J
      tree.insert('H'); //   +------+                  +------+------+
      tree.insert('N'); //   A                         H             N
      tree.insert('E'); //   +---+                     +---+     +---+---+
      tree.insert('I'); //       E                         I     L       O
      tree.insert('L'); //    +--+                            +--+--+    +-+
      tree.insert('O'); //    C                               K     M      P
      tree.insert('C'); //  +-+-+                             
      tree.insert('K'); //  B   D                               
      tree.insert('M');
      tree.insert('B');
      tree.insert('D');
      tree.insert('P');

      // Normal Iterator
      it = tree.begin();
      // Reverse Iterator
      rit = tree.ribegin();

      cout << "Using the <iterator> class: \n";

      for (it; it != tree.end(); ++it)
      {
         cout << "\tPrinting Node '" << *it << "'\n";
      }

      cout << "Using the <reverse_iterator> class: \n";

      for (rit; rit != tree.riend(); ++rit)
      {
         cout << "\tPrinting Node '" << *rit << "'\n";
      }
   }
   catch (const char * s)
   {
      cout << "Thrown exception: " << s << endl;
   }
}

/******************************************
 * DISPLAY A TREE
 * Display the contents of a BST using an iterator
 ******************************************/
template <class T>
ostream & operator << (ostream & out, BST <T> & rhs)
{
   out << '{';
   typename BST <T> :: iterator it;
   for (it = rhs.begin(); it != rhs.end(); it++)
      out << "  " << *it;
   out << "  }";
   return out;
}