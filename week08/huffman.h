/***********************************************************************
 * Module:
 *    Week 08, Huffman
 *    Brother Helfrich, CS 235
 * Author:
 *    Br. Helfrich
 * Summary:
 *    This program will implement the huffman() function
 ************************************************************************/

#ifndef HUFFMAN_H
#define HUFFMAN_H // HUFFMAN_H

#include "bnode.h"         // for BINARY_NODE class definition
#include "vector.h"        // for VECTOR container
#include "pair.h"          // for PAIR container
#include "node.h"          // for NODE class definition
#include "list.h"          // for LIST class definition
#include <string>

using namespace std;

// Driver
void huffman(const std::string & fileName);

// Create a new branch of the tree
void addBranch(BNode<custom::pair<string,float>> *Tree, BNode<custom::pair<string,float>> branch) ;

// Place the node in the correct place in the list
void includeListOnSort(custom::list<BNode<custom::pair<string,float>>> &lTree, BNode<custom::pair<string,float>> data);

// Produce the binary huffman code from a BNode tree
void getBinary(BNode<custom::pair<string,float>> * pHead, string value, string find, string &p);

// Creates a BNode tree used to generate Huffman codes
BNode<custom::pair<string,float>> generateTree(custom::list<BNode<custom::pair<string,float>>> &lTree);

#endif // HUFFMAN_H