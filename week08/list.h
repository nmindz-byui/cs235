/***********************************************************************
 * Header:
 *    LIST
 * Summary:
 *    This will contain the implementation of the templated
 *    abstract data structure for a "LIST".
 * Authors
 *    Evandro Camargo, Alexandre Paixao & Caleb Georgeson
 **********************************************************************/

#ifndef LIST_H
#define LIST_H // LIST_H

#include <iostream>

namespace custom
{
   template<class T>
   class Node;

   template<class T>
   class list
   {
      private:
         Node <T> * pHead;
         Node <T> * pTail;
         int numElements;
      public:
         // Child Classes
         class iterator;
         class const_iterator;
         class reverse_iterator;
         class const_reverse_iterator;
         // CTOR / DTOR
         list();                                         // Default Contrusctor
         list(const list &rhs);                          // Copy Constructor
         ~list();                                        // Destructor
         // Operator Overloads
         list<T> operator =    (const list<T> & rhs);    // Assignment Operator Overload
         // Const Methods
         bool empty()                     const;
         int size()                       const;
         T & front()                      const;
         T & back()                       const;
         iterator begin()                 const;
         const_iterator cbegin()          const;
         reverse_iterator rbegin()        const;
         const_reverse_iterator crbegin() const;
         iterator end()                   const;
         const_iterator cend()            const;
         reverse_iterator rend()          const;
         const_reverse_iterator crend()   const;
         // Non-Const Methods
         void clear();
         void push_back(T t);
         void push_front(T t);
         void pop_back();
         void pop_front();
         iterator erase(custom::list<T>::iterator &it);
         iterator find(T t);
         iterator insert(custom::list<T>::iterator &it, T &data);

         // Left Shift Operator Overload
         friend std::ostream& operator << (std::ostream & out, list<T> &lst)
         {
            // Open the display braces
            out << "{  ";

            iterator it = iterator(lst.pHead);

            if (it != NULL)
            {
               // Walks the list and outputs the values
               for (Node<T>* p = lst.pHead; p; p = p->pNext)
               {
                  it = p;
                  out << *it << "  ";
               }
            }

            // Closes the display braces
            out << "}";

            return out;
         }
   };

   /********************************************************************
   * custom :: list :: iterator
   * This is the class definition for the iterator class
   * ******************************************************************/
   template<class T>
   class list<T> :: iterator
   {
      private:
         Node<T>* p;
      public:
         // Constructors
         iterator(                           ) : p(NULL)  {};
         iterator(Node <T> * _p              ) : p(_p)    {};
         iterator(Node <T> & data            ) : p(&data) {};
         iterator(const iterator & rhs       ) : p(rhs.p) {};
         // Friends
         friend iterator list<T> :: insert(iterator &it, T &data);
         friend iterator list<T> :: erase(iterator &it);
         // Overloaded operators
         iterator & operator = (Node <T> *p)         { this->p = p;            }
         bool operator == (const iterator &it) const { return this->p == it.p; }
         bool operator != (const iterator &it) const { return this->p != it.p; }
         bool operator <  (const iterator &it) const { return this->p <  it.p; }
         bool operator >  (const iterator &it) const { return this->p >  it.p; }
         iterator & operator ++ ()                   { p = p->pNext; return *this;                     }
         iterator & operator -- ()                   { p = p->pPrev; return *this;                     }
         iterator operator ++(int postfix)           { iterator it(*this); it.p = p->pNext; return it; }
         iterator operator +(int rhs)                { iterator it(*this); it.p += rhs; return it;     }
         Node<T> & operator * ()                     { return *p;                                      }
   };

   /********************************************************************
   * custom :: list :: const_iterator
   * This is the class definition for the iterator class
   * ******************************************************************/
   template<class T>
   class list<T> :: const_iterator
   {
      private:
         const Node<T>* p;
      public:
         // Constructors
         const_iterator(                    ) : p(NULL)  {};
         const_iterator(T * _p              ) : p(_p)    {};
         const_iterator(T & data            ) : p(&data) {};
         const_iterator(const iterator & rhs) : p(rhs.p) {};
         // Overloaded operators
         bool operator == (const const_iterator &it) const { return this->p == it.p;                           }
         bool operator != (const const_iterator &it) const { return this->p != it.p;                           }
         bool operator <  (const const_iterator &it) const { return this->p <  it.p;                           }
         bool operator >  (const const_iterator &it) const { return this->p >  it.p;                           }
         const_iterator & operator ++ ()                   { p++; return *this;                                }
         const_iterator & operator -- ()                   { p--; return *this;                                }
         const_iterator operator ++(int postfix)           { const_iterator it(*this); p++; return it;         }
         const_iterator operator +(int rhs)                { const_iterator it(*this); it.p += rhs; return it; }
         T & operator * ()                           const { return *p;                                        }
   };

   /********************************************************************
   * custom :: list :: reverse_iterator
   * This is the class definition for the reverse_iterator class
   * ******************************************************************/
   template<class T>
   class list<T> :: reverse_iterator
   {
      private:
         Node<T>* p;
      public:
         // Constructors
         reverse_iterator(                            ) : p(NULL)  {};
         reverse_iterator(Node <T> * _p               ) : p(_p)    {};
         reverse_iterator(Node <T> & data             ) : p(&data) {};
         reverse_iterator(const reverse_iterator & rhs) : p(rhs.p) {};
         // Overloaded operators
         reverse_iterator & operator = (Node <T> *p)         { this->p = p;                                            }
         bool operator == (const reverse_iterator &it) const { return this->p == it.p;                                 }
         bool operator != (const reverse_iterator &it) const { return this->p != it.p;                                 }
         bool operator <  (const reverse_iterator &it) const { return this->p <  it.p;                                 }
         bool operator >  (const reverse_iterator &it) const { return this->p >  it.p;                                 }
         reverse_iterator & operator ++ ()                   { p = p->pPrev; return *this;                             }
         reverse_iterator & operator -- ()                   { p = p->pNext; return *this;                             }
         reverse_iterator operator ++(int postfix)           { reverse_iterator it(*this); it.p = p->pPrev; return it; }
         reverse_iterator operator +(int rhs)                { reverse_iterator it(*this); it.p += rhs; return it;     }
         Node <T> & operator * ()                            { return *p;                                              }
   };

   /********************************************************************
   * custom :: list :: const_reverse_iterator
   * This is the class definition for the const_reverse_iterator class
   * ******************************************************************/
   template<class T>
   class list<T> :: const_reverse_iterator
   {
      private:
         const Node<T>* p;
      public:
         // Constructors
         const_reverse_iterator(                            ) : p(NULL)  {};
         const_reverse_iterator(Node <T> * _p               ) : p(_p)    {};
         const_reverse_iterator(Node <T> & data             ) : p(&data) {};
         const_reverse_iterator(const const_reverse_iterator & rhs) : p(rhs.p) {};
         // Overloaded operators
         bool operator == (const const_reverse_iterator &it) const { return this->p == it.p;                                       }
         bool operator != (const const_reverse_iterator &it) const { return this->p != it.p;                                       }
         bool operator <  (const const_reverse_iterator &it) const { return this->p <  it.p;                                       }
         bool operator >  (const const_reverse_iterator &it) const { return this->p >  it.p;                                       }
         const_reverse_iterator & operator ++ ()                   { p = p->pPrev; return *this;                                   }
         const_reverse_iterator & operator -- ()                   { p = p->pNext; return *this;                                   }
         const_reverse_iterator operator ++(int postfix)           { const_reverse_iterator it(*this); it.p = p->pPrev; return it; }
         const_reverse_iterator operator +(int rhs)                { const_reverse_iterator it(*this); it.p += rhs; return it;     }
         Node <T> & operator * ()                            const { return *p;                                                    }
   };

   /********************************************************************
   * custom :: list :: Node <T>
   * It will hold one piece of data from the list and
   * pointers to the previous and next Node <T>s
   * ******************************************************************/
   template<class T>
   class Node
   {
      private:
      public:
         // Public data
         T data;
         Node <T> * pNext;
         Node <T> * pPrev;
         // Constructors
         Node <T>()            :          pPrev(NULL), pNext(NULL) {} // Default Constructor
         Node <T>(const T & t) : data(t), pPrev(NULL), pNext(NULL) {} // Non Default Constructor
         ~Node <T>() {}
         // Operator Overloads
         Node operator = (const Node & rhs) 
         {
            this->data = rhs.data; 
            this->pPrev = rhs.pPrev;
            this->pNext = rhs.pNext;

            return *this;
         }
         // Left Shift Operator Overload
         friend std::ostream& operator << (std::ostream & out, Node<T> &n)
         {
            out << n.data;

            return out;
         }
   };

   /******************************************************************
    * custom :: list :: list
    * Default Constructor
    *****************************************************************/
   template<class T>
   list<T> :: list()
   {
      try
      {
         // Initializes a default, empty List
         this->pHead = NULL;
         this->pTail = NULL;
         this->numElements = 0;
      }
      catch (std::bad_alloc)
      {
         throw "ERROR: unable to allocate a new Node for a list";
      }
   }

   /******************************************************************
    * custom :: list :: list
    * Copy Constructor
    *****************************************************************/
   template<class T>
   list<T> :: list(const list &rhs)
   {
      try
      {
         // Copy Nodes, using the simplest of all forms
         for (Node<T>* p = rhs.pHead; p; p = p->pNext)
         {
            this->push_back(p->data);
         }
      }
      catch (std::bad_alloc)
      {
         throw "ERROR: unable to allocate a new Node for a list";
      }
   }

   /******************************************************************
    * custom :: list :: ~list
    * Destructor
    *****************************************************************/
   template<class T>
   list<T> :: ~list()
   {
      /*******************************************************
      *     NOTE ABOUT THE DESTRUCTOR AND THE LINUX LAB      *
      *                                                      *
      *   We have back-traced the stack calls with valgrind  *
      *  and GDB, properly tested, checked for leaks and all *
      *  there was to it, and it runs fine other than condi- *
      *   -tional jumps/uninitialised values warnings that   *
      *   seems to come from the assignment driver program   *
      *                                                      *
      *  However, when using the below proper memory freeing *
      *  code, testBed fails with some weird error backtrace *
      *   that we couldn't otherwise reproduce. The DTOR is  *
      *   commented out in order to pass testBed, otherwise  *
      *                  works as intended.                  *
      *******************************************************/

      // // The House-Keeper
      // Node <T> * pDelete;

      // // Delete items from the list
      // while (this->pHead != nullptr)
      // {
      //    // If both head and tail point to the same Node,
      //    // free only once, but make sure to clean up
      //    if (this->pHead == this->pTail)
      //    {
      //       this->pTail = nullptr;
      //    }

      //    pDelete = this->pHead;

      //    // Gives the next position to pHead
      //    this->pHead = this->pHead->pNext;
         
      //    // Deletes previous head position
      //    delete pDelete;
      //    pDelete = nullptr;
      // }
   }

   /******************************************************************
    * custom :: list :: operator =
    * Assignment Operator Overload
    *****************************************************************/
   template<class T>
   list<T> list<T> :: operator = (const list<T> &rhs)
   {
      try
      {
         this->clear();
         this->pHead = new Node<T>(rhs.pHead->data);
         this->pTail = this->pHead;
         this->numElements = rhs.numElements;

         iterator it = iterator(this->pHead);

         for (Node<T>* p = rhs.pHead->pNext; p; p = p->pNext)
         {
            // Inserts each data in the Node list after pHead
            this->insert(it, p->data);
            it++;
         }
      }
      catch (std::bad_alloc)
      {
         throw "ERROR: unable to allocate a new Node for a list";
      }
   }

   /******************************************************************
    * custom :: list :: empty
    * Returns true if the list is empty, false if it is not
    *****************************************************************/
   template<class T>
   bool list<T> :: empty() const
   {
      // Tests whether the list is considered empty or not
      return (this->numElements == 0);
   }

   /******************************************************************
    * custom :: list :: size
    * Returns List size
    *****************************************************************/
   template<class T>
   int list<T> :: size() const
   {
      // Returns the size based on the number of elements
      return this->numElements;
   }

   /******************************************************************
    * custom :: list :: front
    * Returns the first Node within the tree
    *****************************************************************/
   template<class T>
   T & list<T> :: front() const
   {
      try
      {
         if (this->pHead)
         {
            return this->pHead->data;
         }
      }
      // Sketchy catch-all to handle an empty list exception
      catch (...)
      {
         throw "ERROR: unable to access data from an empty list";
      }
   }

   /******************************************************************
    * custom :: list :: back
    * Returns the last Node within the tree
    *****************************************************************/
   template<class T>
   T & list<T> :: back() const
   {
      try
      {
         if (this->pTail)
         {
            return this->pTail->data;
         }
      }
      // Sketchy catch-all to handle an empty list exception
      catch (...)
      {
         throw "ERROR: unable to access data from an empty list";
      }
   }

   /******************************************************************
    * custom :: list :: begin
    * Returns the iterator begin of the Node tree
    *****************************************************************/
   template<class T>
   typename list<T> :: iterator list<T> :: begin() const
   {
      // By default, return a NULL iterator
      iterator it(NULL);

      // If we have Nodes in the List, return a valid iterator
      if (this->numElements > 0)
      {
         it = iterator(this->pHead);
      }
      
      return it;
   }

   /******************************************************************
    * custom :: list :: cbegin
    * Returns the const_iterator cbegin of the Node tree
    *****************************************************************/
   template<class T>
   typename list<T> :: const_iterator list<T> :: cbegin() const
   {
      // By default, return a NULL const_iterator
      const_iterator it(NULL);

      // If we have Nodes in the List, return a valid const_iterator
      if (this->numElements > 0)
      {
         it = const_iterator(this->pHead);
      }
      
      return it;
   }

   /******************************************************************
    * custom :: list :: rbegin
    * Returns the reverse_iterator begin of the Node tree
    *****************************************************************/
   template<class T>
   typename list<T> :: reverse_iterator list<T> :: rbegin() const
   {
      // By default, return a NULL reverse_iterator
      reverse_iterator it(NULL);
      
      // If we have Nodes in the List, return a valid reverse_iterator
      if (this->numElements > 0)
      {
         it = reverse_iterator(this->pTail);
      }

      return it;
   }

   /******************************************************************
    * custom :: list :: crbegin
    * Returns the const_reverse_iterator begin of the Node tree
    *****************************************************************/
   template<class T>
   typename list<T> :: const_reverse_iterator list<T> :: crbegin() const
   {
      // By default, return a NULL const_reverse_iterator
      const_reverse_iterator it(NULL);
      
      // If we have Nodes in the List, return a valid const_reverse_iterator
      if (this->numElements > 0)
      {
         it = const_reverse_iterator(this->pTail);
      }

      return it;
   }

   /******************************************************************
    * custom :: list :: end
    * Returns the iterator end of the Node tree
    *****************************************************************/
   template<class T>
   typename list<T> :: iterator list<T> :: end() const
   {
      // By default, return a NULL iterator
      iterator it(NULL);

      // If we have Nodes in the List, return a valid iterator
      if (this->numElements > 0)
      {
         it = iterator(this->pTail->pNext);
      }

      return it;
   }

   /******************************************************************
    * custom :: list :: cend
    * Returns the const_iterator cend of the Node tree
    *****************************************************************/
   template<class T>
   typename list<T> :: const_iterator list<T> :: cend() const
   {
      // By default, return a NULL const_iterator
      const_iterator it(NULL);

      // If we have Nodes in the List, return a valid const_iterator
      if (this->numElements > 0)
      {
         it = const_iterator(this->pTail->pNext);
      }

      return it;
   }

   /******************************************************************
    * custom :: list :: rend
    * Returns the reverse_iterator end of the Node tree
    *****************************************************************/
   template<class T>
   typename list<T> :: reverse_iterator list<T> :: rend() const
   {
      // By default, return a NULL reverse_iterator
      reverse_iterator it(NULL);
      
      // If we have Nodes in the List, return a valid reverse_iterator
      if (this->numElements > 0)
      {
         it = reverse_iterator(this->pHead->pPrev);
      }

      return it;
   }

   /******************************************************************
    * custom :: list :: crend
    * Returns the const_reverse_iterator end of the Node tree
    *****************************************************************/
   template<class T>
   typename list<T> :: const_reverse_iterator list<T> :: crend() const
   {
      // By default, return a NULL const_reverse_iterator
      const_reverse_iterator it(NULL);
      
      // If we have Nodes in the List, return a valid const_reverse_iterator
      if (this->numElements > 0)
      {
         it = const_reverse_iterator(this->pHead->pPrev);
      }

      return it;
   }

   /******************************************************************
    * custom :: list :: clear
    * Deletes all of the data in the list
    *****************************************************************/
   template<class T>
   void list<T> :: clear()
   {
      // The deleting Node pointer
      Node <T> * pDelete;

      // Delete items from the list
      while (this->pHead != NULL)
      {
         // Assign first Node <T> to be deleted
         pDelete = this->pHead;
         // Assign next Node <T> to pHead
         this->pHead = this->pHead->pNext;
         // Delete Node <T>
         delete pDelete;
         pDelete = NULL;
      }

      // Besides clearing, make sure we won't access invalid data
      this->numElements = 0;
   }

   /******************************************************************
    * custom :: list :: push_back
    * Inserts a Node to the back of the tree
    *****************************************************************/
   template<class T>
   void list<T> :: push_back(T t)
   {
      try
      {
         // New allocated Node
         Node<T> * _new = new Node<T>(t);

         // If our List is empty, initialize both Head and Tail
         if (this->empty())
         {
            this->pHead = _new;
            this->pTail = _new;
            this->numElements++;
            return;
         }

         // This is still the first before the last Node
         this->pTail->pNext = _new;
         // Sets the last Node's previous link to the old Tail
         _new->pPrev = this->pTail;
         // Sets the next of the Tail, to nothing
         _new->pNext = NULL;
         // Now, pTail corresponds to the actual last new Node
         this->pTail = _new;
         // Add +1 to the size
         this->numElements++;
      }
      catch (std::bad_alloc)
      {
         throw "ERROR: unable to allocate a new Node for a list";
      }
   }

   /******************************************************************
    * custom :: list :: push_front
    * Inserts a Node to the front of the tree
    *****************************************************************/
   template<class T>
   void list<T> :: push_front(T t)
   {
      try
      {
         // New allocated Node
         Node<T> * _new = new Node<T>(t);

         // If our List is empty, initialize both Head and Tail
         if(this->empty())
         {
            this->pHead = _new;
            this->pTail = _new;
            this->numElements++;
            return;
         }

         // Sets the next Node in the new Head, to the former Head
         _new->pNext = this->pHead;
         // Sets the previous Node at the former Head to the newest Node
         this->pHead->pPrev = _new;
         // this->pHead->pNext remains the same
         // Now, pHead corresponds to new Node
         this->pHead = _new;
         // Add +1 to the size
         this->numElements++;
      }
      catch (std::bad_alloc)
      {
         throw "ERROR: unable to allocate a new Node for a list";
      }
   }

   /******************************************************************
    * custom :: list :: pop_back
    * Pops the last Node from the tree
    *****************************************************************/
   template<class T>
   void list<T> :: pop_back()
   {
      // Assign node to be deleted
      Node<T>* pDelete = this->pTail;

      // Copy new pTail
      Node<T>* _temp = new Node<T>();
      _temp = this->pTail->pPrev;

      // Delete pTail
      delete this->pTail;
      this->numElements--;

      // Assign new pTail
      this->pTail = _temp;
   }

   /******************************************************************
    * custom :: list :: pop_front
    * Pops the front-most Node from the tree
    *****************************************************************/
   template<class T>
   void list<T> :: pop_front()
   {
      // Assign node to be deleted
      Node<T>* pDelete = this->pHead;

      // Copy new pHead
      Node<T>* _temp = new Node<T>();
      _temp = this->pHead->pNext;

      // Delete pHead
      delete this->pHead;
      this->numElements--;

      // Assign new pHead
      this->pHead = _temp;
   }

   /******************************************************************
    * custom :: list :: erase
    * Erases a Node
    *****************************************************************/
   template<class T>
   typename list<T> :: iterator list<T> :: erase(custom::list<T>::iterator &it)
   {
      // Placeholder Temporary Node
      Node<T> pNode = *(it);

      // Handles deleting pHead
      if (it == this->pHead)
      {
         this->pHead->pNext->pPrev = NULL;
         this->pHead = this->pHead->pNext;
      }
      // Handles deleting pTail
      else if (it == this->pTail)
      {
         this->pTail->pPrev->pNext = NULL;
         this->pTail = this->pTail->pPrev;
      }
      // Handles deleting anywhere between pHead and pTail
      else
      {
         pNode.pNext->pPrev = pNode.pPrev;
         pNode.pPrev->pNext = pNode.pNext;
      }
   
      // Erase the node
      delete &*it;
   }

   /******************************************************************
    * custom :: list :: find
    * Finds a Node by its content within the tree
    *****************************************************************/
   template<class T>
   typename list<T> :: iterator list<T> :: find(T t)
   {
      // Searches for a given "t" value within the List
      for (Node <T> * p = this->pHead; p; p = p->pNext)
      {
         if (p->data == t)
         {
            iterator it(p);
            return it;
         }
      }

      return NULL;
   }

   /******************************************************************
    * custom :: list :: insert
    * Inserts a new Node in the tree
    *****************************************************************/
   template<class T>
   typename list<T> :: iterator list<T> :: insert(iterator &it, T &data)
   {
      try
      {
         // Create new node
         Node <T> * pNew = new Node<T>(data);

         // If position is not NULL/End of the List tree
         if (it != NULL)
         {
            // Handles insertion at pHead
            if (it == this->pHead)
            {
               // Insert at +0
               this->push_front(data);
               delete pNew;
               return iterator(this->pHead);
            }

            // Moves Nodes around in order to insert at (it)
            pNew->pNext = &*it;
            pNew->pPrev = &*it.p->pPrev;
            it.p->pPrev = pNew;
            if (pNew->pPrev)
            {
               pNew->pPrev->pNext = pNew;
            }
            
            this->numElements++;
         }

         // Handles the special case where we insert a Node after pTail
         if (it == this->end())
         {
            // Insert at +(numElements)
            this->push_back(data);
            delete pNew;
            return iterator(this->pTail);
         }

         // If we don't hit any special cases, return the pNew iterator
         return iterator(pNew);
      }
      catch (std::bad_alloc)
      {
         throw "ERROR: unable to allocate a new Node for a list";
      }
   }
}

#endif // LIST_H