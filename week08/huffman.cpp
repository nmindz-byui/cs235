/***********************************************************************
 * Module:
 *    Week 08, Huffman
 *    Brother Helfrich, CS 235
 * Author:
 *    Evandro Camargo, Alexandre Paixao & Caleb Georgeson
 * Summary:
 *    This program will implement the huffman() function
 ************************************************************************/

#include <iostream>        // for CIN and COUT
#include <fstream>         // for IFSTREAM
#include <cassert>         // for ASSERT
#include <string>          // for STRING: binary representation of codes
#include "bnode.h"         // for BINARY_NODE class definition
#include "vector.h"        // for VECTOR container
#include "pair.h"          // for PAIR container
#include "huffman.h"       // for HUFFMAN() prototype
#include "node.h"          // for NODE class definition
#include "list.h"          // for LIST class definition

using std::cout;
using std::cin;
using std::ifstream;
using std::endl;
using std::string;
using std::bad_alloc;
using namespace custom;

/*******************************************
 * addBranch()
 * This function will add a node to the tree in the correct place
 *******************************************/
void addBranch(BNode<custom::pair<string,float>> *Tree, BNode<custom::pair<string,float>> branch) 
{
   if (branch.pLeft)
   {
      custom::pair<string,float> p(branch.pLeft->data.getFirst(),branch.pLeft->data.getSecond());
      BNode<custom::pair<string,float>> * b = new BNode<custom::pair<string,float>>(p);
      b->pLeft = branch.pLeft->pLeft;
      b->pRight = branch.pLeft->pRight;

      addLeft(Tree,b);

      addBranch(Tree->pLeft,*b);
   }
   if (branch.pRight)
   {
      custom::pair<string,float> p(branch.pRight->data.getFirst(),branch.pRight->data.getSecond());
      BNode<custom::pair<string,float>> * b = new BNode<custom::pair<string,float>>(p);
      addRight(Tree,b);
      b->pLeft = branch.pRight->pLeft;
      b->pRight = branch.pRight->pRight;

      addBranch(Tree->pRight,*b);
   }
}

/*******************************************
 * includeListOnSort()
 * Places the BNode in the correct place in the list
 * (List order goes from smallest to largest)
 *******************************************/
void includeListOnSort(custom::list<BNode<custom::pair<string,float>>> &lTree, BNode<custom::pair<string,float>> data)
{

   // Aux for sorted
   bool isSorted = false;

   // Find where the BNode goes in the list and place it
   list <BNode<custom::pair<string,float>>> :: iterator it;
   for (it = lTree.begin(); it != lTree.end(); ++it)
   {
      if ((*it).data.data.getSecond() >= data.data.getSecond())
      {  
         if ((*it).data.data.getSecond() == data.data.getSecond() 
            && ((*it).data.data.getFirst() != "" && data.data.getFirst() == "")
            && (data.pRight->pRight)
            ) 
         {
            list <BNode<custom::pair<string,float>>> :: iterator itTemp;
            itTemp = it;
            lTree.insert(itTemp, data);
            isSorted = true;
            break;
         }
         else
         {
            if ((*it).data.data.getSecond() > data.data.getSecond())
            {
               // Verify if is First
               if (!(*it).pPrev) {
                  lTree.push_front(data);
                  isSorted = true;
                  break;
               }
               
               list <BNode<custom::pair<string,float>>> :: iterator itTemp;
               itTemp = it;
               lTree.insert(itTemp, data);
               isSorted = true;
               break;
            }
         }
         
         
      }
   }

   // If hasn't been placed, place it at the back
   if (!isSorted)
   {
      lTree.push_back(data);
   }
}

/*******************************************
 * getBinary()
 * Produces the binary huffman code for a BNode
 *******************************************/
void getBinary(BNode<custom::pair<string,float>> * pHead, string value, string find, string &p)
{
   string data = pHead->data.getFirst();
   // std::cout << "Buscando em : " << find << " em " << data << "\n";
   if (data == find)
   {
      p = value;
   }
   else
   {
      if (pHead->pLeft) getBinary(pHead->pLeft,value + "0", find, p);
      if (pHead->pRight) getBinary(pHead->pRight,value + "1", find, p);
   }
}

/*******************************************
 * generateTree()
 * Takes a list of BNodes and generates a 
 * tree in a Huffman code order
 *******************************************/
BNode<custom::pair<string,float>> generateTree(custom::list<BNode<custom::pair<string,float>>> &lTree)
{
   BNode<custom::pair<string,float>> response;

   try
   {
      list <BNode<custom::pair<string,float>>> :: iterator it;
      
      // Loop through until there is only one item in the list
      while (lTree.size() > 1)
      {
         // BNodes that will refer to the two smallest items in the list
         BNode<custom::pair<string,float>> pos1;
         BNode<custom::pair<string,float>> pos2;

         // Pop the first two items of the list into pos1 and pos2
         pos1 = lTree.front();
         lTree.pop_front();
         pos2 = lTree.front();
         lTree.pop_front();
         
         //Create a new BNode with a value that is equal to the added frequency of pos1 and pos2
         custom::pair<string,float> newPair("",pos1.data.getSecond()+pos2.data.getSecond());
         BNode<custom::pair<string,float>> * newPos = new BNode<custom::pair<string,float>>(newPair);
         //Aux
         BNode<custom::pair<string,float>> * newPosTemp;
         BNode<custom::pair<string,float>> * p1  = new BNode<custom::pair<string,float>>(pos1);
         BNode<custom::pair<string,float>> * p2  = new BNode<custom::pair<string,float>>(pos2);

         // New BNode will point to pos1 and pos2
         addLeft(newPos,pos1.data);
         addRight(newPos,pos2.data);

         newPosTemp = newPos->pLeft;
         addBranch(newPosTemp,pos1);
         
         newPosTemp = newPos->pRight;
         addBranch(newPosTemp,pos2);

         // Sort the list from smallest to largest
         includeListOnSort(lTree,*newPos);
      }

      response = lTree.front();
   }
   catch(const std::exception& e)
   {
      std::cout << e.what() << '\n';
   }

   return response;

}

/*******************************************
 * HUFFMAN
 * Driver program to exercise the huffman generation code
 *******************************************/
void huffman(const string & fileName)
{

   // Default list of huffman
   custom::list<BNode<custom::pair<string,float>>> pTree;
   custom::list<string> huff; //Used for show result
   custom::list<custom::pair<string,float>> fileReaded; //used for read file without repetition

   // Read file
   ifstream file;
   file.open(fileName);
   if (!file.fail())
   {
      custom::pair<string,float> p;
      BNode<custom::pair<string,float>> b;

      while (!file.eof())
      {
         file >> p;
         b.data = p;
         huff.push_back(b.data.getFirst());
         fileReaded.push_back(p);
      }
      fileReaded.pop_back(); // delete the last
      huff.pop_back(); // delete the last
   }
   else
   {
      cout << "Error opening file: " << fileName << endl;
      return;
   }
   
   // Create a List ordenated with values
   while(!fileReaded.empty())
   {
      BNode<custom::pair<string,float>> b;
      b.data = fileReaded.front();
      includeListOnSort(pTree, b);
      fileReaded.pop_front();
   }

   // Create a Tree
   BNode<custom::pair<string,float>> pTreeHuffman = generateTree(pTree);

   // Variables temp for display result
   BNode<custom::pair<string,float>> * pT = new BNode<custom::pair<string,float>>(pTreeHuffman);

   // With list default display result
   string result = "";
   while(!huff.empty())
   {
      result = "";
      getBinary(pT,"", huff.front(), result); //Find the binary value
      std::cout << huff.front() << " = " << result << "\n"; //display
      huff.pop_front();
   }

   return;
}
