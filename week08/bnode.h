#ifndef BNODE_H
#define BNODE_H // BNODE_H

#include <ostream>

#define CHECK std::cout << "Check\n";

/*****************************************************************************
 * class BNode
 * Contains a variable of data and pointers to the parent and child nodes of a
 * tree.
 *****************************************************************************/
template<class T>
class BNode
{
   private:
   public:
      T data;
      BNode * pParent;
      BNode * pLeft;
      BNode * pRight;

   // Constructors
   BNode()        :              pParent(NULL), pLeft(NULL), pRight(NULL) {};
   BNode(T _data) : data(_data), pParent(NULL), pLeft(NULL), pRight(NULL) {};
};

// Non-member functions

/**************************************************************************
 * addLeft
 * Takes a BNode and a template value. Creates a left child BNode
 * using the template value
 **************************************************************************/
template<class T>
void addLeft(BNode<T> * pTree, T value)
{
   BNode<T> * pNode = new BNode<T>(value);

   pTree->pLeft = pNode;
   pNode->pParent = pTree;

}

/**************************************************************************
 * addLeft
 * Takes two BNodes as parameters. Places the second BNode as the left 
 * child of the first
 **************************************************************************/
template<class T>
void addLeft(BNode<T> * pTree, BNode<T> * pNode)
{
   if (pTree)
   {
      pTree->pLeft = pNode;
   }

   if (pNode)
   {
      pNode->pParent = pTree;
   }

}

/**************************************************************************
 * addRight
 * Takes a BNode and a template value. Creates a right child BNode
 * using the template value
 **************************************************************************/
template<class T>
void addRight(BNode<T> * pTree, T value)
{
   BNode<T> * pNode = new BNode<T>(value);

   pTree->pRight = pNode;
   pNode->pParent = pTree;
}

/**************************************************************************
 * addRight
 * Takes two BNodes as parameters. Places the second BNode as the right 
 * child of the first
 **************************************************************************/
template<class T>
void addRight(BNode<T> * pTree, BNode<T> * pNode)
{
   if (pTree)
   {
      pTree->pRight = pNode;
   }

   if (pNode)
   {
      pNode->pParent = pTree;
   }
}

/**************************************************************************
 * bopyBTree
 * Returns a copy of the passed BNode tree
 **************************************************************************/
template<class T>
BNode<T> * copyBTree(BNode<T> * pTree)
{
   // Create a new BNode
   BNode<T> * newTree;

   // Copy the root to the new tree
   if (pTree)
   {
      newTree = new BNode<T>(pTree->data);
   }
  
   if (pTree->pLeft != NULL)
   {
      newTree->pLeft = copyBTree(pTree->pLeft);
   }
   if (newTree->pLeft != NULL)
   {
     newTree->pLeft->pParent = newTree;
   }
   
   if (pTree->pRight != NULL)
   {
      newTree->pRight = copyBTree(pTree->pRight);
   }
   if (newTree->pRight != NULL)
   {
     newTree->pRight->pParent = newTree;
   }

   // After copying is over, return the root node
   return newTree;
}

/**************************************************************************
 * deleteBTree
 * Takes a BNode as a parameter and deletes it and all of its descendent 
 * BNodes
 **************************************************************************/
template<class T>
void deleteBTree(BNode<T> * & pTree)
{
   if (pTree == NULL)
   {
      return;
   }

   deleteBTree(pTree->pLeft);
   deleteBTree(pTree->pRight);

   // Delete the tree
   delete pTree;
   
   // Set it to NULL
   pTree = NULL;
}

/**************************************************************************
 * sizeBTree
 * Takes a BNode as a parameter and returns the number of BNodes in the tree
 **************************************************************************/
template<class T>
int sizeBTree(BNode<T> * pTree)
{
   // Size starts as 1 (counting this BNode)
   int size = 1;

   if (pTree->pLeft)
   {
      // Add left children
      size += sizeBTree(pTree->pLeft);
   }

   if (pTree->pRight)
   {
      // Add right children
      size += sizeBTree(pTree->pRight);
   }

   return size;
}

/**************************************************************************
 * operator <<
 * displays the BNode tree in LVR order
 **************************************************************************/
template<class T>
std::ostream & operator << (std::ostream & out, const BNode<T> * rhs)
{
   if (rhs == NULL)
      return out;
   
   out << rhs->pLeft
      << rhs->data << ' '
      << rhs->pRight;

   return out;
}

/**************************************************************************
 * displayRVL
 * displays the BNode tree in RVL order
 **************************************************************************/
template<class T>
void displayRVL(const BNode<T> * pHead)
{
   if (pHead == NULL)
   {
      return;
   }

   displayRVL(pHead->pRight); // R
   std::cout << pHead->data;  // V
   displayRVL(pHead->pLeft);  // L

}

/**************************************************************************
 * displayVLR
 * Dislplays the BNode tree in VLR order
 **************************************************************************/
template<class T>
void displayVLR(const BNode<T> * pHead)
{
   if (pHead == NULL)
   {
      return;
   }

   std::cout << pHead->data;  // V
   displayRVL(pHead->pLeft);  // L
   displayRVL(pHead->pRight); // R
}

/**************************************************************************
 * displayLRV
 * Displays the BNode tree in LRV order
 **************************************************************************/
template<class T>
void displayLRV(const BNode<T> * pHead)
{
   if (pHead == NULL)
   {
      return;
   }

   displayRVL(pHead->pLeft);  // L
   displayRVL(pHead->pRight); // R
   std::cout << pHead->data;  // V
}

#endif // BNODE_H