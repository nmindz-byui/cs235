/***********************************************************************
 * Header:
 *    FIBONACCI
 * Summary:
 *    This will contain just the prototype for fibonacci(). You may
 *    want to put other class definitions here as well.
 * Authors
 *    Evandro Camargo, Alexandre Paixao & Caleb Georgeson
 ************************************************************************/

#ifndef FIBONACCI_H
#define FIBONACCI_H

#include "wholeNumber.h" // for wholeNumber class

// the interactive fibonacci program
void fibonacci();

#endif // FIBONACCI_H

