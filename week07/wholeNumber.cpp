/***********************************************************************
 * Header:
 *    WHOLENUMBER
 * Summary:
 *    This contains the implementation of a wholeNumber class,
 *    which handles big integers for use in conjunction with
 *    linked lists and a Fibonacci sequence generator.
 * Authors
 *    Evandro Camargo, Alexandre Paixao & Caleb Georgeson
 **********************************************************************/

#include <iomanip>
#include <iostream>
#include <cmath>
#include "wholeNumber.h"

using namespace custom;

/********************************************************************
* wholeNumber :: wholeNumber
* Default Constructor
*******************************************************************/
wholeNumber :: wholeNumber()
{
   // Positive by default
   this->isNegative = false;

   // This ensures an empty wholeNumber is returned by default
}

/********************************************************************
* wholeNumber :: wholeNumber
* Initializes wholeNumber with a single int as a Node of the numbers list
*******************************************************************/
wholeNumber :: wholeNumber(int _number, bool _negative = 0)
{
   // Positive by default
   this->isNegative = _negative;

   this->numbers.push_front(0);

   custom::list<int>::iterator it = this->numbers.begin();

   int total = (*it).data + _number;

   while (total > 0)
   {
      (*it).data = total % 1000;
      total = total / 1000;

      if (total > 0)
      {
         if (--it == this->numbers.end())
         {
            this->numbers.push_front(0);
            it = *(this->numbers.begin());
         }
         total += (*it).data;
      }
   }
}

/********************************************************************
* wholeNumber :: wholeNumber
* Initializes wholeNumber with a given custom::list object
*******************************************************************/
wholeNumber :: wholeNumber(custom::list<int> _list, bool _negative = 0)
{
   // Positive by default
   this->isNegative = _negative;

   // The iterator for the given list
   custom::list<int>::iterator it = _list.begin();

   // Empties current custom::list of this->numbers
   this->numbers.clear();

   // Adds each element from _list to the now empty numbers list
   for (it; it != _list.end(); ++it)
   {
      this->numbers.push_back((*it).data);
   }
}

/********************************************************************
* wholeNumber :: wholeNumber
* Copy Constructor
*******************************************************************/
wholeNumber :: wholeNumber(const wholeNumber &rhs)
{
   // The iterator for the RHS list
   custom::list<int>::iterator it = rhs.numbers.begin();

   // Carries sign bit
   this->isNegative = rhs.isNegative;

   // Empties current custom::list of this->numbers
   this->numbers.clear();

   // Adds each element form RHS to the now empty numbers list
   for (it; it != rhs.numbers.end(); ++it)
   {
      this->numbers.push_back((*it).data);
   }
}

/********************************************************************
* wholeNumber :: ~wholeNumber
* Destructor
*******************************************************************/
wholeNumber :: ~wholeNumber()
{
   // Housekeeping just in case
   // this->numbers.clear();
}

/********************************************************************
* wholeNumber :: operator =
* Assignment Operator Overload +1
*******************************************************************/
wholeNumber wholeNumber :: operator = (int number)
{
   if (number < 0)
   {
      // Carries sign bit
      this->isNegative = true;
   }

   // Making sure list will be empty before insert
   this->numbers.clear();

   // Re-initializes the list with a given integer
   this->numbers.push_back(number);

   return *this;
}

/********************************************************************
* wholeNumber :: operator =
* Assignment Operator Overload +2
*******************************************************************/
wholeNumber wholeNumber :: operator = (const wholeNumber &rhs)
{
   custom::list<int>::iterator it = rhs.numbers.begin();

   // Carries sign bit
   this->isNegative = rhs.isNegative;
   
   this->numbers.clear();

   for (it; it != rhs.numbers.end(); ++it)
   {
      this->numbers.push_back((*it).data);
   }

   return *this;
}

/********************************************************************
* wholeNumber :: operator +=
* Increment Equals Operator Overload
*******************************************************************/
wholeNumber wholeNumber :: operator += (const wholeNumber &rhs)
{
   // LHS and RHS list iterators
   custom::list<int>::reverse_iterator it_lhs = this->numbers.rbegin();
   custom::list<int>::reverse_iterator it_rhs = rhs.numbers.rbegin();

   // Tracker variables
   int lhs_count = this->numbers.size();
   int _lastCarry = 0;

   // For the segments present at both the left and right hand side,
   // perform addition and keep track of carry bits.
   for (size_t i = 0; i < lhs_count; i++, ++it_lhs, ++it_rhs)
   {
      // Carry bit for LHS addition
      int _carry = 0;

      // The result of the sum from LHS and RHS nodes,
      // before 3 digit overflow
      int _sumResult = (*it_lhs).data + (*it_rhs).data;

      // Handles 3 digits overflows
      if (_sumResult > 999)
      {
         _carry = _sumResult / 1000;
         _sumResult = _sumResult % 1000;
      }

      // Sets the LHS to the remainder of the
      // 3 digit overflow, if any
      (*it_lhs).data = _sumResult;

      // Carries the addition bit to the next
      // RHS node, if there are any left
      if (_carry > 0 && (*it_lhs).pPrev)
      {
         (*(*it_lhs).pPrev).data = (*(*it_lhs).pPrev).data + _carry;
      }

      // Else just save the carry bit for
      // the latter phase of wholeNumber addition
      else if (_carry > 0)
      {
         _lastCarry = _carry;
      }
   }

   // Adds the remaining segments from the right hand side
   // to the current wholeNumber sequence
   for (it_rhs; it_rhs != rhs.numbers.rend() || _lastCarry > 0; ++it_rhs)
   {
      // Takes care of adding the last carry bit from the
      // first phase of addition (same Nodes segment)
      // even if there are no more segments to the RHS
      if (it_rhs == rhs.numbers.rend())
      {
         this->numbers.push_back(_lastCarry);
         break;
      }
      // If there are still RHS segments and a _lastCarry is set
      // add it to the RHS node and perform addition as usual
      else if (_lastCarry > 0)
      {
         int _sumResult = _lastCarry + (*it_rhs).data;

         if (_sumResult > 999)
         {
            // Handles 3 digits overflows
            _lastCarry = _sumResult / 1000;
            _sumResult = _sumResult % 1000;
         }
         else
         {
            _lastCarry = 0;
         }
         
         this->numbers.push_back(_sumResult);
      }
      // If there are RHS nodes, and no _lastCarry bits
      // just keep incrementing the LHS from the RHS nodes
      else
      {
         this->numbers.push_back((*it_rhs).data);
      }
   }
   
   if (lhs_count < rhs.numbers.size())
   {
      this->numbers.push_front(this->numbers.back());
      this->numbers.pop_back();
   }
   
   return *this;
}

/********************************************************************
* wholeNumber :: operator +
* Addition Operator Overload
*******************************************************************/
wholeNumber wholeNumber :: operator + (const wholeNumber &rhs) const
{
   // LHS and RHS list iterators
   custom::list<int>::iterator it_lhs = this->numbers.begin();
   custom::list<int>::iterator it_rhs = rhs.numbers.begin();

   // Tracker variables
   custom::wholeNumber _new;
   int lhs_count = this->numbers.size();
   int _lastCarry = 0;

   // For the segments present at both the left and right hand side,
   // perform addition and keep track of carry bits.
   for (size_t i = 0; i < lhs_count; i++, ++it_lhs, ++it_rhs)
   {
      // Carry bit for LHS addition
      int _carry = 0;

      // The result of the sum from LHS and RHS nodes,
      // before 3 digit overflow
      int _sumResult = (*it_lhs).data + (*it_rhs).data;

      // Handles 3 digits overflows
      if (_sumResult > 999)
      {
         _carry = _sumResult / 1000;
         _sumResult = _sumResult % 1000;
      }

      // Sets the LHS to the remainder of the
      // 3 digit overflow, if any
      (*it_lhs).data = _sumResult;

      // Carries the addition bit to the next
      // LHS node, if there are any left
      if (_carry > 0 && (*it_lhs).pNext)
      {
         (*(*it_lhs).pNext).data = (*(*it_lhs).pNext).data + _carry;
      }

      // Else just save the carry bit for
      // the latter phase of wholeNumber addition
      else if (_carry > 0)
      {
         _lastCarry = _carry;
      }

      // Since we are returning a new instance of wholeNumber,
      // adds the already calculated Node to the new list
      _new.numbers.push_back((*it_lhs).data);
   }

   // Adds the remaining segments from the right hand side
   // to the current wholeNumber sequence
   for (it_rhs; it_rhs != rhs.numbers.end() || _lastCarry > 0; ++it_rhs)
   {
      // Takes care of adding the last carry bit from the
      // first phase of addition (same Nodes segment)
      // even if there are no more segments to the RHS
      if (it_rhs == rhs.numbers.end())
      {
         _new.numbers.push_back(_lastCarry);
         break;
      }
      // If there are still RHS segments and a _lastCarry is set
      // add it to the RHS node and perform addition as usual
      else if (_lastCarry > 0)
      {
         int _sumResult = _lastCarry + (*it_rhs).data;

         // Handles 3 digits overflows
         if (_sumResult > 999)
         {
            _lastCarry = _sumResult / 1000;
            _sumResult = _sumResult % 1000;
         }
         else
         {
            _lastCarry = 0;
         }
         
         // Since we are returning a new instance of wholeNumber,
         // adds the already calculated Node to the new list
         _new.numbers.push_back(_sumResult);
      }
      // If there are RHS nodes, and no _lastCarry bits
      // just keep incrementing the LHS from the RHS nodes
      else
      {
         _new.numbers.push_back((*it_rhs).data);
      }
   }

   // Returns a new instance of wholeNumber, _new
   return _new;
}

/********************************************************************
* wholeNumber :: operator -
* Subtraction Operator Overload
*******************************************************************/
wholeNumber wholeNumber :: operator - (const wholeNumber &rhs) const
{
   // LHS and RHS list iterators
   custom::list<int>::reverse_iterator it_lhs = this->numbers.rbegin();
   custom::list<int>::reverse_iterator it_rhs = rhs.numbers.rbegin();

   // Tracker variables
   custom::wholeNumber _new;
   int lhs_count = this->numbers.size();
   int _lastBorrow = 0;

   // For the segments present at both the left and right hand side,
   // perform subtraction and keep track of borrow bits.
   for (size_t i = 0; i < lhs_count; i++, ++it_lhs, ++it_rhs)
   {
      // Borrow bit for LHS subtraction
      int _borrow = 0;

      // The result of the sub from LHS and RHS nodes,
      // before 3 digit overflow
      int _subResult = (*it_lhs).data - (*it_rhs).data;

      // Handles 3 digits overflows
      if (_subResult < 0 && (*it_lhs).pPrev)
      {
         _subResult = _subResult + 1000;
         _borrow = 1;
      }
      else if (_subResult < 0 && !(*it_lhs).pPrev)
      {
         _new.isNegative = !_new.isNegative;
         _subResult = (_subResult + 1) * -1;
         _borrow = 1;
      }

      // Sets the LHS to the remainder of the
      // 3 digit overflow, if any
      (*it_lhs).data = _subResult;

      // Carries the subtraction bit to the next
      // LHS node, if there are any left
      if (_borrow > 0 && (*it_lhs).pPrev)
      {
         (*(*it_lhs).pPrev).data = (*(*it_lhs).pPrev).data - _borrow;
      }
      // Else just save the borrow bit for
      // the latter phase of wholeNumber subtraction
      else if (_borrow > 0)
      {
         _lastBorrow = _borrow;
      }

      // Since we are returning a new instance of wholeNumber,
      // adds the already calculated Node to the new list
      _new.numbers.push_back((*it_lhs).data);
   }

   // Resets LHS pointer to _new numbers list
   it_lhs = _new.numbers.rbegin();

   // Adds the remaining segments from the right hand side
   // to the current wholeNumber sequence
   for (it_rhs; it_rhs != rhs.numbers.rend() || _lastBorrow > 0; ++it_rhs)
   {
      // Takes care of going back to the first (right-most) Node
      // from the subtraction result and adjust its offset by -1000
      // in case we had to borrow more than we had at the last operation
      // and there are no more RHS Nodes to iterate through
      if (it_rhs == rhs.numbers.rend() && _lastBorrow > 0)
      {
         (*it_lhs).pPrev->data = ((*it_lhs).pPrev->data - 1000) * -1;
         break;
      }
      // If there are still RHS segments and a _lastBorrow is set
      // sub it from the RHS node and perform subtraction as usual
      else if (_lastBorrow > 0)
      {
         // In this case it should always be negative
         _new.isNegative = true;
         int _subResult = (*it_rhs).data - _lastBorrow;

         // Handles 3 digits overflows
         if (_subResult < 0)
         {
            _subResult = _subResult + 1000;
            _lastBorrow = 1;
         }
         else
         {
            (*it_lhs).data = 1000 - (*it_lhs).data;
            _lastBorrow = 0;
         }
         
         // Since we are returning a new instance of wholeNumber,
         // adds the already calculated Node to the new list
         _new.numbers.push_back(_subResult);
      }
      // If there are RHS nodes, and no _lastBorrow bits
      // just keep subtracting the LHS from the RHS nodes
      else
      {
         // In this case it should always be negative
         _new.isNegative = true;

         // If there are no LHS Nodes to re-evaluate,
         // break out of the loop as there is nothing left to do
         if (it_lhs == NULL)
         {
            break;
         }
         
         // Go back to first LHS and make it pay its debt
         (*it_lhs).data = ((*it_rhs).data * 1000) - (*it_lhs).data;

         // If we have a big RHS, the operation needs to continue
         if ((*it_lhs).data > 999)
         {
            // We will modify (*it_lhs), so hold it in _tmp
            int _tmp = (*it_lhs).data;
            _new.numbers.push_back(_tmp / 1000);
            _new.numbers.pop_front();
            _new.numbers.push_front(_tmp % 1000);
         }

         // Move on to the next LHS
         ++it_lhs;
      }
   }

   // Returns a new instance of wholeNumber, _new
   return _new;
}

/********************************************************************
* wholeNumber :: operator >>
* Extraction Operator Overload
*******************************************************************/
wholeNumber wholeNumber :: operator >> (const wholeNumber &rhs)
{
   //
}

/********************************************************************
* wholeNumber :: operator * // Partially works
* Multiplication Operator Overload
*******************************************************************/
wholeNumber wholeNumber :: operator * (const wholeNumber &rhs) const
{
   // LHS and RHS list iterators
   custom::list<int>::iterator it_lhs = this->numbers.begin();
   custom::list<int>::iterator it_rhs = rhs.numbers.begin();

   // Tracker variables
   custom::list<custom::wholeNumber> _productSteps;
   custom::list<custom::wholeNumber>::iterator p_it;
   custom::wholeNumber _new(0);
   int lhs_count = this->numbers.size();
   int _lastCarry = 0;

   // Takes each RHS segment and multiply the LHS ones, one at a time
   for (size_t i = 0; it_rhs != rhs.numbers.end(); i++, ++it_rhs)
   {
      custom::wholeNumber _product;
      int _cPower = pow(1000, i);

      for (it_lhs; it_lhs != this->numbers.end() || _lastCarry > 0; ++it_lhs)
      {
         // Adds the carry from last product to the next LHS Node
         if (_lastCarry > 0)
         {
            _product.numbers.push_back(_lastCarry);
            _lastCarry = 0;
            if (it_lhs == this->numbers.end()) break;
         }

         // Carry bit for LHS multiplication
         // int _carry = 0;

         // The result of the product from LHS and RHS nodes,
         // before 3 digit overflow
         int _prodResult = (*it_lhs).data * (*it_rhs).data;

         // Handles 3 digits overflows
         if (_prodResult > 999)
         {
            _lastCarry = _prodResult / 1000;
            _prodResult = _prodResult % 1000;
         }
         else if (_prodResult == 0)
         {
            _lastCarry = 0;
            _product.numbers.push_front(0);
         }

         // Sets the LHS to the remainder of the
         // 3 digit overflow, if any
         (*it_lhs).data = _prodResult;

         // // Carries the addition bit to the next
         // // LHS node, if there are any left
         // if (_carry > 0 && (*it_lhs).pNext)
         // {
         //    (*(*it_lhs).pNext).data = (*(*it_lhs).pNext).data + _carry;
         // }

         // // Else just save the carry bit for
         // // the latter phase of wholeNumber addition
         // else if (_carry > 0)
         // {
         //    _lastCarry = _carry;
         // }

         // Since we are returning a new instance of wholeNumber,
         // adds the already calculated Node to the new list
         _product.numbers.push_back((*it_lhs).data);
      }

      _productSteps.push_back(_product);

      for (p_it = _productSteps.begin(); p_it != _productSteps.end(); ++p_it)
      {
         _new += (*p_it).data;
      }

      return _new;
   }


   // For the segments present at both the left and right hand side,
   // perform addition and keep track of carry bits.
   for (size_t i = 0; i < lhs_count; i++, ++it_rhs)
   {
      // Carry bit for LHS addition
      int _carry = 0;

      // The result of the sum from LHS and RHS nodes,
      // before 3 digit overflow
      int _sumResult = (*it_lhs).data * (*it_rhs).data;

      // Handles 3 digits overflows
      if (_sumResult > 999)
      {
         _carry = _sumResult / 1000;
         _sumResult = _sumResult % 1000;
      }

      // Sets the LHS to the remainder of the
      // 3 digit overflow, if any
      (*it_lhs).data = _sumResult;

      // Carries the addition bit to the next
      // RHS node, if there are any left
      if (_carry > 0 && (*it_lhs).pNext)
      {
         (*(*it_lhs).pNext).data = (*(*it_lhs).pNext).data + _carry;
      }

      // Else just save the carry bit for
      // the latter phase of wholeNumber addition
      else if (_carry > 0)
      {
         _lastCarry = _carry;
      }

      // Since we are returning a new instance of wholeNumber,
      // adds the already calculated Node to the new list
      _new.numbers.push_back((*it_lhs).data);
   }

   // Returns a new instance of wholeNumber, _new
   return _new;
}