/***********************************************************************
 * Implementation:
 *    FIBONACCI
 * Summary:
 *    This will contain the implementation for fibonacci() as well as any
 *    other function or class implementations you may need
 * Authors
 *    Evandro Camargo, Alexandre Paixao & Caleb Georgeson
 **********************************************************************/

#include <iostream>
#include <math.h>        // for Mathematical functions
#include "fibonacci.h"   // for fibonacci() prototype
#include "list.h"        // for LIST
#include "wholeNumber.h" // for WHOLENUMBER

/************************************************
 * FIBONACCI
 * The interactive function allowing the user to
 * display Fibonacci numbers
 ***********************************************/
void fibonacci()
{
   int number;

   // show the first serveral Fibonacci numbers
   std::cout << "How many Fibonacci numbers would you like to see? ";
   std::cin >> number;
   {
      int t1 = 0, t2 = 1, nextTerm = 0;

      for (int i = 2; i <= number + 1; ++i)
      {
         // Prints the first two terms.
         if (i == 1)
         {
            std::cout << "\t" << t1 << "\n";
            continue;
         }
         if (i == 2)
         {
            std::cout << "\t" << t2 << "\n";
            continue;
         }
         nextTerm = t1 + t2;
         t1 = t2;
         t2 = nextTerm;
         
         std::cout << "\t" << nextTerm << "\n";
      }
   }

   // prompt for a single large Fibonacci
   std::cout << "Which Fibonacci number would you like to display? ";
   std::cin >> number;
   {
      custom::wholeNumber t1(0, false), t2(1, false), nextTerm(0, false), _swap(0, false);

      for (int i = 3; i <= number + 1; ++i)
      {
         // Prints the first two terms.
         if (number == 1)
         {
            std::cout << "\t" << t1 << "\n";
            continue;
         }
         if (number == 2)
         {
            std::cout << "\t" << t2 << "\n";
            continue;
         }
         nextTerm = t1 + t2;
         t1 = t2;
         t2 = nextTerm;
      }
      std::cout << "\t" << nextTerm << "\n";
   }
}