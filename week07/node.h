   /********************************************************************
   * custom :: list :: Node
   * It will hold one piece of data from the list and
   * pointers to the previous and next nodes
   * ******************************************************************/
   template<class T>
   class Node
   {
      private:
         // Private data
         Node<T> pHead;
         Node<T> pTail;
         int numElements;
      public:
         // Public data
         T data;
         Node<T> * pNext;
         Node<T> * pPrev
         // Constructors
         Node()            :          pPrev(NULL), pNext(NULL) {} // Default Constructor
         Node(const T & t) : data(t), pPrev(NULL), pNext(NULL) {} // Non Default Constructor
   };