/***********************************************************************
 * Header:
 *    wholeNumber
 * Summary:
 *    This contains the prototype and definitions of a wholeNumber
 *    class, which handles big integers for use in conjunction with
 *    linked lists and a Fibonacci sequence generator.
 * Authors
 *    Evandro Camargo, Alexandre Paixao & Caleb Georgeson
 **********************************************************************/

#ifndef WHOLENUMBER_H
#define WHOLENUMBER_H // WHOLENUMBER_H

#include <iostream>
#include <string>
#include "list.h"

namespace custom
{
   class wholeNumber
   {
      private:
         custom::list<int> numbers;
      public:
         bool isNegative;
         // CTOR / DTOR
         wholeNumber();                                           // Default Constructor
         wholeNumber(int _number, bool _negative);             // Non Default Constructor
         wholeNumber(custom::list<int> _list, bool _negative); // Non Default Constructor
         wholeNumber(const wholeNumber &rhs);                     // Copy Constructor
         ~wholeNumber();                                          // Destructor
         // Operator Overloads
         wholeNumber operator = (int number);                  // Assignment Operator Overload
         wholeNumber operator = (const wholeNumber &rhs);         // Assignment Operator Overload
         wholeNumber operator += (const wholeNumber &rhs);        // Add Onto Operator Overload
         wholeNumber operator + (const wholeNumber &rhs) const;   // Addition Operator Overload
         // Extra Credit
         wholeNumber operator - (const wholeNumber &rhs) const;   // Subtraction Operator Overload
         wholeNumber operator >> (const wholeNumber &rhs);        // Extraction Operator Overload
         wholeNumber operator * (const wholeNumber &rhs) const;   // Multiplication Operator Overload

         // Left Shift Operator Overload
         friend std::ostream& operator << (std::ostream &out, const wholeNumber &rhs)
         {
            // Use reverse_iterator to display in natural order
            list<int>::reverse_iterator it;

            if (rhs.isNegative)
            {
               out << "-";
            }

            // Checks if last stored Node (left-most) is 000
            bool _skipFirst = (rhs.numbers.back() == 0);

            for (it = rhs.numbers.rbegin(); it != rhs.numbers.rend(); it)
            {
               // Skips the first Node if it is a 000 to the left
               if (_skipFirst)
               {
                  ++it;
                  _skipFirst = !_skipFirst;
                  continue;
               }

               int _n = (*it).data;

               // Adds 1 leading zero to 10 <= x < 100
               if (_n >= 10 && _n < 100)
               {
                  out << "0" << std::to_string(_n);
               }
               // Adds 2 leading zeros to 0 <= x < 10
               else if (_n >= 0 && _n < 10)
               {
                  out << "00" << std::to_string(_n);
               }
               // Just outputs 3 digit numbers as-is
               else
               {
                  out << _n;
               }
               // Handles separating sets of 3 by comma
               if (++it != rhs.numbers.rend())
               {
                  out << ",";
               }
            }

            return out;
         }
   };
}

#endif // WHOLENUMBER_H