/***********************************************************************
* Program:
*    Assignment 05, Go Fish
*    Brother Helfrich, CS 235
*
* Authors:
*    Evandro Camargo, Alexandre Paixao & Caleb Georgeson
*
* Summary: 
*    This is all the functions necessary to play Go Fish!
*
*    Estimated:  1.0 hrs   
*    Actual:     2.0 hrs
*
*    The Go Fish! driver wasn't actually all that hard, once
*    we figured how to use the iterators. While doing it we
*    realized we had a problem with our operator overloading
*    within the definition of the custom::set class.
*
************************************************************************/

#include <iostream>
#include <fstream>
#include <string>
#include "set.h"
#include "card.h"
#include "goFish.h"
using namespace std;

/**********************************************************************
 * GO FISH
 * The function which starts it all
 ***********************************************************************/
void goFish()
{
   // Initializes base variables
   custom::set<Card> cardsHand = getHandCard();
   int matches = 0;

   std::cout << "We will play 5 rounds of Go Fish.  Guess the card in the hand\n";
   
   // Plays the game
   for (int i = 1; i <= 5; i++)
   {
      std::string card;
   
      std::cout << "round " << i << ": ";
      std::cin >> card;

      // Card set iterators to check for a match
      custom::set <Card> :: iterator itEmptyCard = cardsHand.end();
      custom::set <Card> :: iterator itFindCard = cardsHand.find(card.c_str());
      
      // Search the hand for the card.
      if (itFindCard != itEmptyCard)
      {
         std::cout << "\tYou got a match!\n";
         cardsHand.erase(itFindCard);
         matches++;
      }
      else 
      {
         std::cout << "\tGo Fish!\n";
      }
   }
   std::cout << "You have " << matches << " matches!\n";
   std::cout << "The remaining cards: ";
   
   // Displays the remaining cards in our hand
   for (custom::set<Card>::iterator it = cardsHand.begin(); it != cardsHand.end(); ++it)
   {
      std::cout << *it;
      if (it + 1 != cardsHand.end())
      {
         std::cout<< ", ";  
      }
    
   }
   std::cout << "\n";
}

// Read File
// Return the hand
custom::set<Card> getHandCard()
{
   custom::set<Card> setReturn;
   string sHand;

   ifstream fileHand;
   fileHand.open("/home/cs235/week05/hand.txt");
   
   if (fileHand.is_open()) 
   {
      while (!fileHand.eof())
      {
         fileHand >> sHand;
         setReturn.insert(sHand.c_str());
      }
   }

   return setReturn;

}

