/***********************************************************************
* Header:
*    Go Fish
* Summary:
*    This will contain just the prototype for the goFish() function
* Author
*    <your names here>
************************************************************************/

#ifndef GO_FISH_H
#define GO_FISH_H

#include "card.h"

/**************************************************
 * GO FISH
 * Play the game of "Go Fish"
 *************************************************/
void goFish();
custom::set<Card> getHandCard();
#endif // GO_FISH_H