/***********************************************************************
 * Header:
 *    Node * Summary:
 *    This will contain the implementation of the templated
 *    abstract data structure for a "Node".
 * Author
 *    Evandro Camargo, Alexandre Paixao & Caleb Georgeson
 ************************************************************************/

#ifndef NODE_H
#define NODE_H // NODE_H

#include <iostream>

// Node Class
template<class T>
class Node
{
   private:
   public:
      T data;
      Node * pPrev;
      Node * pNext;

   Node()            : pNext(NULL), pPrev(NULL) {}          // Default Constructor
   Node(const T & t) : data(t), pPrev(NULL), pNext(NULL) {} // Non Default Constructor
};

/********************************************************************
 * freeData
 * Frees memory used by Nodes
 *******************************************************************/
template <class T>
void freeData(Node <T> * & pHead)
{
   Node <T> * pDelete;

   // Delete items from the list
   while (pHead != NULL)
   {
      pDelete = pHead;
      // Gives the next position to pHead
      pHead = pHead->pNext;
      // Deletes previous head position
      delete pDelete;
   }
}

/********************************************************************
 * insert
 * Inserts a new Node to the tree
 *******************************************************************/
template <class T>
Node <T> * insert(Node <T> * pNode, const T & t, bool after = false)
{
   // New placeholder Node
   Node <T> * pNew = new Node <T> (t);
   
   if (pNode == NULL)
   {
      // If given Node is NULL/invalid,
      // return an empty Node
      return pNew;
   }

   // The "after" boolean determines
   // wether the Node will be appended
   // before or after "pNode"
   if (after) 
   {
      if (!(pNode->pNext) && !(pNode->pPrev))
      {
         pNew->pPrev = pNode;
         pNode->pNext = pNew;
      }
      else
      {
         pNew->pNext = pNode->pNext;
         pNew->pPrev = pNode;   
         pNode->pNext = pNew;
         if (pNew->pNext)
         {
            pNode->pNext->pPrev = pNew;
         }
      }
   }
   // If false, inserts before "pNode"
   else
   {
      pNew->pNext = pNode;
      pNew->pPrev = pNode->pPrev;
   
      if (pNew->pPrev)
      {
         pNode->pPrev->pNext = pNew;
      }
      pNode->pPrev = pNew;
   }
   
   return pNew;
}

/********************************************************************
 * copy
 * Copies the data off a Node
 *******************************************************************/
template <class T>
Node <T> * copy(Node <T> * pSource)
{
   // Allocates and initializes a Node pointer with pSource
   Node <T> * pDestination = new Node <T> (pSource->data);

   // Points the head of the Node list to pDestination
   Node <T> * pHead = pDestination;

   for (Node <T> * p = pSource->pNext; p; p = p->pNext)
   {
      // Inserts each data in the Node list after pHead
      pHead = insert(pHead, p->data, true);
   }

   // Returns the pDestination Node pointer
   return pDestination;
}

/********************************************************************
 * find
 * Finds an element, given the Node head
 *******************************************************************/
template <class T>
Node <T> * find(Node <T> * pHead, const T & t)
{
   // Iterates through the linked Nodes
   // to find a given value of "t"
   for (Node <T> * p = pHead; p; p = p->pNext)
   {
      // If data matches "t"
      if (p->data == t)
      {
         // Returns Node pointer
         return p;
      }
   }
   // If there is no match, return NULL
   return NULL;
}

/********************************************************************
 * remove
 * Removes a Node from the tree
 *******************************************************************/
template <class T>
Node <T> * remove(Node <T> * pNode)
{	
   // If Node is NULL, there is nothing
   // linked, therefore it is an empty list
   if (pNode == NULL)
   {
      std::cout << "List empty";
   }

   // Checks if there is a Node "before" current
   if (pNode->pPrev)
   {
      pNode->pPrev->pNext = pNode->pNext;
   }

   // Checks if there is a Node "after" current
   if (pNode->pNext)
   {
      pNode->pNext->pPrev = pNode->pPrev;
   }

   // The returning Node pointer
   Node <T> * pReturn;
   pReturn = pNode->pPrev ? pNode->pPrev : pNode->pNext;

   // Deletes the current pNode
   delete pNode;

   return pReturn;
}

/********************************************************************
 * operator <<
 * Left Shift Operator Overload
 *******************************************************************/
template <class T>
std::ostream & operator << (std::ostream & out, const Node <T> * pHead)
{
   // Iterates through the entire linked Node tree
   for (const Node <T> * p = pHead; p; p = p->pNext)
   {
      // If this is not the last Node in the tree, add a ","
      if (p->pNext != NULL)
      {
         out << p->data << ", ";
      }
      // If this is the last Node, just print it
      else
      {
         out << p->data;
      }
   }

   return out;
}

#endif // NODE_H