/***********************************************************************
 * Header:
 *    INSERTION SORT
 * Summary:
 *    This will contain just the prototype for insertionSortTest(). You may
 *    want to put other class definitions here as well.
 * Author
 *    Evandro Camargo, Alexandre Paixao & Caleb Georgeson
 ************************************************************************/

#ifndef INSERTION_SORT_H
#define INSERTION_SORT_H

#include "node.h"

/***********************************************
 * sortInsertion
 * Sort the items in the array
 **********************************************/
template <class T>
void sortInsertion(T array[], int num)
{
   // Creates a first Node off the first element from the Array
   Node <T> * pSorted = new Node<T> (array[0]);

   // Temporary placeholder Node
   Node <T> * pTemp;

   // Begins sorting
   for (int i = 1; i < num; i++)
   {
      pTemp = NULL;
      Node <T> * p = NULL;

      // Compares current Node data with next
      for (p = pSorted; p; p = p->pNext)
      {
         if (p->data > array[i])
         {
            pTemp = insert(p, array[i]);
            if (p == pSorted)
            {
               pSorted = pTemp;
            }
            break;
         }
         
         // If next Node points to nothing, exit
         if (!p->pNext)
         {
            break;
         }
      }

      if (!pTemp)
      {
         insert(p, array[i], true);
      }
   }

   // Fills an array with the sorted Node data
   int i = 0;
   for (Node <T> * p = pSorted; p; p = p->pNext)
   {
      array[i] = p->data;
      i++;
   }

   // At last, free the Node
   freeData(pSorted);
}

#endif // INSERTION_SORT_H

