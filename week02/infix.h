/***********************************************************************
* Header:
*    INFIX      
* Summary:
*    This will contain just the prototype for the convertInfixToPostfix()
*    function
* Authors
*    Evandro Camargo, Alexandre Paixao & Caleb Georgeson
************************************************************************/

#ifndef INFIX_H
#define INFIX_H

/*****************************************************
 * TEST INFIX TO POSTFIX
 * Prompt the user for infix text and display the
 * equivalent postfix expression
 *****************************************************/
void testInfixToPostfix();

/*****************************************************
 * TEST INFIX TO ASSEMBLY
 * Prompt the user for infix text and display the
 * resulting assembly instructions
 *****************************************************/
void testInfixToAssembly();

/*****************************************************
 * int weightPostfixOperators
 * Receives an operator and returns an
 * index of priority inside the postfix notation
 *****************************************************/
int weightPostfixOperators(const char &opcode);

#endif // INFIX_H

