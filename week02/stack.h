/***********************************************************************
* Header:
*    Stack      
 * Summary:
 *    This class contains the definition and implementation of a generic
 *    STACK data structure.
* Authors
*    Evandro Camargo, Alexandre Paixao & Caleb Georgeson
************************************************************************/

#ifndef STACK_H
#define STACK_H

#define ALLOC_SIZE_STD 4

namespace custom
{
   /************************************************
    * Stack
    * Abstract Data Structure
    ***********************************************/
   template<class T>
   class stack
   {
      private:
         T* data;
         int numElements;
         int numCapacity;
         void resize(unsigned int _size);
      public:
         // Constructors and Destructor
         stack();
         stack(int numCapacity);
         // Rule of Three
         ~stack();
         stack(const stack<T> &rhs);   // Copy constructor
         stack(stack&&) = default;     // Move constructor
         // Operator Overloading
         stack<T>& operator=(const stack &rhs);
         // Internal Methods
         bool empty() const;
         int size() const;
         int capacity() const;
         void clear();
         // Modifies the Stack
         void push(const T &t);
         T& top();
         T top() const;
         void pop();
   };

   /******************************************
    * stack :: stack
    * Default Constructor
    ******************************************/
   template<class T>
   stack<T> :: stack()
   {
      this->numElements = 0;
      this->numCapacity = 0;
      this->data = NULL;
   }
   
   /******************************************
    * stack :: stack
    * Non-Default Constructor (+1 Overload)
    ******************************************/
   template<class T>
   stack<T> :: stack(int numCapacity)
   {
      try
      {
         numElements = 0;
         this->numCapacity = numCapacity;
         this->data = new T[this->numCapacity];
      }
      catch (std::bad_alloc)
      {
         throw "ERROR: Unable to allocate a new buffer for Stack.";
      }
   }

   /******************************************
    * stack :: ~stack
    * Class Destructor
    ******************************************/
   template<class T>
   stack<T> :: ~stack()
   {
      // If I attempt to free this pointer,
      // there is a memory exception. I can't
      // for the life of me figure why, not
      // even after debugging with Valgrind

      // Free the allocated memory
      // delete[] this->data;

      // Set the pointer to NULL
      // data = NULL;
   }

   /******************************************
    * stack :: stack
    * Copy Constructor
    ******************************************/
   template<class T>
   stack<T> :: stack(const stack<T> &rhs)
   {
      try
      {
         this->data = new T[rhs.capacity()];
         this->numCapacity = rhs.capacity();
         this->numElements = rhs.size();

         for (unsigned int i = 0; i < rhs.size(); i++)
         {
            this->data[i] = rhs.data[i];
         }
      }
      catch (std::bad_alloc)
      {
         throw "ERROR: Unable to allocate a new buffer for Stack.";
      }
   }

   /******************************************
    * stack :: operator =
    * Assignment Operator Overload
    ******************************************/
   template<class T>
   stack<T>& stack<T> :: operator=(const stack &rhs)
   {
      try
      {
         if (this->numCapacity < rhs.capacity())
         {
            this->resize(rhs.capacity());
         }

         this->numElements = rhs.size();
         this->data = rhs.data;

         return *this;
      }
      catch (std::bad_alloc)
      {
         throw "ERROR: Unable to allocate a new buffer for Stack.";
      }
   }

   /******************************************
    * stack :: resize
    * Returns whether or not the stack is resize
    ******************************************/
   template<class T>
   void stack<T> :: resize(unsigned int _size)
   {
      // calc capacity size
      int remainder = _size % ALLOC_SIZE_STD;
      int _capacity = 0;


      if (this->numCapacity >= ALLOC_SIZE_STD)
      {
         _capacity = this->numCapacity * 2;
      }
      else if (remainder == 0)
      {
         // keep _capacity the same
      }
      else
      {
         _capacity = _size + ALLOC_SIZE_STD - remainder;
      }

      
      try
      {
         T* _data = new T[_capacity]; // allocate new temporary data

         // move data from old data to new tmp data
         for(int i = 0; i < this->size(); i++)
         {
            _data[i] = this->data[i];
         }
         delete[] this->data;

         // initialize new data with more capacity
         this->data = _data;
         
         this->numCapacity = _capacity;
      }
      catch (std::bad_alloc)
      {
         throw "ERROR: Unable to allocate a new buffer for Stack";
      }
   }

   /******************************************
    * stack :: empty
    * Returns whether or not the stack is empty
    ******************************************/
   template<class T>
   bool stack<T> :: empty() const
   {
      return (this->numElements == 0 ? true : false);
   }

   /******************************************
    * stack :: size
    * Returns the number of elements currently
    * in the stack.
    ******************************************/
   template<class T>
   int stack<T> :: size() const
   {
      return this->numElements;
   }

   /******************************************
    * stack :: capacity
    * Returns the number of elements the stack
    * will hold before reallocating.
    ******************************************/
   template<class T>
   int stack<T> :: capacity() const
   {
      return this->numCapacity;
   }

   /******************************************
    * stack :: clear
    * Removes all elements from the stack.
    ******************************************/
   template<class T>
   void stack<T> :: clear()
   {
      this->numElements = 0;
   }

   /******************************************
    * stack :: push
    * Adds an element to the top of the stack,
    * thereby increasing size by one.
    ******************************************/
   template<class T>
   void stack<T> :: push(const T &t)
   {
      if (this->numElements + 1 > this->numCapacity)
      {
         this->resize(this->numElements+1);
      }

      this->data[this->size()] = t;
      this->numElements++;
   }

   /******************************************
    * stack :: top
    * Returns the top-most element in the stack.
    ******************************************/
   template<class T>
   T& stack<T> :: top()
   {
      if (this->numElements < 1)
      {
         throw "ERROR: Unable to reference the element from an empty Stack";
      }

      return this->data[this->numElements-1];
   }

   /******************************************
    * stack :: top
    * Returns the top-most element in the stack.
    ******************************************/
   template<class T>
   T stack<T> :: top() const
   {
      if (this->numElements < 1)
      {
         throw "ERROR: Unable to reference the element from an empty Stack";
      }

      return this->data[this->numElements-1];
   }

   /******************************************
    * stack :: pop
    * Remove the top-most element in the stack,
    * thereby reducing the size by one.
    ******************************************/
   template<class T>
   void stack<T> :: pop()
   {
      // Reduce the size by one if we have elements in the stack
      if (this->numElements > 0)
      {
         --numElements;
      }
   }
}

#endif // STACK_H