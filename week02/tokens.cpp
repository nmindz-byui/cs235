/***********************************************************************
 * Module:
 *    Week 02, Stack
 *    Brother Helfrich, CS 235
 * Author:
 *    Evandro Camargo
 *    Caleb Georgeson
 *    Alexandre Paixao
 * Summary:
 *    This program will implement the testInfixToPostfix()
 *    and testInfixToAssembly() functions
 ************************************************************************/

#include <iostream>    // for ISTREAM and COUT
#include <string>      // for STRING
#include <cassert>     // for ASSERT
#include <vector>      // for VECTOR
#include "stack.h"     // for STACK
#include "tokenizer.h"     // for STACK
using namespace std;

/**********************************************************************
 * MAIN
 * This is just a simple menu to launch a collection of tests
 ***********************************************************************/
int main()
{
    std::string myStr = "this is a string";
    custom::Tokenizer<string> t = new custom::Tokenizer(myStr);
}