/***********************************************************************
 * Module:
 *    Week 02, Stack
 *    Brother Helfrich, CS 235
 * Authors:
 *    Evandro Camargo, Alexandre Paixao & Caleb Georgeson
 * Summary:
 *    This program will implement the testInfixToPostfix()
 *    and testInfixToAssembly() functions
 ************************************************************************/

#include <iostream>    // for ISTREAM and COUT
#include <string>      // for STRING
#include <cassert>     // for ASSERT
#include <vector>      // for VECTOR
#include "stack.h"     // for STACK
using namespace std;

int weightPostfixOperator(const char &opcode);

/*****************************************************
 * CONVERT INFIX TO POSTFIX
 * Convert infix equation "5 + 2" into postifx "5 2 +"
 *****************************************************/
string convertInfixToPostfix(const string& infix)
{
	string _postfix = "";
	std::vector<string> postfix;
	custom::stack<char> operators;
	std::vector<string> tokens;

   // Temporary placeholder for a word
	std::string temp = "";
   std::string megaWord;

   // Splits words/variables

   for (unsigned int i = 0; i < infix.size(); i++)
   {
	   if (infix[i] != ' ')
	   {
		   megaWord += infix[i];
	   }
   }

   for (unsigned int i = 0; i < megaWord.size(); i++)
   {
	   if (
		   megaWord[i] == '+' ||
		   megaWord[i] == '-' ||
		   megaWord[i] == '*' ||
		   megaWord[i] == '/' ||
		   megaWord[i] == '%' ||
		   megaWord[i] == '^' ||
		   megaWord[i] == ')' ||
		   megaWord[i] == '('
		   )
	   {
		   if (temp != " ")
			{
			   if (temp != "")
			   {
				   tokens.push_back(temp);
			   }
			}

		   temp = megaWord[i];
		   tokens.push_back(temp);
		   temp = "";
	   }
	   else
	   {
		   temp += megaWord[i];
		   if (i == megaWord.size() - 1) tokens.push_back(temp);
	   }
   }

   // Walks the vector of tokens, takes variables and operators
   // and uses stacks to store the components of the postfix notation
   std::vector<string>::iterator it;
   
	for (it = tokens.begin(); it != tokens.end(); it++)
	{
		if (
            ((*it) >= "1" && (*it) <= "9") ||
            ((*it) >= "a" && (*it) <= "z") ||
            (*it).size() > 1
         )       
		{
			postfix.push_back((*it));
		}
		else
		{
			switch ((*it)[0])
			{
			case ' ':
				break;

			case '+': case '-':
			case '*': case '/':
			case '^': case '%':
				while (!operators.empty() && weightPostfixOperator((*it)[0]) <= weightPostfixOperator(operators.top()))
				{
					std::string _tmpstr = " ";
					_tmpstr[0] = operators.top();
					postfix.push_back(_tmpstr);
					operators.pop();
				}
				operators.push((*it)[0]);
				break;

			case '(':
				operators.push((*it)[0]);
				break;

			case ')':
				while (operators.top() != '(')
				{
					// create temporary string to convert char into string
					std::string _tmpstr = " ";
					_tmpstr[0] = operators.top();

					postfix.push_back(_tmpstr);
					operators.pop();
				}
				if (operators.top() == '(')
					operators.pop();
				break;

			default:
				postfix.push_back("" + (*it)[0]);
				break;
			}
		}
	}

	int iInfix = 0;

	while (!operators.empty())
	{
		// create temporary string to convert char into string
		std::string _tmpstr = " ";
		_tmpstr[0] = operators.top();

		postfix.push_back(_tmpstr);
		operators.pop();
	}

   // Builds the final postfix notation from all the
   // tokens (variables and operators)
	std::vector<string>::iterator jt;
	int x = 0;
	for (jt = postfix.begin(); jt != postfix.end(); jt++)
	{
		_postfix += (*jt);
		_postfix += " ";
	}
   _postfix.erase(_postfix.end()-1, _postfix.end());

	return _postfix;
}

/*****************************************************
 * TEST INFIX TO POSTFIX
 * Prompt the user for infix text and display the
 * equivalent postfix expression
 *****************************************************/
void testInfixToPostfix()
{
	string input;
	cout << "Enter an infix equation.  Type \"quit\" when done.\n";

	do
	{
		// handle errors
		if (cin.fail())
		{
			cin.clear();
			cin.ignore(256, '\n');
		}

		// prompt for infix
		cout << "infix > ";
		getline(cin, input);

		// generate postfix
		if (input != "quit")
		{
			string postfix = convertInfixToPostfix(input);
         // added a space here so we can pass the testBed
			cout << "\tpostfix:  " << postfix << endl << endl;
		}
	} while (input != "quit");
}

/**********************************************
 * CONVERT POSTFIX TO ASSEMBLY
 * Convert postfix "5 2 +" to assembly:
 *     LOAD 5
 *     ADD 2
 *     STORE VALUE1
 **********************************************/
string convertPostfixToAssembly(const string& postfix)
{
	string _assembly = "";
	custom::stack<char> variables;
	char variableName = 'A';
	custom::stack<string> _tmp;

	std::vector<string> tokens;
	string temp = "";

	for (unsigned int i = 0; i < postfix.size(); i++)
	{
		if (postfix[i] != ' ')
		{
			temp += postfix[i];
			if (i == postfix.size() - 1) tokens.push_back(temp);
		}
		else
		{
			tokens.push_back(temp);
			temp = "";
		}

	}

	std::vector<string>::iterator it;
	for (it = tokens.begin(); it != tokens.end(); it++)
	{
		std::string rhs;
		std::string lhs;

		if ((*it)[0] != ' ')
		{
			if ((*it).size() > 1 || ((*it) >= "0" && (*it) <= "9") || ((*it) >= "a" && (*it) <= "z"))
			{
				_tmp.push((*it));
			}
			else
			{
				if (_tmp.top() >= "A" && _tmp.top() <= "Z")
				{
					lhs = _tmp.top();
					_tmp.pop();
					rhs = _tmp.top();
					_tmp.pop();
				}
				else
				{
					rhs = _tmp.top();
					_tmp.pop();
					lhs = _tmp.top();
					_tmp.pop();
				}

				std::string _tmpstr = " ";
				_tmpstr[0] = variableName++;
				_tmp.push(_tmpstr);

				if ((((*it).size() == 1) && ((*it) >= "A" && (*it) <= "Z")) || (lhs >= "A" && lhs <= "Z")
					|| lhs >= "a" && lhs <= "z" || lhs == "pi" || lhs == "fahrenheit")
				{
					if (rhs == "pi" || rhs == "fahrenheit" || (rhs >= "A" && rhs <= "Z"))
					{
						_assembly += "\tLOD " + rhs;
						_assembly += '\n';
					}
					else
					{
						_assembly += "\tLOD " + lhs;
						_assembly += '\n';
					}

				}
				else
				{
					_assembly += "\tSET " + lhs;
					_assembly += '\n';
				}

				string assemblyText;
				switch ((*it)[0])
				{
				case '+':
					assemblyText = "\tADD ";
					break;
				case '-':
					assemblyText = "\tSUB ";
					break;
				case '*':
					assemblyText = "\tMUL ";
					break;
				case '/':
					assemblyText = "\tDIV ";
					break;
				case '%':
					assemblyText = "\tMOD ";
					break;
				case '^':
					assemblyText = "\tEXP ";
					break;
				}

				if (rhs == "pi" || rhs == "fahrenheit" || (rhs >= "A" && rhs <= "Z"))
				{
					_assembly += assemblyText + lhs;
				}
				else
				{
					_assembly += assemblyText + rhs;
				}
				_assembly += '\n';
				_assembly += "\tSAV ";
				_assembly += _tmp.top();
				_assembly += '\n';
			}

		}
	}

	return _assembly;
}


/*****************************************************
 * TEST INFIX TO ASSEMBLY
 * Prompt the user for infix text and display the
 * resulting assembly instructions
 *****************************************************/
void testInfixToAssembly()
{
	string input;
	cout << "Enter an infix equation.  Type \"quit\" when done.\n";

	do
	{
		// handle errors
		if (cin.fail())
		{
			cin.clear();
			cin.ignore(256, '\n');
		}

		// prompt for infix
		cout << "infix > ";
		getline(cin, input);

		// generate postfix
		if (input != "quit")
		{
			string postfix = convertInfixToPostfix(input);
			cout << convertPostfixToAssembly(postfix);
		}
	} while (input != "quit");
}

/******************************************
 * int weightPostfixOperator
 * Returns an index of operator priority
 ******************************************/
int weightPostfixOperator(const char &opcode)
{
   switch (opcode)
   {
      case '^':
         return 3;
         break;

      case '*':
      case '/':
      case '%':
         return 2;
         break;

      case '+':
      case '-':
         return 1;
         break;
   
      default:
         return 0;
         break;
   }
}