/*************************************************************
 *
 *  NAME: Brother Cameron
 *  Modified by Brother Ercanbrack 3 Aug 2006
 *  FILE: tree.h
 *
 *  PURPOSE: This file defines the Binary Tree Class
 *
 ************************************************************/
#ifndef _TREE_H
#define _TREE_H
using namespace std;
#include <iostream>

class BTree
{
 private:
  int data;          // --- data stored in node of tree
  BTree* left;       // --- pointer to left subtree
  BTree* right;      // --- pointer to right subtree
  BTree* parent;     // --- pointer to the parent node;

 public:
  BTree(int = 0);  
  ~BTree(void);
  void Insert_Left(int item);     // insert node to left
  void Insert_Right(int item);    // insert node to right
  BTree* LChild(void);            // return the ptr to the left child
  BTree* RChild(void);            // return the ptr to the right child
  BTree* Parent(void);            // return parent
  int Get_Data(void);
  void Set_Data(int item);
  void Set_Left(BTree* tree);    
  void Set_Right(BTree* tree);
  void Infix(void);              // do infix traversal
  void Prefix(void);             // do prefix traversal
  void Postfix(void);            // do postfix traversal
  void Level(void);              // do level order traversal
};
#endif
