/************************************************************
 *
 *  NAME: Brother Cameron
 * 
 *  PURPOSE: Example of pointers
 *
 ************************************************************/
using namespace std;
#include <iostream>

class Node
{
private:
  int data;
  Node *next;

public:
  Node();
  void setData(int item);
  int getData(void);
  void setNext(Node *n);
  Node *getNext(void);
};

main()
{
  Node *ptr;  // --- pointer to a Node object
  Node *temp;
  Node *list;

  list = new Node;    
  list->setData(15);

  temp = new Node;
  temp->setData(20);
  ptr = temp;
  list->setNext(temp);

  temp = new Node;
  temp->setData(25);
  ptr->setNext(temp);
  ptr = temp;

  ptr = list;
  while (ptr != NULL)
  {
    cout << ptr->getData() << " ";
    ptr = ptr->getNext();
  }
  cout << endl;

 
}

/**************************************************************
 *  Constructor for node class
 *
 **************************************************************/
Node::Node()
{
  data = 0;
  next = NULL;
}

/**************************************************************
 *  Accessor function
 *
 **************************************************************/
int Node::getData()
{
  return data;
}

/**************************************************************
 *  Mutator function
 *
 **************************************************************/
void Node::setData(int item)
{
  data = item;
}

/**************************************************************
 *  Mutator function
 *
 **************************************************************/
void Node::setNext(Node *n)
{
  next = n;
}

/**************************************************************
 *  Accessor function
 *
 **************************************************************/
Node *Node::getNext(void)
{
  return next;
}


