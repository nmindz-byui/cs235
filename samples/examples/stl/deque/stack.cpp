/***********************************************************************
* Program:
*    Project 05, deque container
*    Brother Cameron, CS 235
* Author:
*    Greg Cameron
* Summary: 
*    Program that uses a stack to convert the base-ten
*    representation of a positive integer to base two.
*    Uses the standard C++ stack container.
************************************************************************/
#include <iostream>
#include <deque>
#include <stack>
#include <cstdlib>
using namespace std;

/* --- main program
 */
main(int argc, char *argv[])
{
   int number;                        // the number to be converted
   int base;
   int remainder;                     // remainder when number is divided by 2
   stack < int > stackOfRemainders;   // stack of remainders   

   number = atoi(argv[1]);
   base = atoi(argv[2]);

   while (number != 0)
   {
      remainder = number % base;
      stackOfRemainders.push(remainder);
      number /= base;
   }
      
   cout << "Base " << base << " representation: ";
   while (!stackOfRemainders.empty() )
   {
      remainder = stackOfRemainders.top();
      stackOfRemainders.pop();
      cout << remainder;
   }
   cout << "\n";
   
}
