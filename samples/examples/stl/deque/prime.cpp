/***********************************************************************
* Program:
*    Project 05, deque container
*    Brother Cameron, CS 235
* Author:
*    Greg Cameron
* Summary: 
*    Example program for using the deque to determine if a number is
*    prime.
***********************************************************************/
#include <deque>
#include <vector>
#include <list>
#include <stack>
#include <cstdlib>
#include <iostream>
using namespace std;

/* run program as a.out number where number is the input number
 */
main(int argc, char *argv[])
{
   unsigned int number;
   stack < unsigned int, list < unsigned int > > st;
   
   number = atoi(argv[1]);
   
   cout << number << "= ";
   for (int n = 2; n <= number; ++n)
   {
      while (number % n == 0)
      {
         st.push(n);
         number /= n;
      }
   }
   
   while (1)
   {
      cout << st.top();
      st.pop();
      if (st.empty())
         break;
      cout << " * ";
   }
   cout << endl;
}
