/************************************************
 * Program:
 *    Homework 0, Vector 1 Example
 *    Cameron, CS 235
 * Author:
 *    Brother Cameron
 * Summary:
 *    Example of declaring and using vector container
 ************************************************/
#include <vector>
#include <string>
#include <iostream>
using namespace std;

class Temp
{
private:
  int a;
public:
  Temp();
  void print();
};

main()
{
  vector<double> vec1;
  vector<string> vec2;
  vector<Temp> vec3;

}

