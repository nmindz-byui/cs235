/******************************************************************************
 *
 *  File: poly.cpp
 *
 *  Author: Brother Cameron, 10/9/2001
 *
 *  Purpose: Implements a polynomial using STL
 *
 ******************************************************************************/
using namespace std;
#include <iostream>
#include <fstream>
#include <list>

class Term
{
 private:
  float coeff;
  int expon;
 
 public:
  Term();                  // --- Creates empty Term
  Term(float c, int e);    // --- Creates Term object with coeff. c and expon. e
  float get_coeff(void);   // --- returns coeff of term
  int get_expon(void);     // --- returns expon of term
  void set_coeff(float c); // --- sets term's coeff. to c
  void set_expon(int c);   // --- sets term's expon to c
};

main(int argc, char *argv[])
{
  list<Term> poly1;
  ifstream inFile(argv[1]);
  int totalTerms;            //number of terms in the polynomial

  inFile >> totalTerms;
  for (int i = 0; i < totalTerms; i++)
    {
      float c;
      int e;
      inFile >> c;             // --- coefficient
      inFile >> e;             // --- exponent
      Term t(c, e);
      if (poly1.size() == 0)   // --- list is empty
	poly1.push_front(t);   // --- insert first term at beginning of list
      else
	{
	  // --- insert polynomial in correct place
	  list<Term>::iterator i = poly1.begin();
	  while (i != poly1.end())
	    {
	      if (e == i->get_expon())
		i->set_coeff(i->get_coeff() + c);
	      else if (e > i->get_expon())
		poly1.insert(i,t);
	      else
		i++;
	    }
	  if (i == poly1.end())
	    poly1.push_back(t);
	}
    }
  inFile.close();
  
  // --- print out polynomial
  list<Term>::iterator i = poly1.begin();
  float num = i->get_coeff();
  int exp = i->get_expon();
  if (num != 1 && num != -1 || exp == 0)
    cout << num;
  else if (num == -1)
    cout << "-";
  if (exp > 1)
    cout << "x^" << exp;
  else if (exp == 1)
    cout << "x";
  ++i;

  while (i != poly1.end())
  {
    num = i->get_coeff();
    exp = i->get_expon();
    if (num < 0)
      {
	cout << " - ";
	if (num != 1 && num != -1 || exp == 0)
	  cout << -num;
      }
    else
      {
	cout << " + ";
	if (num != 1 && num != -1 || exp == 0)
	  cout << num;
      }
    if (exp > 1)
      cout << "x^" << exp;
    else if (exp == 1)
      cout << "x";
    ++i;
  }
  cout << endl;

}
  


/*****************************************************************************
 *
 *  Function: Term
 *
 *  Parameters - none
 *
 *  Return Value - none
 *
 *  Purpose - Constructor function for Term class.  Creates empty Term.
 *
 ****************************************************************************/
Term::Term(void)
{
  coeff = 0.0;
  expon = -1;  // --- guarentees insert will occur
}

/*****************************************************************************
 *
 *  Function: Term
 *
 *  Parameters - a float that is the coefficent and a int that is the expon.
 *
 *  Return Value - None
 *
 *  Purpose: Creates a Term object set to coefficent and expon.
 *
 ****************************************************************************/
Term::Term(float c, int e)
{
  coeff = c;
  expon = e;
}

/*****************************************************************************
 *
 *  Function: get_coeff
 *
 *  Parameters: None
 *
 *  Return Value - an integer representing coeff.
 *
 *  Purpose: Returns the coefficent data member of Term class
 *
 ****************************************************************************/
float Term::get_coeff(void)
{
  return(coeff);
}

/***************************************************************************
 *
 *  Function: get_expon
 *
 *  Parameters - none
 *
 *  Return value - integer, representing expon.
 *
 *  Purpose: Returns the expon data member of Term class
 *
 **************************************************************************/
int Term::get_expon(void)
{
  return(expon);
}

/***************************************************************************
 *
 *  Function: set_coeff
 *
 *  Parameters - float representing coeff
 *
 *  Return value - none
 *
 *  Purpose: sets c to data member coeff in Term class
 *
 **************************************************************************/
void Term::set_coeff(float c)
{
  coeff = c;
}

/***************************************************************************
 *
 *  Function: set_expon
 *
 *  Parameters - int representing expon
 *
 *  Return value - none
 *
 *  Purpose: sets e to data member expon in Term class
 *
 **************************************************************************/
void Term::set_expon(int e)
{
  expon = e;
}
