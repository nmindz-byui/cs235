#include <iostream>
#include <fstream>
#include <set>
#include <algorithm>
#include <iomanip>
#include <cstdlib>
#include <time.h>
#include <math.h>
using namespace std;

void print_set(set<int> A);

main(int argc, char *argv[])
{
  set<int> A;
  set<int> B;
  set<int> C;
  set<int>::iterator a;
  set<int>::iterator b;
  set<int>::iterator c;
  int i = 1;
  ifstream infile(argv[1]);
  int num = 0;
  int data = 0;

  // --- Read first data set
  infile >> num;
  for (i = 0; i < num; ++i)
  {
    infile >> data;
    A.insert(data);
  }

  // --- Read second data set
  infile >> num;
  for (i = 0; i < num; ++i)
  {
    infile >> data;
    B.insert(data);
  }

  // --- Output Set A and B
  cout << "Set A: " << endl;
  print_set(A);

  cout << "Set B: " << endl;
  print_set(B);

  set_intersection(A.begin(), A.end(), B.begin(), B.end(), inserter(C,C.begin()));
  cout << "Intersection of A and B: " << endl;
  print_set(C);
  C.clear();
  
  set_union(A.begin(), A.end(), B.begin(), B.end(), inserter(C,C.begin()));
  cout << "Union of A and B: " << endl;
  print_set(C);
  C.clear();

  set_difference(A.begin(), A.end(), B.begin(), B.end(), inserter(C,C.begin()));
  cout << "Difference of A and B: " << endl;
  print_set(C);
  C.clear();

  set_difference(B.begin(), B.end(), A.begin(), A.end(), inserter(C,C.begin()));
  cout << "Difference of B and A: " << endl;
  print_set(C);
}

void print_set(set<int> A)
{
  set<int>::iterator a;  
  int i = 0;

  for (a = A.begin(); a != A.end(); a++)
  {
     cout << setw(3) << *a << " ";
     ++i;
     if (i == 10)
     {
        i = 0;
        cout << endl;
     }
  }
  cout << endl << endl;
}
