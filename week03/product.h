/***********************************************************************
 * Header:
 *    PRODUCT
 * Summary:
 *    This will contain the header and prototypes for Product.
 * Author
 *    Evandro Camargo, Alexandre Paixao & Caleb Georgeson
 ************************************************************************/
#include "dollars.h"    //DOLLARS
#include <iostream>    // for ISTREAM, OSTREAM, CIN, and COUT
#include <string>      // for STRING
#include <cassert>     // for ASSERT

class Product
{
   private:
      int qtyBuy;
      Dollars priceBuy;
      Dollars profit;

   public:
      Product();
      void add(int qty, Dollars price);
      int getQty();
      void setQty(int _quantity);
      Dollars getPrice();
      Dollars getProfit();
      void setProfit(Dollars value);
};