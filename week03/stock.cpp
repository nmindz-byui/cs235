/***********************************************************************
 * Implementation:
 *    STOCK
 * Summary:
 *    This will contain the implementation for stocksBuySell() as well
 *    as any other function or class implementation you need
 * Author
 *    Evandro Camargo, Alexandre Paixao & Caleb Georgeson
 **********************************************************************/

#include <iostream>     // for ISTREAM, OSTREAM, CIN, and COUT
#include <string>       // for STRING
#include <cassert>      // for ASSERT
#include "stock.h"      // for STOCK_TRANSACTION
#include "queue.h"      // for QUEUE
#include "sstream"      // for STRING BUFFER
#include "product.cpp"  // for PRODUCTS

using namespace std;

/************************************************
 * STOCKS BUY SELL
 * The interactive function allowing the user to
 * buy and sell stocks
 ***********************************************/

/*******************************************************************
* stocksBuySell
* Handles the Buying and Selling of Stock
********************************************************************/
void stocksBuySell()
{
   // Variables
   int totalStock = 0;                    // Total Sock
   custom::queue <Product> itemsBuy;      // Items bought
   custom::queue <Product> itemsSold;     // Items sold
   Dollars profit;
   
   // Instructions
   cout << "This program will allow you to buy and sell stocks. "
        << "The actions are:\n";
   cout << "  buy 200 $1.57   - Buy 200 shares at $1.57\n";
   cout << "  sell 150 $2.15  - Sell 150 shares at $2.15\n";
   cout << "  display         - Display your current stock portfolio\n";
   cout << "  quit            - Display a final report and quit the program\n";

   // Portfolio
   cout << "> ";
   std::cin.ignore();
   std::string line;

   while (getline(std::cin, line))
   {
      int piracanjuba = getTotalItems(itemsBuy);
      // Primary stack of variables
      int shares;
      Dollars price;
      stringstream ssValues(line);
      string option;
      
      // Handles user menu input
      ssValues >> option >> shares >> price;

      // MENU :: BUY OPTION
      if (option == "buy")
      {
         Product p;
         p.add(shares, price);

         itemsBuy.push(p);
      }
      // MENU :: SELL OPTION
      else if (option == "sell" && piracanjuba > 0)
      {
         int totalSell = getTotalItems(itemsBuy);

         if (shares > totalSell)
         {
            // Only sells what you own
            shares = totalSell;
         }
         
         // Executes the selling action and
         // calculates stock profits
         Product pSell = itemsBuy.front();
         int qtyTemp = shares;
         do
         {
            Product p;
            p.add(qtyTemp, price);
            itemsSold.push(p);

            if (pSell.getQty() >= p.getQty())
            {
               Dollars profit = (p.getPrice() - pSell.getPrice()) * p.getQty();
               itemsSold.pBack()->setProfit(profit);

               // Delete the products if your quantity is 0.
               if (pSell.getQty() == p.getQty())
               {
                  itemsBuy.pop();
               }
               else 
               {
                  // Change quantity of products for
                  // stock sales that split the amount
                  itemsBuy.pBack()->setQty(itemsBuy.front().getQty() - qtyTemp);
               }
               shares -= p.getQty();
            }
            else
            {
               Dollars profit = (p.getPrice() - pSell.getPrice()) * pSell.getQty();
               itemsSold.pBack()->setProfit(profit);
               itemsSold.pBack()->setQty(pSell.getQty());

               itemsBuy.pop();

               qtyTemp = p.getQty() - pSell.getQty();
               shares -= pSell.getQty();

               if (!itemsBuy.empty()) pSell = itemsBuy.front();
            }
         }
         while (pSell.getQty() >= shares && shares > 0);
      }
      // MENU :: DISPLAY OPTION
      else if (option == "display")
      {
         // _temp is declared as a copy of the list of items to be bought
         custom::queue<Product> _temp = itemsBuy;

         // If the stock queue is not empty,
         // then proceed displaying its contents
         if (!itemsBuy.empty()) 
         {
            // 1. Bought Shares
            if (!_temp.empty()) std::cout << "Currently held:\n";
            while (!_temp.empty())
            {
               Product pTemp = _temp.front();
               std::cout
                  << "\tBought " << pTemp.getQty()
                  << " shares at " << pTemp.getPrice()
                  << "\n";
               _temp.pop();
            }
            
         }

         if (!itemsSold.empty())
         {
            // _temp now becomes a copy of the list of items to be sold
            _temp = itemsSold;
            // 2. Sold Shares
            if (!_temp.empty()) std::cout << "Sell History:\n";
            while (!_temp.empty())
            {
               Product pTemp = _temp.front();
               std::cout << "\tSold " << pTemp.getQty() << " shares at " << pTemp.getPrice() << " for a profit of " << pTemp.getProfit() << "\n";   
               _temp.pop();
            }
         }
         
         // 3. Displays the Profits
         std::cout << "Proceeds: " << getTotalProfit(itemsSold) << "\n";
      }
      // MENU :: QUIT OPTION
      else if (option == "quit")
      {
         break;
      }
      // MENU :: INVALID OPTION
      else if (option != "")
      {
         std::cout << "Unrecognized command, try again.\n";
      }

      // Displays an angle bracket before the user prompt
      // to make it clearly distinguishable from output
      std::cout << "> ";
   }
}

/*******************************************************************
* getTotalItems
* Returns the total count of items
********************************************************************/
template<class T>
int getTotalItems(const custom::queue<T> &q)
{
   custom::queue<T> pQ = q;
   int total = 0;
   while (!pQ.empty())
   {
      Product p = pQ.front();
      total += p.getQty();
      pQ.pop();
   }
   return total;
}

/*******************************************************************
* getTotalProfit
* Returns the total profit of items
********************************************************************/
template<class T>
Dollars getTotalProfit(const custom::queue<T> &q)
{
   custom::queue<T> pQ = q;
   Dollars total = 0;
   while (!pQ.empty())
   {
      Product p = pQ.front();
      total += p.getProfit();
      pQ.pop();
   }

   return total;
}

