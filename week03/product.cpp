/***********************************************************************
 * Class:
 *    PRODUCT
 * Summary:
 *    This will contain the implementation of Product.
 * Author
 *    Evandro Camargo, Alexandre Paixao & Caleb Georgeson
 ************************************************************************/

#include "product.h"

/*******************************************************************
* Product :: Product
* Default Constructor
********************************************************************/
Product :: Product()
{
   this->qtyBuy = 0;
   this->priceBuy = 0.0;
   this->profit = 0.0;
}

/*******************************************************************
* Product :: add
* Pushes (adds) a new item to the Product object
********************************************************************/
void Product :: add(int qty, Dollars price)
{
   this->qtyBuy = qty;
   this->priceBuy = price;
}

/*******************************************************************
* Product :: getQty
* Returns the quantity of products
********************************************************************/
int Product :: getQty()
{
   return this->qtyBuy;
}

/*******************************************************************
* Product :: setQty
* Returns the quantity of products
********************************************************************/
void Product :: setQty(int _quantity)
{
   this->qtyBuy = _quantity;
}

/*******************************************************************
* Product :: getProfit
* Returns the value of profit
********************************************************************/
Dollars Product :: getProfit()
{
   return this->profit;
}

/*******************************************************************
* Product :: getPrice
* Returns the buy price
********************************************************************/
Dollars Product :: getPrice()
{
   return this->priceBuy;
}

/*******************************************************************
* Product :: setProfit
* Sets the profit value
********************************************************************/
void Product :: setProfit(Dollars value)
{
   this->profit = value;
}