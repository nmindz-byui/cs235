/***********************************************************************
 * Header:
 *    STOCK
 * Summary:
 *    This will contain just the prototype for stocksBuySell(). You may
 *    want to put other class definitions here as well.
 * Author
 *    Evandro Camargo, Alexandre Paixao & Caleb Georgeson
 ************************************************************************/

#ifndef STOCK_H
#define STOCK_H

#include "dollars.h"   // for Dollars defined in StockTransaction
#include "queue.h"     // for QUEUE
#include <iostream>    // for ISTREAM and OSTREAM

/*******************************************************************
* stocksBuySell
* Handles the Buying and Selling of Stock
********************************************************************/
void stocksBuySell();

/*******************************************************************
* getTotalItems
* Returns the total count of items
********************************************************************/
template<class T>
int getTotalItems(const custom::queue<T> &q);

/*******************************************************************
* getTotalProfit
* Returns the total profit of items
********************************************************************/
template<class T>
Dollars getTotalProfit(const custom::queue<T> &q);

#endif // STOCK_H