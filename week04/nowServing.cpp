/***********************************************************************
 * Implementation:
 *    NOW SERVING
 * Summary:
 *    This will contain the implementation for nowServing() as well as any
 *    other function or class implementations you may need
 * Author
 *    Evandro Camargo, Alexandre Paixao & Caleb Georgeson
 **********************************************************************/

#include <iostream>     // for ISTREAM, OSTREAM, CIN, and std::cout
#include <string>       // for STRING
#include <cassert>      // for ASSERT
#include "nowServing.h" // for nowServing() prototype
#include "deque.h"      // for DEQUE
#include "sstream"     // for STRING BUFFER

using namespace std;

/************************************************
 * This is a structure for request
 ***********************************************/
struct Request
{
	bool emergency;
	string Class;
	string Name;
	int Time;
};

/************************************************
 * NOW SERVING
 * The interactive function allowing the user to
 * handle help requests in the Linux lab
 ***********************************************/
void nowServing()
{
   // instructions
   std::cout << "Every prompt is one minute.  The following input is accepted:\n";
   std::cout << "\t<class> <name> <#minutes>    : a normal help request\n";
   std::cout << "\t!! <class> <name> <#minutes> : an emergency help request\n";
   std::cout << "\tnone                         : no new request this minute\n";
   std::cout << "\tfinished                     : end simulation\n";

   // your code here
   custom::deque <Request> requestList;
   Request newRequest;
   Request currentRequest;

   //initialize the currentRequest time to 0
   currentRequest.Time = 0;

   int numSolicitation = 0;
 	string option;
   cin.ignore();
   do 
   {
      string line;

      std::cout << "<";
      std::cout << numSolicitation++;
      std::cout << "> ";
      
      getline(std::cin, line);

      stringstream ssValues;
      ssValues << line;
      ssValues >> option;
      if (option != "finished")
      {
        if (option == "!!") 
        {
          newRequest.emergency = true;
          ssValues >> newRequest.Class >> newRequest.Name >> newRequest.Time;

          requestList.push_front(newRequest);

        }
        else if (option == "none"){}
        else
        {
          try
          {
            newRequest.Class = option;
              ssValues >> newRequest.Name >> newRequest.Time;
              newRequest.emergency = false;

              requestList.push_back(newRequest);

          }
          catch(const std::exception& e)
          {
              std::cout << "Sorry this is not option, try again!\n";
          }
          

        }
        
        if (currentRequest.Time <= 0 && !requestList.empty())
        {
          currentRequest = requestList.front();
          requestList.pop_front();
        }
        
        if (currentRequest.Time > 0)
        {
          if (currentRequest.emergency)
          {
              std::cout << "\tEmergency for "; 
              std::cout << currentRequest.Name; 
              std::cout << " for class "; 
              std::cout << currentRequest.Class;
              std::cout << ". Time left: "; 
              std::cout << currentRequest.Time-- << endl;
          }
          else 
          {
              std::cout << "\tCurrently serving "; 
              std::cout << currentRequest.Name; 
              std::cout << " for class "; 
              std::cout << currentRequest.Class;
              std::cout << ". Time left: "; 
              std::cout << currentRequest.Time-- << endl;
          }
        }
      }
    } while(option != "finished");

   // end
   std::cout << "End of simulation\n";
}


