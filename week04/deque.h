/***********************************************************************
 * Header:
 *    DEQUE
 * Summary:
 *    This will contain the implementation of the templated
 *    abstract data structure for a "DEQUE".
 * Author
 *    Evandro Camargo, Alexandre Paixao & Caleb Georgeson
 ************************************************************************/

#ifndef DEQUE_H
#define DEQUE_H // DEQUE_H

#define ALLOC_SIZE_STD 1

#include <iostream>

namespace custom
{
   template<class T>
   class deque
   {
      private:
         // Variables
         T* array;
         int iBack;
         int iFront;
         int numCapacity;
         // Methods
         void resize(int _numCapacity);
         int iNormalize(int iRelative) const;
         int iFrontNormalize() const;
         int iBackNormalize() const;

      public:
         // Constructors
         deque();
         deque(int _numCapacity);
         deque(const deque &rhs);
         // Destructor
         ~deque();
         // Overloaded operators
         deque<T>& operator=(const deque &rhs);
         // Methods
         int size() const;
         void clear();
         bool empty() const;
         void push_back(T t);
         void pop_back();
         void push_front(T t);
         void pop_front();
         T front() const;
         T back() const;
         void display() const;
   };

   // Constructors
   
   /******************************************************************
    * deque :: deque
    * Default Constructor
    *******************************************************************/
   template<class T>
   deque <T> :: deque()
   {
      this->iBack = 0;
      this->iFront = 0;
      this->numCapacity = ALLOC_SIZE_STD;
      this->array = new T[this->numCapacity];
   }

   /*******************************************************************
    * deque :: deque
    * Non-default Constructor
    ********************************************************************/
   template<class T>
   deque <T> :: deque(int _numCapacity)
   {
      try
      {
         this->iBack = 0;
         this->iFront = 0;
         this->numCapacity = _numCapacity;
         this->array = new T[this->numCapacity];
      }
      catch (std::bad_alloc)
      {
          throw "ERROR: unable to allocate a new buffer for deque.";
      }
   }

   /*******************************************************************
    * deque :: deque
    * Copy Constructor
    ********************************************************************/
   template<class T>
   deque <T> :: deque(const deque &rhs)
   {
      try
      {
         this->iFront = rhs.iFront;
         this->iBack = rhs.iBack;
         this->numCapacity = rhs.numCapacity;
         this->array = new T[rhs.numCapacity];

         for (int i = 0; i < rhs.numCapacity; i++)
         {
            this->array[i] = rhs.array[i];
         }
      }
      catch (std::bad_alloc)
      {
          throw "ERROR: unable to allocate a new buffer for deque.";
      }
   }
   
   /*******************************************************************
    * deque :: ~deque
    * Destructor
    ********************************************************************/
   template<class T>
   deque <T> :: ~deque()
   {
      delete[] this->array;

      this->array = NULL;
   }

   // Overloaded operators
   /*******************************************************************
    * deque :: operator =
    * Assignment Operator Overload
    ********************************************************************/
   template<class T>
   deque<T>& deque<T> :: operator=(const deque &rhs)
   {
      try
      {
         if (this->numCapacity < rhs.size())
         {
            this->numCapacity = rhs.size();
            this->resize(rhs.size());
         }
        
        this->iFront = 0;
        this->iBack = 0;

         for (int i = rhs.iFront; i < rhs.iBack; i++)
         {
            this->push_back(rhs.array[i % rhs.numCapacity]);
         }
      }
      catch (std::bad_alloc)
      {
         throw "ERROR: unable to allocate a new buffer for deque.";
      }

      return *this;
   }

   // Public member functions
   /*******************************************************************
    * deque :: size
    * Returns the size of the Deque
    ********************************************************************/
   template<class T>
   int deque <T> :: size() const
   {
      return this->iBack - this->iFront;
   }

   /*******************************************************************
    * deque :: clear
    * Empties the Deque of all elements
    ********************************************************************/
   template<class T>
   void deque <T> :: clear()
   {
      delete [] this->array;
      this->array = new T[this->numCapacity];
      this->iBack = 0;
      this->iFront = 0;
   }

   /*******************************************************************
    * deque :: empty
    * Test whether the Deque is empty
    ********************************************************************/
   template<class T>
   bool deque <T> :: empty() const
   {
      if (this->size() == 0)
      {
         return true;
      }
      else
      {
         return false;
      }
   }

   /*******************************************************************
    * deque :: push_back
    * Adds an element to the back of the Deque
    ********************************************************************/
   template<class T>
   void deque <T> :: push_back(T t)
   {
      // Checks if the array needs to be resized
      // prior to pushing data to the tail
      if (this->size() == this->numCapacity)
      {     
         resize(this->numCapacity);
      }

      this->iBack++;

      // As it normally would, just add the
      // element to the tail of the Deque
      this->array[this->iBackNormalize()] = t;
   }

   /*******************************************************************
    * deque :: push_front
    * Adds an element to the front of the Deque
    ********************************************************************/
   template<class T>
   void deque <T> :: push_front(T t)
   {
      // Checks if the array needs to be resized
      // prior to pushing data to the head
      if (this->size() == this->numCapacity)
      {     
         resize(this->numCapacity);
      }

      this->iBack++;

      // Creates a temporary placeholder array
      T* _array = new T[this->numCapacity];

      // Fills the HEAD of the array with passed argument
      _array[this->iFrontNormalize()] = t;

      // Adds every element from current to placeholder array
      for(int i = 1; i <= (iBackNormalize() - iFrontNormalize()); i++)
      {
         _array[i] = this->array[i - 1];
      }

      // Discards old data
      delete[] this->array;

      // Assigns current array with normalized placeholder
      this->array = _array;

   }

   /*******************************************************************
    * deque :: pop_back
    * Removes an element from the head of the Deque
    ********************************************************************/
   template<class T>
   void deque <T> :: pop_back()
   {
      if (!this->empty())
      {
         this->iBack--;
      }

      if (this->size() == 0)
      {
        this->clear();
      }
   }

   /*******************************************************************
    * deque :: pop_front
    * Removes an element from the head of the Deque
    ********************************************************************/
   template<class T>
   void deque <T> :: pop_front()
   {
      if (!this->empty())
      {
         this->iFront++;
      }

      if (this->size() == 0)
      {
        this->clear();
      }
   }

   /*******************************************************************
    * deque :: front
    * Returns the element that is currently at the front of the Deque
    ********************************************************************/
   template<class T>
   T deque <T> :: front() const
   {
      if (this->empty())
      {
         throw "ERROR: unable to access data from an empty deque";
      }
      else
      {
         return this->array[this->iFrontNormalize()];
      }
   }

   /*******************************************************************
    * deque :: back
    * Returns the element that is currently at the back of the Deque
    ********************************************************************/
   template<class T>
   T deque <T> :: back() const
   {
      if (this->empty())
      {
         throw "ERROR: unable to access data from an empty deque";
      }
      else
      {
         return this->array[this->iBackNormalize()];
      }
   }

   // Private member functions
   /*******************************************************************
    * deque :: resize
    * Allocates more memory as needed
    ********************************************************************/
   template<class T>
   void deque <T> :: resize(int _numCapacity)
   {
      // calc capacity size
      int remainder = _numCapacity % ALLOC_SIZE_STD;
      int _capacity = 0;

      if (this->numCapacity >= ALLOC_SIZE_STD)
      {
         _capacity = this->numCapacity * 2;
      }
      else if (remainder == 0)
      {
         // keep _capacity the same
      }
      else
      {
         _capacity = _numCapacity + ALLOC_SIZE_STD - remainder;
      }

      try
      {
         T* _array = new T[_capacity]; // allocate new temporary array

         // move data from old array to new tmp array
         for (int i = this->iFront, j = 0; i < this->iBack; i++, j++)
         {
            _array[j] = this->array[i % this->numCapacity];
         }
         delete[] this->array;

         // initialize new array with more capacity
         this->array = _array;
         this->iFront = 0;
         this->iBack = this->numCapacity;
         this->numCapacity = _capacity;
      }
      catch (std::bad_alloc)
      {
         throw "ERROR: unable to allocate a new buffer for deque.";
      }
   }

   /*******************************************************************
    * deque :: iNormalize
    * Returns a normalized value for the index (including negatives)
    ********************************************************************/
   template<class T>
   int deque <T> :: iNormalize(int iRelative) const
   {
      int _number = iRelative * (-1);
      _number *= this->numCapacity;   
      iRelative < 0 ? iRelative += _number : iRelative;

      return iRelative % this->numCapacity;
   }

   /*******************************************************************
    * deque :: iFrontNormalize
    * Returns the index of the first item of the Deque
    ********************************************************************/
   template<class T>
   int deque <T> :: iFrontNormalize() const
   {
      return iNormalize(this->iFront) % this->numCapacity;
   }

   /*******************************************************************
    * deque :: iBackNormalize
    * Returns the index of the last item of the Deque
    ********************************************************************/
   template<class T>
   int deque <T> :: iBackNormalize() const
   {
      return iNormalize((this->iBack - 1)) % this->numCapacity;
   }

   /*******************************************************
    * deque :: display
    * Helper function to display the Deque in a friendly format
    *******************************************************/
   template <class T>
   void deque <T> :: display() const
   {
      // display the header info
      std::cerr << "\ndeque<T>::display()\n";
      std::cerr << "\tnumCapacity = " << numCapacity << "\n";

      // display the contents of the array
      std::cerr << "\tdata = ";
      if (numCapacity == 0)
         std::cerr << "NULL";
      else
      {
         std::cerr << "{ ";
         for (int i = 0; i < numCapacity; i++)
         {
            if (i != 0)
               std::cerr << ", ";

            // not wrapped
            //      0   1   2   3   4   5   6
            //    +---+---+---+---+---+---+---+
            //    |   |   | A | B | C |   |   |
            //    +---+---+---+---+---+---+---+
            // iFront = 9     iFrontNormalize() = 2
            // iBack  = 11    iBackNormalize()  = 4
            if (iFrontNormalize() <= iBackNormalize() &&       // not wrapped
               iFrontNormalize() <= i &&
               i <= iBackNormalize())                          // in range
               std::cerr << this->array[i];

            // wrapped
            //      0   1   2   3   4   5   6
            //    +---+---+---+---+---+---+---+
            //    | B | C |   |   |   |   | A |
            //    +---+---+---+---+---+---+---+
            // iFront = -8    iFrontNormalize() = 6
            // iBack  = -6    iBackNormalize()  = 1
            else if (iFrontNormalize() > iBackNormalize() &&   // wrapped
                     size() != 0 &&                            // not empty
                     (i <= iBackNormalize() ||
                     i >= iFrontNormalize()))                  // in range
               std::cerr << this->array[i];
         }
         std::cerr << " }";
      }
      std::cerr << "\n";

      // display the front and back with the normalized values in ()s
      if (numCapacity)
      {
         std::cerr << "\tiFront = " << iFront
            << " ("          << iFrontNormalize() << ")\n";
         std::cerr << "\tiBack  = " << iBack
            << " ("          << iBackNormalize()  << ")\n";
      }
   }
}

#endif // DEQUE_H