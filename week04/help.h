/************************************************
 * 
 * The interactive function allowing the user to
 * buy and sell stocks
 ***********************************************/
#include <iostream>         // for ISTREAM, OSTREAM, CIN, and std::cout
#include <string>           // for STRING

namespace custom
{
    template<class T>
    class help
    {
        public:
        void help();
        bool emergency;     // request with emergency
        string course;      // which course
        string firstName;   // first name of student
        int time;           // time necessary

        help::void help(){}
    };

}