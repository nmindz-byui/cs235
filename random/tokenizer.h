/***********************************************************************
* Header:
*    Tokenizer      
 * Summary:
 *    Defines a class to work with characters and words and provides
 *    handful methods to deal with them
* Author
*    Evandro Camargo: 0 hours
*    Caleb Georgeson: 0 hours
*    Alexandre Paixao: 0 hours
************************************************************************/

#ifndef TOKENIZER_H
#define TOKENIZER_H

#include <string>
#include <vector>

namespace custom
{
   /************************************************
    * Tokenizer
    * Abstract Data Structure
    ***********************************************/
   class Tokenizer
   {
      private:
         std::string s;
         std::string word;
         std::vector<std::string> tokens;
      public:
         // Constructors and Destructor
         Tokenizer();
         Tokenizer(std::string &s);
         // Rule of Three
         ~Tokenizer();
         Tokenizer(const Tokenizer &rhs);   // Copy constructor
         Tokenizer(Tokenizer&&) = default;     // Move constructor
         // Operator Overloading
         Tokenizer& operator=(const Tokenizer &rhs);
         // Methods
         std::vector<std::string> generateTokens();
   };
}

#endif // TOKENIZER_H