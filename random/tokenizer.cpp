/*************************************************************
 * File: tokenizer.cpp
 * 
 * Author: Evandro Camargo
 * Author: Alexandre Paixao
 * Author: Caleb Georgeson
 *
 * Description: Implements Tokenizer Class
 *
 *************************************************************/

#include "tokenizer.h"
#include <string>
#include <vector>

/******************************************
* custom :: Tokenizer :: Tokenizer
* Default Constructor
******************************************/
custom :: Tokenizer :: Tokenizer()
{
   this->s = "";
   this->word = "";
}

/******************************************
* custom :: Tokenizer :: Tokenizer
* Default Constructor
******************************************/
custom :: Tokenizer :: Tokenizer(std::string &s)
{
   this->s = s;
   this->word = "";
}

/******************************************
* custom :: Tokenizer :: generateTokens
* Creates a vector of tokens
******************************************/
std::vector<std::string> custom :: Tokenizer :: generateTokens()
{
   std::string::iterator it;
   std::string _word = " ";

   for (it = this->s.begin(); it < this->s.end(); ++it)
   {
      if ((*it) != ' ')
      {
         _word += (*it);
      }
      else
      {
         this->tokens.push_back(_word);
         _word = " ";
      }
   }
}

/*******************************************
 * custom :: Tokenizer :: operator <<
 * Left shift operator for outputting to std::cout
 *******************************************/
std::ostream & operator << (std::ostream & out, custom::Tokenizer rhs)
{
   // we need to make a copy of the stack that is backwards
   std::vector<std::string> backwards;
   std::vector<std::string> tokens = rhs.generateTokens();

   std::vector<std::string>::iterator it = tokens.begin();
   
   std::cout << "{ ";

   for (it = tokens.begin(); it != tokens.end(); it++)
   {
      backwards
   }
   while (!rhs.empty())
   {
      backwards.push(rhs.top());
      rhs.pop();
   }

   // now we will display this one
   
   while (!backwards.empty())
   {
      out << backwards.top() << ' ';
      backwards.pop();
   }
   out << '}';

   return out;   
}