/***********************************************************************
 * Module:
 *    Week 11, Sort Select
 *    Brother Helfrich, CS 235
 * Author:
 *    Evandro Camargo, Alexandre Paixao & Caleb Georgeson
 * Summary:
 *    This program will implement the Selection Sort
 ************************************************************************/

#ifndef SORT_SELECTION_H
#define SORT_SELECTION_H // SORT_SELECTION_H

/****************************************************
* void - sortSelection
* Perform the selection sort
* **************************************************/
template <class T>
void sortSelection(T array[], int num)
{
   int iLargest;

   for (int iPivot = num - 1; iPivot > 0; iPivot--)
   {
      iLargest = 0;

      for (int iCheck = 1; iCheck < iPivot; iCheck++)
      {
         if (array[iLargest] <= array[iCheck])
         {
            iLargest = iCheck;
         }
      }
      if (array[iLargest] > array[iPivot])
      {
         T temp = array[iLargest];
         array[iLargest] = array[iPivot];
         array[iPivot] = temp;
      }
   }
}

#endif // SORT_SELECTION_H
