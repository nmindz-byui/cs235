/***********************************************************************
 * Component:
 *    Assignment 09, Binary Search Tree (BST)
 *    Brother Kirby, CS 235
 * Author:
 *    Evandro Camargo, Alexandre Paixao & Caleb Georgeson
 * Summary:
 *    Create a binary search tree
 ************************************************************************/

#ifndef BST_H
#define BST_H // BST_H

namespace custom
{
   template<class T>
   class BST
   {
      public:      
         // Binary node class
         class BNode;
         // Iterator class
         class iterator;
         class const_iterator;
         class reverse_iterator;
         // Constructors
         BST() : root(NULL), numElements(0) {};
         BST(const BST & rhs);
         // Destructor
         ~BST();
         // Operator Overloads
         BST operator = (const BST & rhs);
         // Standard Container Interfaces
         int size() const;
         bool empty() const;
         void clear();
         void insert(const T& t);
         void erase(iterator& it);
         iterator find(const T& t);
         // Iterator Related
         BNode* getRoot() const;
         BNode* getFirstElement() const;
         BNode* getLastElement() const;
         iterator begin();
         iterator end();
         iterator rbegin();
         iterator rend();
         reverse_iterator ribegin();
         reverse_iterator riend();
         const_iterator cbegin() const;
         const_iterator cend() const;
      private:
         // Private Data
         BNode* root;
         int numElements;
         // Private Methods
         BNode* insertRecursive(BNode* _pRoot, BNode* _newNode);
         void deleteNode(BNode _del, bool _right);
         void deleteBinaryTree(BNode* &_pNode);
         BNode* copyBinaryTree(const BNode* _pSrc);
   };

   template<class T>
   class BST <T> :: BNode
   {
      private:
      public:
         T data;
         BNode* pParent;
         BNode* pLeft;
         BNode* pRight;
         bool isRed;

         // Constructors
         BNode() : pLeft(NULL), pRight(NULL), pParent(NULL), data(), isRed(true) {}
         BNode(const T& t) : pLeft(NULL), pRight(NULL), pParent(NULL), data(t), isRed(true) {}

         // Node Inserts (L/R)
         void addLeft(BNode* _pNode, BNode* _pAdd);
         void addRight(BNode* _pNode, BNode* _pAdd);

         // Node Insers with new Node given T value
         void addLeft(BNode* _pNode, const T &_t);
         void addRight(BNode* _pNode, const T &_t);

         // Node Checking Helpers
         bool isRightChild(BNode* _pNode) const  { return pRight == _pNode; }
         bool isLeftChild(BNode* _pNode) const   { return pLeft == _pNode; }

         // Red-Black Tree Balancing
         void balance();
   };


   /********************************************************************
   * custom :: BST :: iterator
   * This is the class definition for the iterator class
   * ******************************************************************/
   template<class T>
   class BST <T> :: iterator
   {
      private:
         BST<T>::BNode* pNode;
         friend void BST<T>::erase(BST<T>::iterator& it);
      public:
         // Constructors & Destructor
         iterator() : pNode(NULL) {}
         iterator(BNode* pNode) : pNode(pNode)  {}
         iterator(const iterator& rhs)          { *this = rhs; }
         iterator(const reverse_iterator& rhs)  : pNode(rhs.getNodeRef()) {}
         // Public Getter in order to construct another type of iterator
         // out of an instance of a BST :: iterator.
         BNode* getNodeRef() const              { return this->pNode; }
         // Operator Overloads
         iterator& operator = (const iterator& rhs)
         {
            this->pNode = rhs.pNode;
            return *this;
         }
         iterator& operator = (const T &rhs)
         {
            this->pNode = rhs;
            return *this;
         }
         bool operator != (const iterator& rhs) const { return rhs.pNode != this->pNode; }
         bool operator == (const iterator& rhs) const { return rhs.pNode == this->pNode; }
         T&        operator * ()                      { return pNode->data;              }
         const T&  operator * ()                const { return pNode->data;              }
         iterator& operator ++ ();
         iterator& operator -- ();
         iterator  operator ++ (int postfix);
         iterator  operator -- (int postfix);
   };

   /********************************************************************
   * custom :: BST :: const_iterator
   * This is the class definition for the const_iterator class
   * ******************************************************************/
   template<class T>
   class BST <T> :: const_iterator
   {
      private:
         const BST<T>::BNode* pNode;
         friend void BST<T>::erase(BST<T>::const_iterator& it);
      public:
         // Constructors & Destructor
         const_iterator() : pNode(NULL) {}
         const_iterator(BNode* pNode) : pNode(pNode) {}
         const_iterator(const const_iterator& rhs) { *this = rhs; }
         // Public Getter in order to construct another type of iterator
         // out of an instance of a BST :: iterator.
         BNode* getNodeRef() const              { return this->pNode; }
         // Operator Overloads
         bool operator == (const const_iterator& rhs) const { return rhs.pNode == this->pNode; }
         bool operator != (const const_iterator& rhs) const { return rhs.pNode != this->pNode; }
         const T&        operator * ()                const { return pNode->data;              }
         const_iterator& operator ++ ();
         const_iterator& operator -- ();
         const_iterator  operator ++ (int postfix);
         const_iterator  operator -- (int postfix);
   };

   /********************************************************************
   * custom :: BST :: iterator
   * This is the class definition for the iterator class
   * ******************************************************************/
   template<class T>
   class BST <T> :: reverse_iterator
   {
      private:
         BST<T>::BNode* pNode;
      public:
         // Constructors & Destructor
         reverse_iterator() : pNode(NULL) {}
         reverse_iterator(BNode* pNode) : pNode(pNode) {}
         reverse_iterator(const reverse_iterator& rhs) { *this = rhs;             }
         // Public Getter in order to construct another type of iterator
         // out of an instance of a BST :: iterator.
         BNode* getNodeRef() const                     { return this->pNode;      }
         // Operator Overloads
         reverse_iterator& operator = (const reverse_iterator& rhs)
         {
            this->pNode = rhs.pNode;
            return *this;
         }
         bool operator != (const reverse_iterator& rhs)  const { return rhs.pNode != this->pNode; }
         bool operator == (const reverse_iterator& rhs)  const { return rhs.pNode == this->pNode; }
         T&        operator * ()                               { return pNode->data;              }
         const T&  operator * ()                         const { return pNode->data;              }
         reverse_iterator& operator ++ ();
         reverse_iterator& operator -- ();
         reverse_iterator  operator ++ (int postfix);
         reverse_iterator  operator -- (int postfix);
   };

   /* IMPLEMENTATION - custom :: BST<T> class */

   /**********************************************************************
    * custom :: BST<T> :: ctor
    * Copies one BST into another
    *********************************************************************/
   template<class T>
   BST<T> :: BST(const BST & rhs)
   {
      this->numElements = rhs.numElements;
      this->root = copyBinaryTree(rhs.root);
   }

   /**********************************************************************
    * custom :: BST<T> :: Destructor
    * Deletes all of the allocated data
    *********************************************************************/
   template<class T>
   BST<T> :: ~BST()
   {
      deleteBinaryTree(this->root);
   }

   /**********************************************************************
    * custom :: BST<T> :: operator =
    * Assignment Operator Overload
    *********************************************************************/
   template<class T>
   BST<T> BST<T> :: operator = (const BST & rhs)
   {
      this->numElements = rhs.numElements;
      this->root = copyBinaryTree(rhs.root);

      return *this;
   }

   /**********************************************************************
    * custom :: BST<T> :: size
    * Returns the size of the BST
    *********************************************************************/
   template<class T>
   int BST <T> :: size() const
   {
      return this->numElements;
   }

   /**********************************************************************
    * custom :: BST<T> :: empty
    * Returns whether the BST is empty or not
    *********************************************************************/
   template<class T>
   bool BST <T> :: empty() const
   {
      return (this->numElements == 0);
   }

   /**********************************************************************
    * custom :: BST<T> :: clear
    * Clears the Binary Search Tree
    *********************************************************************/
   template<class T>
   void BST <T> :: clear()
   {
      this->numElements = 0;
      deleteBinaryTree(root);
      this->root = NULL;
   }

   /**********************************************************************
    * custom :: BST<T> :: insert
    * Inserts a new element to the tree
    *********************************************************************/
   template<class T>
   typename BST<T> :: BNode* BST<T> :: getRoot() const
   {
      return root;
   }

   /**********************************************************************
    * custom :: BST<T> :: insert
    * Inserts a new element to the tree
    *********************************************************************/
   template<class T>
   void BST<T>::insert(const T& t)
   {
      try
      {
         // Empty Tree State: Initialize with a new root
         if (this->root == NULL)
         {
            this->root = new BNode(t);
            this->root->isRed = false;
            this->numElements = 1;
            return;
         }

         // Non-Empty Tree: Find the insertion point
         BNode* node = this->root;
         bool done = false;
         while (!done)
         {
            // If root node is larger, go left
            if (node->data > t)
            {
               // If there is a child to the left, go for it
               if (node->pLeft)
               {
                  node = node->pLeft;
               }
               // Otherwise, create a new node
               else
               {
                  node->addLeft(node, t);
                  done = true;
               }
            }
            // If root node is smaller, go right
            else
            {
               // If there is a child to the right, go for it
               if (node->pRight)
               {
                  node = node->pRight;
               }
               // Otherwise, create a new node
               else
               {
                  node->addRight(node, t);
                  done = true;
               }
            }
         }

         // Increase the tree size counter
         this->numElements++;

         // Make sure we know where the root node is
         while (this->root->pParent != NULL)
         {
            this->root = this->root->pParent;
         }
      }
      catch (...)
      {
         throw "ERROR: Unable to allocate a node";
      }
   }

   /**********************************************************************
    * custom :: BST<T> :: erase
    * Erases a Node from the Tree
    *********************************************************************/
   template<class T>
   void BST<T> :: erase(BST<T>::iterator &it)
   {
      // Case 1: No Children
      if (it.pNode->pRight == NULL && it.pNode->pLeft == NULL)
      {
         if (it.pNode->pParent != NULL && it.pNode->pParent->pRight == it.pNode)
         {
            it.pNode->pParent->pRight = NULL;
         }
         if (it.pNode->pParent != NULL && it.pNode->pParent->pLeft == it.pNode)
         {
            it.pNode->pParent->pLeft = NULL;
         }
         delete it.pNode;
         it.pNode = NULL;
      }
      // Case 2: One Child
      else if (it.pNode->pRight == NULL && it.pNode->pLeft != NULL)
      {
         if (it.pNode->pParent != NULL && it.pNode->pParent->pRight == it.pNode)
         {
            it.pNode->pParent->pRight = it.pNode->pLeft;
            it.pNode->pLeft->pParent = it.pNode->pParent;
         }
         if (it.pNode->pParent != NULL && it.pNode->pParent->pLeft == it.pNode)
         {
            it.pNode->pParent->pLeft = it.pNode->pLeft;
         }
         delete it.pNode;
         it.pNode = NULL;
      }
      else if (it.pNode->pRight != NULL && it.pNode->pLeft == NULL)
      {
         it.pNode->pRight->pParent = it.pNode->pParent;

         if (it.pNode->pParent != NULL && it.pNode->pParent->pLeft == it.pNode)
         {
            it.pNode->pParent->pLeft = it.pNode->pRight;
         }
         if (it.pNode->pParent != NULL && it.pNode->pParent->pRight == it.pNode)
         {
            it.pNode->pParent->pRight = it.pNode->pRight;
         }
         delete it.pNode;
         it.pNode = NULL;
      }

      // Case 3 - Two Children
      else
      {
         // Find the in-order successor
         iterator newIt = it;
         newIt++;
         
         // In case of a right child, set its parent to newIt's parent
         if (newIt.pNode->pRight != NULL)
         {
            newIt.pNode->pRight->pParent = newIt.pNode->pParent;
            newIt.pNode->pParent->pLeft = newIt.pNode->pRight;
         }
         
         // Remove dreaded references to the old parents of newIt
         if (newIt.pNode->pParent->pLeft == newIt.pNode) 
         {
            newIt.pNode->pParent->pLeft = NULL;
         }
         else if (newIt.pNode->pParent->pRight == newIt.pNode)
         {
            newIt.pNode->pParent->pRight = NULL;
         }

         // Set the parent and children of *newIt
         newIt.pNode->pParent = it.pNode->pParent;
         newIt.pNode->pLeft   = it.pNode->pLeft;
         newIt.pNode->pRight  = it.pNode->pRight;

         // Point the children to the new parent
         it.pNode->pLeft->pParent  = newIt.pNode;
         it.pNode->pRight->pParent = newIt.pNode;

         // Check if *it has a parent node
         if (it.pNode->pParent)
         {
            // Check if *it is the left or right child and set newIt accordingly
            if (it.pNode->pParent->pLeft == it.pNode)
            {
               // *it is the left child
               it.pNode->pParent->pLeft = newIt.pNode;
            }
            // *it is the right child
            else 
            {
               it.pNode->pParent->pRight = newIt.pNode;
            }
         }

         delete it.pNode;
         it.pNode = NULL;         
      }
   }

   /**********************************************************************
    * custom :: BST<T> :: find
    * Locates a BNode in the tree
    *********************************************************************/
   template<class T>
   typename BST<T> :: iterator BST<T> :: find(const T &t)
   {
      // Iterate through the tree until
      for (iterator it = begin(); it != NULL; it++)
      {
         // We find the data value
         if (*it == t)
         {
            // And return it
            return it;
         }
      }

      // Or return NULL if not found
      return NULL;
   }

   /**********************************************************************
    * custom :: BST<T> :: getFirstElement
    * Retrieves the first element from the Tree
    *********************************************************************/
   template<class T>
   typename BST <T> :: BNode* BST <T> :: getFirstElement() const
   {
      if (this->root == NULL)
      {
         return NULL;
      }
      else
      {
         BNode* _tmp = this->root;
         while (_tmp->pLeft != NULL)
         {
            _tmp = _tmp->pLeft;
         }
         return _tmp;
      }
   }

   /**********************************************************************
    * custom :: BST<T> :: getLastElement
    * Retrieves the last element from the Tree
    *********************************************************************/
   template<class T>
   typename BST <T> :: BNode* BST <T> :: getLastElement() const
   {
      if (this->root == NULL)
      {
         return NULL;
      }
      else
      {
         BNode* _tmp = this->root;
         while (_tmp->pRight != NULL)
         {
            _tmp = _tmp->pRight;
         }
         return _tmp;
      }
   }

   /**********************************************************************
    * custom :: BST<T> :: copyBinaryTree
    * This function takes a pSrc BNode and replicates the BST
    * into the current object
    *********************************************************************/
   template <class T>
   typename BST<T> :: BNode* BST<T> :: copyBinaryTree(const BST<T>::BNode * _pSrc)
   {
      if (_pSrc == NULL)
      {
         return NULL;
      }

      try
      {
         BNode* pDest = new BNode(_pSrc->data);

         pDest->pLeft = copyBinaryTree(_pSrc->pLeft);
         if (pDest->pLeft != NULL)
         {
            pDest->pLeft->pParent = pDest;
         }

         pDest->pRight = copyBinaryTree(_pSrc->pRight);
         if (pDest->pRight != NULL)
         {
            pDest->pRight->pParent = pDest;
         }

         return pDest;
      }
      catch (const std::bad_alloc)
      {
         std::cout << "Error: Unable to allocate a new BNode";
      }
   }

   /**********************************************************************
    * custom :: BST<T> :: insertRecursive
    * Inserts Nodes recursively
    *********************************************************************/
   template<class T>
   typename BST<T> :: BNode* BST<T> :: insertRecursive(BST<T> :: BNode* pRoot, BST<T> :: BNode* newNode)
   {
      if (pRoot == NULL)
      {
         return newNode;
      }

      if (newNode->data < pRoot->data)
      {
         pRoot->pLeft = insertRecursive(pRoot->pLeft, newNode);
         pRoot->pLeft->pParent = pRoot;
      }

      else if (newNode->data > pRoot->data)
      {
         pRoot->pRight = insertRecursive(pRoot->pRight, newNode);
         pRoot->pRight->pParent = pRoot;
      }

      return pRoot;
   }

   /**********************************************************************
    * custom :: BST<T> :: deleteBinaryTree
    * This function takes two BNodes as parameters. It copies the tree
    * from the first into the second.
    *********************************************************************/
   template <class T>
   void BST<T> :: deleteBinaryTree(BST<T>::BNode * &_pNode)
   {
      if (_pNode == NULL)
      {
         return;
      }
      deleteBinaryTree(_pNode->pLeft);
      deleteBinaryTree(_pNode->pRight);
      delete _pNode;
      _pNode = NULL;
   }

   /**********************************************************************
    * custom :: BST<T> :: begin
    * Returns an iterator referring to the left-most node in the tree
    *********************************************************************/
   template <class T>
   typename BST<T> :: iterator BST<T> :: begin()
   {
      return iterator(getFirstElement());
   }

   /**********************************************************************
    * custom :: BST<T> :: end
    * Returns a NULL iterator
    *********************************************************************/
   template <class T>
   typename BST<T> :: iterator BST<T> :: end()
   {
      return iterator(NULL);
   }

   /**********************************************************************
    * custom :: BST<T> :: cbegin
    * Returns an const_iterator referring to the left-most node in the tree
    *********************************************************************/
   template <class T>
   typename BST<T> :: const_iterator BST<T> :: cbegin() const
   {
      return const_iterator(getFirstElement());
   }

   /**********************************************************************
    * custom :: BST<T> :: cend
    * Returns a NULL const_iterator
    *********************************************************************/
   template <class T>
   typename BST<T> :: const_iterator BST<T> :: cend() const
   {
      return const_iterator(NULL);
   }

   /**********************************************************************
    * custom :: BST<T> :: rbegin
    * Returns an iterator referring to the right-most node in the tree
    *********************************************************************/
   template <class T>
   typename BST<T> :: iterator BST<T> :: rbegin()
   {
      return iterator(getLastElement());
   }

   /**********************************************************************
    * custom :: BST<T> :: rend()
    * Returns a NULL iterator
    *********************************************************************/
   template <class T>
   typename BST<T> :: iterator BST<T> :: rend()
   {
      return iterator(NULL);
   }

   /**********************************************************************
    * custom :: BST<T> :: ribegin
    * Returns a reverse_iterator referring to the right-most node in the tree
    *********************************************************************/
   template <class T>
   typename BST<T> :: reverse_iterator BST<T> :: ribegin()
   {
      return reverse_iterator(getLastElement());
   }

   /**********************************************************************
    * custom :: BST<T> :: riend()
    * Returns a NULL reverse_iterator
    *********************************************************************/
   template <class T>
   typename BST<T> :: reverse_iterator BST<T> :: riend()
   {
      return reverse_iterator(NULL);
   }

   /* IMPLEMENTATION - custom :: BST<T> :: BNode class */

   /*********************************************************************
    * custom :: BST<T> :: BNode :: addLeft() +1
    * Adds a node to the left of the current node
   *********************************************************************/
   template <class T>
   void BST<T> :: BNode :: addLeft(BNode * _pNode, BNode * _pAdd)
   {
      _pNode->pLeft = _pAdd;
      if (_pAdd)
      {
         _pAdd->pParent = _pNode;
      }
   }

   /*********************************************************************
    * custom :: BST<T> :: BNode :: addRight() +1
    * Adds a node to the right of the current node
   *********************************************************************/
   template <class T>
   void BST<T> :: BNode :: addRight(BNode * _pNode, BNode * _pAdd)
   {
      _pNode->pRight = _pAdd;
      if (_pAdd)
      {
         _pAdd->pParent = _pNode;
      }
   }

   /*********************************************************************
    * custom :: BST<T> :: BNode :: addLeft() +2
    * Adds a node to the left of the current node
   *********************************************************************/
   template <class T>
   void BST<T> :: BNode :: addLeft(BNode * _pNode, const T &_t)
   {
      try
      {
         addLeft(_pNode, new BNode (_t));
      }
      catch (...)
      {
         throw "ERROR: Unable to allocate a node";
      }
   }

   /*********************************************************************
    * custom :: BST<T> :: BNode :: addRight() +2
    * Adds a node to the right of the current node
   *********************************************************************/
   template <class T>
   void BST<T> :: BNode :: addRight(BNode * _pNode, const T &_t)
   {
      try
      {
         addRight(_pNode, new BNode (_t));
      }
      catch (...)
      {
         throw "ERROR: Unable to allocate a node";
      }
   }

   /* IMPLEMENTATION - custom :: BST<T> :: iterator class */

   /*********************************************************************
    * custom :: BST<T> :: iterator :: operator ++
    * Pre-Increment Operator Overload
   *********************************************************************/
   template <class T>
   typename BST <T> :: iterator& BST <T> ::iterator :: operator ++ ()
   {
      // If pNode isn't set, do nothing
      if (this->pNode == NULL)
      {
         return *this;
      }

      // Step 1: Is there a RIGHT NODE?
      if (this->pNode->pRight != NULL)
      {
         // Step right
         this->pNode = this->pNode->pRight;

         // Step 2: Check for left nodes
         while (this->pNode->pLeft)
         {
            this->pNode = this->pNode->pLeft;
         }
         return *this;
      }

      // Step 3: Right and Left Nodes are done
      BNode * pSave = this->pNode;

      // Move to the parent Node
      this->pNode = this->pNode->pParent;

      // If there are no more parent Nodes, it is done
      if (this->pNode == NULL)
      {
         return *this;
      }

      // If our saved Node is the same as the left from the sucessor's parent, we are done
      if (pSave == this->pNode->pLeft)
      {
         return *this;
      }

      // If our saved Node is the same as the right from the sucessor's parent,
      // keep moving data until we are done
      while (this->pNode != NULL && pSave == this->pNode->pRight)
      {
         pSave = this->pNode;
         this->pNode = this->pNode->pParent;
      }

      return *this;
   }

   /*********************************************************************
    * custom :: BST<T> :: iterator :: operator ++
    * Post-Increment Operator Overload
   *********************************************************************/
   template <class T>
   typename BST <T> ::iterator BST <T> :: iterator :: operator ++ (int postfix)
   {
      iterator _tmp(*this);

      // If pNode isn't set, do nothing
      if (this->pNode == NULL)
      {
         return *this;
      }

      // Step 1: Is there a RIGHT NODE?
      if (NULL != this->pNode->pRight)
      {
         // Step right
         this->pNode = this->pNode->pRight;

         // Step 2: Check for left nodes
         while (this->pNode->pLeft)
         {
            this->pNode = this->pNode->pLeft;
         }
         return _tmp;
      }

      // Step 3: Right and Left Nodes are done
      BNode * pSave = this->pNode;

      // Move to the parent Node
      this->pNode = this->pNode->pParent;

      // If there are no more parent Nodes, it is done
      if (NULL == this->pNode)
      {
         return _tmp;
      }

      // If our saved Node is the same as the left from the sucessor's parent, we are done
      if (pSave == this->pNode->pLeft)
      {
         return _tmp;
      }

      // If our saved Node is the same as the right from the sucessor's parent,
      // keep moving data until we are done
      while (NULL != this->pNode && pSave == this->pNode->pRight)
      {
         pSave = this->pNode;
         this->pNode = this->pNode->pParent;
      }

      return _tmp;
   }

   /*********************************************************************
    * custom :: BST<T> :: iterator :: operator --
    * Pre-Decrement Operator Overload
   *********************************************************************/
   template <class T>
   typename BST <T> ::iterator& BST <T> :: iterator :: operator -- ()
   {
      // If pNode isn't set, do nothing
      if (this->pNode == NULL)
      {
         return *this;
      }

      // Step 1: Is there a LEFT NODE?
      if (this->pNode->pLeft != NULL)
      {
         // Step left
         this->pNode = this->pNode->pLeft;

         // Step 2: Check for right nodes
         while (this->pNode->pRight)
         {
            this->pNode = this->pNode->pRight;
         }
         return *this;
      }

      // Step 3: Right and Left Nodes are done
      BNode * pSave = this->pNode;

      // Move to the parent Node
      this->pNode = this->pNode->pParent;

      // If there are no more parent Nodes, it is done
      if (this->pNode == NULL)
      {
         return *this;
      }

      // If our saved Node is the same as the right from the sucessor's parent, we are done
      if (pSave == this->pNode->pRight)
      {
         return *this;
      }

      // If our saved Node is the same as the left from the sucessor's parent,
      // keep moving data until we are done
      while (this->pNode != NULL && pSave == this->pNode->pLeft)
      {
         pSave = this->pNode;
         this->pNode = this->pNode->pParent;
      }

      return *this;
   }

   /*********************************************************************
    * custom :: BST<T> :: iterator :: operator --
    * Post-Decrement Operator Overload
   *********************************************************************/
   template <class T>
   typename BST <T> ::iterator BST <T> ::iterator :: operator -- (int postfix)
   {
      iterator _tmp(*this);

      // If pNode isn't set, do nothing
      if (this->pNode == NULL)
      {
         return _tmp;
      }

      // Step 1: Is there a LEFT NODE?
      if (this->pNode->pLeft != NULL)
      {
         // Step left
         this->pNode = this->pNode->pLeft;

         // Step 2: Check for right nodes
         while (this->pNode->pRight)
         {
            this->pNode = this->pNode->pRight;
         }
         return _tmp;
      }

      // Step 3: Right and Left Nodes are done
      BNode * pSave = this->pNode;

      // Move to the parent Node
      this->pNode = this->pNode->pParent;

      // If there are no more parent Nodes, it is done
      if (this->pNode == NULL)
      {
         return _tmp;
      }

      // If our saved Node is the same as the right from the sucessor's parent, we are done
      if (pSave == this->pNode->pRight)
      {
         return _tmp;
      }

      // If our saved Node is the same as the left from the sucessor's parent,
      // keep moving data until we are done
      while (this->pNode != NULL && pSave == this->pNode->pLeft)
      {
         pSave = this->pNode;
         this->pNode = this->pNode->pParent;
      }

      return _tmp;
   }

   /* IMPLEMENTATION - custom :: BST<T> :: const_iterator class */

   /*********************************************************************
    * custom :: BST<T> :: const_iterator :: operator ++
    * Pre-Increment Operator Overload
   *********************************************************************/
   template <class T>
   typename BST <T> :: const_iterator& BST <T> ::const_iterator :: operator ++ ()
   {
      iterator _it(*this);
      ++_it;

     *this = _it.getNodeRef();

      return *this;
   }

   /*********************************************************************
    * custom :: BST<T> :: const_iterator :: operator ++
    * Post-Increment Operator Overload
   *********************************************************************/
   template <class T>
   typename BST <T> ::const_iterator BST <T> :: const_iterator :: operator ++ (int postfix)
   {
      iterator _it(*this);
      // "Return" first,
      *this = _it.getNodeRef();

      // Increment later
      ++_it;

      return *this;
   }

   /*********************************************************************
    * custom :: BST<T> :: const_iterator :: operator --
    * Pre-Decrement Operator Overload
   *********************************************************************/
   template <class T>
   typename BST <T> ::const_iterator& BST <T> :: const_iterator :: operator -- ()
   {
      iterator _it(*this);
      --_it;

     *this = _it.getNodeRef();

      return *this;
   }

   /*********************************************************************
    * custom :: BST<T> :: const_iterator :: operator --
    * Post-Decrement Operator Overload
   *********************************************************************/
   template <class T>
   typename BST <T> ::const_iterator BST <T> ::const_iterator :: operator -- (int postfix)
   {
      iterator _it(*this);
      // "Return" first,
      *this = _it.getNodeRef();

      // Increment later
      --_it;

      return *this;
   }

   /* IMPLEMENTATION - custom :: BST<T> :: reverse_iterator class */

   /*********************************************************************
    * custom :: BST<T> :: reverse_iterator :: operator ++
    * Pre-Increment Operator Overload
   *********************************************************************/
   template <class T>
   typename BST <T> :: reverse_iterator& BST <T> ::reverse_iterator :: operator ++ ()
   {
      iterator _it(*this);
      --_it;

     *this = _it.getNodeRef();

      return *this;
   }

   /*********************************************************************
    * custom :: BST<T> :: reverse_iterator :: operator ++
    * Post-Increment Operator Overload
   *********************************************************************/
   template <class T>
   typename BST <T> ::reverse_iterator BST <T> :: reverse_iterator :: operator ++ (int postfix)
   {
      iterator _it(*this);
      // "Return" first,
      *this = _it.getNodeRef();

      // Decrement later
      --_it;

      return *this;
   }

   /*********************************************************************
    * custom :: BST<T> :: reverse_iterator :: operator --
    * Pre-Decrement Operator Overload
   *********************************************************************/
   template <class T>
   typename BST <T> ::reverse_iterator& BST <T> :: reverse_iterator :: operator -- ()
   {
      iterator _it(*this);
      ++_it;

     *this = _it.getNodeRef();

      return *this;
   }

   /*********************************************************************
    * custom :: BST<T> :: reverse_iterator :: operator --
    * Post-Decrement Operator Overload
   *********************************************************************/
   template <class T>
   typename BST <T> ::reverse_iterator BST <T> ::reverse_iterator :: operator -- (int postfix)
   {
      iterator _it(*this);
      // "Return" first,
      *this = _it.getNodeRef();

      // Increment later
      ++_it;

      return *this;
   }
}

#endif // BST_H
