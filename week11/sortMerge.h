/***********************************************************************
 * Module:
 *    Week 11, Sort Merge
 *    Brother Helfrich, CS 235
 * Author:
 *    Evandro Camargo, Alexandre Paixao & Caleb Georgeson
 * Summary:
 *    This program will implement the Merge Sort
 ************************************************************************/

#ifndef SORT_MERGE_H
#define SORT_MERGE_H // SORT_MERGE_H

/****************************************************
* void - merge
* Merges subarrays from helper calls
* **************************************************/
template<class T>
void merge(T arr[], int left, int mid, int right) 
{ 
   int iFirst, iSecond, iMerged; 
   int iLeft = mid - left + 1; 
   int iRight =  right - mid; 

   // The left placeholder temporary array
   T _left[iLeft];
   // The right placeholder temporary array
   T _right[iRight];

   // Fill temporary _left array
   for (iFirst = 0; iFirst < iLeft; iFirst++)
   {
      _left[iFirst] = arr[left + iFirst];
   }

   // Fill temporary _right array
   for (iSecond = 0; iSecond < iRight; iSecond++)
   {
      _right[iSecond] = arr[mid + 1 + iSecond];
   }

   // First Sub-Array Initial Index
   iFirst = 0;
   // Second Sub-Array Initial Index
   iSecond = 0;
   // Merged Sub-Array Initial Index
   iMerged = left;

   // Now we merge the temporary back into the original array
   while (iFirst < iLeft && iSecond < iRight) 
   { 
      if (_left[iFirst] <= _right[iSecond])
      {
         arr[iMerged] = _left[iFirst];
         iFirst++;
      }
      else
      { 
         arr[iMerged] = _right[iSecond]; 
         iSecond++; 
      } 
      iMerged++; 
   } 

   // If any, copy the remaining elements of _left
   while (iFirst < iLeft)
   { 
      arr[iMerged] = _left[iFirst];
      iFirst++; 
      iMerged++; 
   }

   // If any, copy the remaining elements of _right
   while (iSecond < iRight) 
   {
      arr[iMerged] = _right[iSecond]; 
      iSecond++; 
      iMerged++; 
   } 
}

/****************************************************
* void - helperMerge
* Helper method to recursively merge
* **************************************************/
template <class T>
void helperMerge(T arr[], int left, int right)
{
<<<<<<< HEAD
   int numIterations;
   int iBegin1 = 0;
   int iBegin2 = 0;
   int iEnd1 = 0;
   int iEnd2 = 0;
=======
   int _mid;
>>>>>>> 3217a78cb8779c534427d9d40c546f5698ca7936

   if (left < right)
   {
<<<<<<< HEAD
      numIterations = 0;
      iBegin1 = 0;

      
      while (iBegin1 < num)
      {
         numIterations++;
         for (iEnd1 = iBegin1 + 1; iEnd1 < num && !(source[iEnd1 - 1] > source[iEnd1]); iEnd1++){}
         
         iBegin2 = iEnd1 + 1;

         for (iEnd2 = iBegin2 + 1; iEnd2 < num && !(source[iEnd2 - 1] > source[iEnd2]); iEnd2++){}
         
         if (iBegin2 < num)
         {
            merge(destination, source + iBegin1, iEnd1 - iBegin1 + 1,source + iBegin2, iEnd2 - iBegin2 + 1);
         }
         iBegin1 = iEnd2 + 1;
      }

      std::cout << "iBegin1   : "  << iBegin1 << "\n";
      std::cout << "num       : "  << num << "\n";

      //SWAP source and destination pointer
      T *temp = array;
      *array = *destination;
      *destination = *temp;

   } while (numIterations > 1);

   if (array != source)
   {
      for(int i = 0; i <= num - 1; i++)
      {
         array[i] = source[i];
      }
=======
      // Find the middle
      _mid = (left + right) / 2;

      // Rinse and repeat
      helperMerge(arr, left, _mid);
      helperMerge(arr, _mid + 1, right);

      // Merge final array
      merge(arr, left, _mid, right);
>>>>>>> 3217a78cb8779c534427d9d40c546f5698ca7936
   }
}

/****************************************************
* void - sortMerge
* Perform the merge sort bootstrap
* **************************************************/
template <class T>
void sortMerge(T array[], int num)
{
   helperMerge(array, 0, num - 1);
}

#endif // SORT_MERGE_H