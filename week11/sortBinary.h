/***********************************************************************
 * Module:
 *    Week 11, Sort Binary
 *    Brother Kirby, CS 235
 * Author:
 *    Caleb Georgeson, Evandro Camargo, Alexandre Paixao
 * Summary:
 *    This program will implement the Binary Tree Sort
 ************************************************************************/

#ifndef SORT_BINARY_H
#define SORT_BINARY_H

#include "bst.h"
#include <cassert>

/*****************************************************
 * SORT BINARY
 * Perform the binary tree sort
 ****************************************************/
template <class T>
void sortBinary(T array[], int num)
{
   custom::BST<T> sort;
   for (int i = 0; i < num; i++)
   {
      sort.insert(array[i]);
   }
   
   typename custom::BST<T>::iterator iSort(sort.begin());
   for (int i = 0; i < num; i++)
   {
      array[i] = *iSort;
      iSort++;
   }
}


#endif // SORT_BINARY_H
