/***********************************************************************
 * Module:
 *    Week 11, Sort Heap
 *    Brother Helfrich, CS 235
 * Author:
 *    Evandro Camargo, Alexandre Paixao & Caleb Georgeson
 * Summary:
 *    This program will implement the Heap Sort
 ************************************************************************/

#ifndef SORT_HEAP_H
#define SORT_HEAP_H  // SORT_HEAP_H
#include <utility>   // std::swap

/*****************************************************
 * void - heapify
 * The core of the heap sort
 ****************************************************/
template <class T>
void heapify(T arr[], int num, int i) 
{
   // We assume the largest will be the root
   int _largest = i;
   int _left   = 2 * i + 1;
   int _right  = 2 * i + 2;

   // Left child is larger than root
   if (_left < num && arr[_left] > arr[_largest])
   {
      _largest = _left;
   }

   // Right child is larger than largest so far
   if (_right < num && arr[_right] > arr[_largest])
   {
      _largest = _right;
   } 

   // Largest isn't root 
   if (_largest != i)
   { 
      std::swap(arr[i], arr[_largest]);

      // Continue heapify'ing the rest of the tree 
      heapify(arr, num, _largest);
   } 
} 

/****************************************************
* void - sortHeap
* Perform the heap sort
* **************************************************/
template <class T>
void sortHeap(T array[], int num)
{
   // Begin re-arranging the array by builind the heap
   for (int i = num / 2 - 1; i >= 0; i--)
   {
      heapify(array, num, i);
   }

   // Extract elements from the heap, one at a time
   for (int i = num - 1; i >= 0; i--) 
   { 
      // Current root goes to the end of the tree
      std::swap(array[0], array[i]); 

      // Heapify the final tree 
      heapify(array, i, 0);
   }
}

#endif // SORT_HEAP_H
