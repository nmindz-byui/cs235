/***********************************************************************
 * Module:
 *    Week 11, Sort Quick
 *    Brother Helfrich, CS 235
 * Author:
 *    Evandro Camargo, Alexandre Paixao & Caleb Georgeson
 * Summary:
 *    This program will implement the Quick Sort
 ************************************************************************/

#ifndef SORT_QUICK_H
#define SORT_QUICK_H // SORT_QUICK_H
#include <utility>   // std::swap

/****************************************************
* int - partition
* Partitions the array data and handles ordering
* **************************************************/
template <class T>
int partition(T *arr, int left, int right)
{
   // Find the mid value
   int _mid = left + (right - left) / 2;
   
   // Gets the pivot (mid value)
   T pivot = arr[_mid];

   // Swap the mid and front values
   std::swap(arr[_mid], arr[left]);

   int i = left + 1;
   int j = right;

   // Could have used a for loop, but we find
   // this way easier to read and understand
   while (i <= j)
   {
      while(i <= j && arr[i] <= pivot)
      {
         i++;
      }

      while(i <= j && arr[j] > pivot)
      {
         j--;
      }

      // If we find a lower value to the left,
      // swap it (order) and move on.
      if (i < j)
      {
         std::swap(arr[i], arr[j]);
      }
   }

   std::swap(arr[i - 1], arr[left]);
   return i - 1;
}

/****************************************************
* void - helperQuick
* Helper method to recursively sort
* **************************************************/
template <class T>
void helperQuick(T arr[], int left, int right)
{
   // If left is greater than right, do nothing
   if (left >= right)
   {
      return;
   }

   // Partitions and gets index
   int _part = partition(arr, left, right);

   // Rinse and repeat
   helperQuick(arr, left, _part - 1);
   helperQuick(arr, _part + 1, right);
}

/****************************************************
* void - sortQuick
* Perform the quick sort bootstrap
* **************************************************/
template <class T>
void sortQuick(T array[], int num)
{
   helperQuick(array, 0, num - 1);
}

#endif // SORT_QUICK_H
