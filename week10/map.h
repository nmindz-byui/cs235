/***********************************************************************
 * Component:
 *    Assignment 10, Map
 *    Brother Kirby, CS 235
 * Author:
 *    Evandro Camargo, Alexandre Paixao & Caleb Georgeson
 * Summary:
 *    Create and handle a Map data strcuture with
 *    its iterator and nested classes's dependencies
 ************************************************************************/

#ifndef MAP_H
#define MAP_H // MAP_H

#include "pair.h"
#include "bst.h"

namespace custom
{
   /********************************************************************
   * custom :: map<K,V>
   * Map Class Definition
   * ******************************************************************/
   template<class K, class V>
   class map
   {
      public:
         // Nested Class Definitions
         class iterator;
         // Constructors
         map();
         map(const map<K,V>& rhs);
         // Destructor
         ~map() {};
         // Get Methods
         int size();
         bool empty();
         // Set/Insert Methods
         void clear();
         void insert(K _k, V _v);
         void insert(pair<K, V> _pair);
         // Iterator-related Methods
         iterator begin();
         iterator end();
         iterator find(K _k);
         // Operator Overloads
         V& operator [] (K _k);
         const V& operator [] (K _k) const;
         map<K,V>& operator = (const map<K,V>& rhs);
      private:
         typename custom :: BST <pair<K,V>> bst;
   };

   /********************************************************************
   * custom :: map<K,V> :: iterator
   * This is the nested class definition for the iterator class
   * ******************************************************************/
   template<class K, class V>
   class map <K, V> :: iterator
   {
      private:
         typename BST <pair<K,V>> :: iterator it;
      public:
         // Constructors & Destructor
         iterator() : it() {}
         iterator(const typename BST <pair<K,V>> :: iterator & rhs)  { it = rhs; }
         iterator(const iterator &rhs)                               { it = rhs.it; }
         // Operator Overloads
         bool operator == (const iterator & rhs)   { return (it == rhs.it); }
         bool operator != (const iterator & rhs)   { return (it != rhs.it); }
         pair<K,V>& operator * ()                  { return (*it); }
         iterator&  operator = (const iterator & rhs);
         iterator&  operator ++ ();
         iterator&  operator -- ();
         iterator   operator ++ (int postfix);
         iterator   operator -- (int postfix);
   };

   /* MAP ITERATOR NESTED CLASS IMPLEMENTATION */

   /*********************************************************************
    * custom :: map<K,V> :: iterator :: operator =
    * Equals Operator Overload
   *********************************************************************/
   template<class K, class V>
   typename map<K,V> :: iterator& map<K,V> :: iterator :: operator = (const iterator & rhs)
   {
      it = rhs.it;
      return *this;
   }

   /*********************************************************************
    * custom :: map<K,V> :: iterator :: operator ++
    * Pre-Increment Operator Overload
   *********************************************************************/
   template<class K, class V>
   typename map<K,V> :: iterator& map<K,V> :: iterator :: operator ++ ()
   {
      ++it;
      return *this;
   }

   /*********************************************************************
    * custom :: map<K,V> :: iterator :: operator --
    * Pre-Decrement Operator Overload
   *********************************************************************/
   template<class K, class V>
   typename map<K,V> :: iterator& map<K,V> :: iterator :: operator -- ()
   {
      --it;
      return *this;
   }

   /*********************************************************************
    * custom :: map<K,V> :: iterator :: operator ++
    * Post-Increment Operator Overload
   *********************************************************************/
   template<class K, class V>
   typename map<K,V> :: iterator map<K,V> :: iterator :: operator ++ (int postfix)
   {
      iterator tmp(this);
      it++;
      return tmp;
   }

   /*********************************************************************
    * custom :: map<K,V> :: iterator :: operator --
    * Post-Decrement Operator Overload
   *********************************************************************/
   template<class K, class V>
   typename map<K,V> :: iterator map<K,V> :: iterator :: operator -- (int postfix)
   {
      iterator tmp(this);
      it--;
      return tmp;
   }

   /* MAP CLASS IMPLEMENTATION */

   /******************************************************************
    * custom :: map<K,V> :: map
    * Default Constructor
    * ****************************************************************/
   template<class K, class V>
   map <K, V> :: map()
   {
      // this->bst = bst();
   }

   /******************************************************************
    * custom :: map<K,V> :: map
    * Copy Constructor
    * ****************************************************************/
   template<class K, class V>
   map <K, V> :: map(const map<K,V> & rhs)
   {
      this->bst = rhs.bst;
   }

   /*******************************************************************
    * custom :: map<K,V> :: size
    * Returns the map size
    * ****************************************************************/
   template<class K, class V>
   int map <K, V> :: size()
   {
      return this->bst.size();
   }

   /*******************************************************************
    * custom :: map<K,V> :: empty
    * Returns whether the map is empty
    * ****************************************************************/
   template<class K, class V>
   bool map <K, V> :: empty()
   {
      return this->bst.empty();
   }

   /*******************************************************************
    * custom :: map<K,V> :: clear
    * Clears the map tree
    * ****************************************************************/
   template<class K, class V>
   void map <K, V> :: clear()
   {
      this->bst.clear();
   }

   /*******************************************************************
    * custom :: map<K,V> :: insert
    * Creates a pair out of key/value and inserts into the map tree
    * ****************************************************************/
   template<class K, class V>
   void map <K, V> :: insert(K _k, V _v)
   {
      pair<K, V> pair(_k, _v);
      this->bst.insert(pair);
   }

   /*******************************************************************
    * custom :: map<K,V> :: insert
    * Inserts a pair object directly into the map tree
    * ****************************************************************/
   template<class K, class V>
   void map <K, V> :: insert(pair<K, V> _pair)
   {
      this->bst.insert(_pair);
   }

   /******************************************************************
    * custom :: map<K,V> :: begin
    * Returns an iterator pointing to the first node of the BST
    * ****************************************************************/
   template<class K, class V>
   typename map<K, V> :: iterator map <K, V> :: begin()
   {
      return this->bst.begin();
   }

   /******************************************************************
    * custom :: map<K,V> :: end
    * Returns a null iterator
    * ****************************************************************/
   template<class K, class V>
   typename map<K, V> :: iterator map <K, V> :: end()
   {
      return iterator(NULL);
   }

   /******************************************************************
    * custom :: map<K,V> :: find
    * Returns an iterator pointing to the key
    * ****************************************************************/
   template<class K, class V>
   typename map<K, V> :: iterator map <K, V> :: find(K _k)
   {
      pair<K, V> _tmp(_k, V());
      iterator it(this->bst.find(_tmp));
      iterator _it(it);
      return _it;
   }

   /**********************************************************************
    * custom :: map<K,V> :: operator =
    * Assignment Operator Overload
    *********************************************************************/
   template<class K, class V>
   map <K,V>& map <K,V> :: operator = (const map<K,V>& rhs)
   {
      this->bst = rhs.bst;
      return *this;
   }

   /**********************************************************************
    * custom :: map<K,V> :: operator []
    * Key Access Operator Overload
    *********************************************************************/
   template<class K, class V>
   V& map <K,V> :: operator [] (K _k)
   {
      iterator _tmp(this->find(_k));

      // Not found
      if (_tmp == this->end())
      {
         // Add it
         pair<K, V> _tPair(_k, V());
         this->insert(_tPair);
         _tmp = this->find(_k);
      }
      return (*_tmp).second;
   } 

   /**********************************************************************
    * custom :: map<K,V> :: operator []
    * [CONST] Key Access Operator Overload
    *********************************************************************/
   template<class K, class V>
   const V& map <K,V> :: operator [] (K _k) const
   {
      iterator _tmp(this->find(_k));

      // Not found
      if (_tmp == this->end())
      {
         // Return default V
         return V();
      }

      return (*_tmp).second;
   }   
}

#endif // MAP_H