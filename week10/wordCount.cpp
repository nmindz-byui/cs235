/***********************************************************************
 * Module:
 *    Week 10, WORD COUNT
 *    Brother Kirby, CS 235
 * Author:
 *    Evandro Camargo, Alexandre Paixao & Caleb Georgeson
 * Summary:
 *    This program will implement the wordCount() function
 ************************************************************************/

#include "map.h"        // for MAP
#include "wordCount.h"  // for wordCount() prototype
#include <string>       // for std::string
#include <fstream>      // for std::ifstream
#include <iostream>

/*****************************************************
 * WORD COUNT
 * Prompt the user for a file to read, then prompt the
 * user for words to get the count from
 *****************************************************/
void wordCount()
{
    // Holds the word frequency, or the Word Frequency Map
    custom::map <std::string, int> _wfq;
    std::string _fname, _word;

    // Gets the filename to read from
    std::cout << "What is the filename to be counted? ";
    std::cin >> _fname;

    // Opens the file
    std::ifstream _file(_fname);

    // While the file is open,
    if (_file.is_open())
    {
        // Add each word occurrence to its counter
        while (_file >> _word)
        {
            _wfq[_word] += 1;
        }

        // Close the file after all words have been processed
        _file.close();
    }

    // Asks for the word which we should retrieve frequency for
    std::cout
        << "What word whose frequency is to be found. Type ! when done\n"
        << "> ";
    std::cin >> _word;

    // While the user won't exit,
    while (_word != "!")
    {
        // Outputs the first word frequency
        std::cout
            << "\t"
            << _word
            << " : "
            << _wfq[_word]
            << "\n"
            << "> ";
        
        // And ask for input again
        std::cin >> _word;
    }
}
