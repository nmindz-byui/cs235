/***********************************************************************
* Module:
*    Week 10, WORD COUNT
*    Brother Kirby, CS 235
* Header:
*    WORD COUNT
* Summary:
*    This will contain just the prototype for the wordCount()
*    function
* Author
*    Evandro Camargo, Alexandre Paixao & Caleb Georgeson
************************************************************************/

#ifndef WORD_COUNT_H
#define WORD_COUNT_H // WORD_COUNT_H

/*****************************************************
 * WORD COUNT
 * Prompt the user for a file to read, then prompt the
 * user for words to get the count from
 *****************************************************/
void wordCount();

#endif // WORD_COUNT_H

