/***********************************************************************
 * Header:
 *    Vector
 * Summary:
 *    This class contains the notion of an vector: a bucket to hold
 *    data for the user. This is just a starting-point for more advanced
 *    constainers such as the vector, set, stack, queue, deque, and map
 *    which we will build later this semester.
 *
 *    This will contain the class definition of:
 *       vector             : similar to std::vector
 *       vector :: iterator : an iterator through the vector
 * Author
 *    Evandro Camargo (and Br. Helfrich)
 ************************************************************************/
#include <stdexcept>

#ifndef VECTOR_H
#define VECTOR_H
#define ALLOC_SIZE_STD 4

// a little helper macro to write debug code
#ifdef NDEBUG
#define Debug(statement)
#else
#define Debug(statement) statement
#endif // !NDEBUG

namespace custom
{

/************************************************
 * VECTOR
 * A class that holds stuff
 ***********************************************/
template <class T>
class vector
{
   private:
      T* buffer;        // dynamically allocated vector of T
      int numElements;  // number of elements
      int numCapacity;  // maximum capacity of elements
      void resize(const unsigned int _capacity);
      int scale(const unsigned int _size);

   public:
      // constructors and destructors
      vector();
      vector(const unsigned int _size);
      vector(const unsigned int _size, T &t);
      
      // Rule of Three
      ~vector();                                         // Destructor
      vector(const vector<T> &rhs);                      // Copy constructor
      vector(vector&&) = default;                        // Move constructor

      // TODO: Implement the Rule of Five
      // vector<T>& operator=(const vector&) = default;  // Copy assignment operator
      // vector<T>& operator=(vector&&) = default;       // Move assignment operator

      // Operator overloading
      vector<T>& operator=(const vector &rhs);
      T& operator[](int index);
      const T& operator[](int index) const;
      
      // standard container interfaces
      int size() const;
      int capacity() const;
      bool empty() const;
      void clear();
      void push_back(const T &t);
      T access(int _index) const;
      
      // the various iterator interfaces
      class iterator;
      iterator begin();
      iterator end();
      class const_iterator;
      const_iterator cbegin() const;
      const_iterator cend() const;

      // a debug utility to display the vector
      // this gets compiled to nothing if NDEBUG is defined
      void display() const; 
};

/**************************************************
 * VECTOR CLASS IMPLEMENTATION
 * The Custom Vector Container
 *************************************************/

/******************************************
 * vector :: vector
 * Default Constructor
 ******************************************/
template<class T>
vector<T> :: vector()
{
   this->numCapacity = 0;
   this->numElements = 0;
   this->buffer = NULL;
}

/******************************************
 * vector :: vector
 * Constructor, +1 Overload
 ******************************************/
template<class T>
vector<T> :: vector(const unsigned int _size)
{
   try
   {
      this->numCapacity = _size;
      this->numElements = _size;
      this->buffer = new T[_size];
   }
   catch (std::bad_alloc)
   {
      throw "ERROR: Unable to allocate a new buffer for vector";
   }
}

/******************************************
 * vector :: vector
 * Constructor, +2 Overloads
 ******************************************/
template<class T>
vector<T> :: vector(const unsigned int _size, T &t)
{
   try
   {
      this->numCapacity = _size;
      this->numElements = _size;
      this->buffer = new T[_size];
      for (unsigned int i = 0; i < _size; i++)
         this->buffer[i] = t.buffer[i];
   }
   catch (std::bad_alloc)
   {
      throw "ERROR: Unable to allocate a new buffer for vector";
   }
}

/******************************************
 * vector :: ~vector
 * Destructor
 ******************************************/
template<class T>
vector<T> :: ~vector()
{
   delete[] buffer;
}

/******************************************
 * vector :: vector
 * Copy Constructor
 ******************************************/
template<class T>
vector<T> :: vector(const vector<T> &rhs)
{
   this->buffer = new T[rhs.capacity()]; 

   this->numCapacity = rhs.capacity();
   this->numElements = rhs.size();
   this->buffer = new T[rhs.size()];
   for (unsigned int i = 0; i < rhs.size(); i++)
   {
      this->buffer[i] = rhs.access(i);
   }
}

/******************************************
 * vector :: operator=
 * Assignment Operator
 ******************************************/
template<class T>
vector<T>& vector<T> :: operator=(const vector &rhs)
{
   try
   {
      if (this->numCapacity < rhs.capacity())
      {
         this->resize(this->scale(rhs.capacity()));
      }

      this->numElements = rhs.size();

      for(int i = 0; i < rhs.size(); i++)
      {
         this->buffer[i] = rhs[i];
      }

      return *this;
   }
   catch (std::bad_alloc)
   {
      throw "ERROR: Unable to allocate a new buffer for vector";
   }
}

/******************************************
 * vector :: operator[]
 * Collection/Element Operator
 ******************************************/
template<class T>
T& vector<T> :: operator[](int _index)
{
   if (_index > (this->size() - 1))
   {
      throw std::out_of_range("ERROR: Invalid index");
   }
   
   return buffer[_index];
}

/******************************************
 * vector :: operator[] const
 * Collection/Element Operator
 ******************************************/
template<class T>
const T& vector<T> :: operator[](int _index) const
{
   if (_index > (this->size() - 1))
   {
      throw std::out_of_range("ERROR: Invalid index");
   }

   return buffer[_index];
}

/******************************************
 * vector :: resize
 * Resizes the vector to the new capacity
 ******************************************/
template<class T>
void vector<T> :: resize(const unsigned int _capacity)
{
   try
   {
      T* _buffer = new T[_capacity]; // allocate new temporary buffer

      // move data from old buffer to new tmp buffer
      for(int i = 0; i < this->size(); i++)
      {
         _buffer[i] = this->buffer[i];
      }
      delete[] this->buffer;

      // initialize new buffer with more capacity
      this->buffer = new T[_capacity];

      // move data back from tmp buffer to class buffer
      for(int i = 0; i < this->size(); i++)
      {
         this->buffer[i] = _buffer[i];
      }
      delete[] _buffer;
      
      this->numCapacity = _capacity;
   }
   catch (std::bad_alloc)
   {
      throw "ERROR: Unable to allocate a new buffer for vector";
   }
}

/******************************************
 * vector :: scale
 * Increases vector capacity to at least (or more) the value of _capacity
 ******************************************/
template<class T>
int vector<T> :: scale(const unsigned int _size)
{
   int remainder = _size % ALLOC_SIZE_STD;
   int _capacity = 0;

   if (this->numCapacity >= ALLOC_SIZE_STD)
   {
      _capacity = this->numCapacity * 2;
   }
   else if (remainder == 0)
   {
      // keep _capacity the same
   }
   else
   {
      _capacity = _size + ALLOC_SIZE_STD - remainder;
   }

   return _capacity;
}

/******************************************
 * vector :: size
 * Returns vector size
 ******************************************/
template<class T>
int vector<T> :: size() const
{
   return this->numElements;
}

/******************************************
 * vector :: capacity
 * Returns vector capacity
 ******************************************/
template<class T>
int vector<T> :: capacity() const
{
   return this->numCapacity;
}

/******************************************
 * vector :: empty
 * Returns whether the vector is empty
 ******************************************/
template<class T>
bool vector<T> :: empty() const
{
   return (this->numElements == 0 ? true : false);
}

/******************************************
 * vector :: clear
 * Empties the vector (and clears previous element data)
 ******************************************/
template<class T>
void vector<T> :: clear()
{
   this->numElements = 0;
}

/******************************************
 * vector :: push_back
 * Adds a new element to the bottom of the vector
 ******************************************/
template<class T>
void vector<T> :: push_back(const T &t)
{
   if (this->numElements + 1 > this->numCapacity)
   {
      this->resize(this->scale(this->numElements+1));
   }

   this->buffer[this->size()] = t;
   this->numElements++;
}

/******************************************
 * vector :: access
 * Retrieves a vector element
 ******************************************/
template<class T>
T vector<T> :: access(int _index) const
{
   return this->buffer[_index];
}

/********************************************
 * vector :: begin
 * Returns the first element address
 ********************************************/
template <class T>
typename vector <T> :: iterator vector <T> :: begin ()
{
   return iterator(buffer);
}

/********************************************
 * vector :: end
 * Returns the last element address
 ********************************************/
template <class T>
typename vector <T> :: iterator vector <T> :: end ()
{
   return iterator(buffer + this->size());
}

/********************************************
 * vector :: cbegin
 * Returns the first element address
 ********************************************/
template <class T>
typename vector <T> :: const_iterator vector <T> :: cbegin () const
{
   return const_iterator(buffer);
}

/********************************************
 * vector :: cend
 * Returns the last element address
 ********************************************/
template <class T>
typename vector <T> :: const_iterator vector <T> :: cend () const
{
   return const_iterator(buffer + this->size());
}

/**************************************************
 * VECTOR ITERATOR
 * An iterator through vector
 *************************************************/
template <class T>
class vector <T> :: iterator
{
   private:
      T * p;

   public:
      // constructors, destructors, and assignment operator
      iterator()      : p(NULL)      {              }
      iterator(T * p) : p(p)         {              }
      iterator(const iterator & rhs) { *this = rhs; }
      iterator & operator = (const iterator & rhs)
      {
         this->p = rhs.p;
         return *this;
      }

      // equals, not equals operator
      bool operator != (const iterator & rhs) const { return rhs.p != this->p; }
      bool operator == (const iterator & rhs) const { return rhs.p == this->p; }

      // dereference operator
            T & operator * ()       { return *p; }
      const T & operator * () const { return *p; }

      // prefix increment
      iterator & operator ++ ()
      {
         p++;
         return *this;
      }

      // postfix increment
      iterator operator ++ (int postfix)
      {
         iterator tmp(*this);
         p++;
         return tmp;
      }

      // prefix decrement
      iterator & operator -- ()
      {
         p--;
         return *this;
      }

      // postfix decrement
      iterator operator -- (int postfix)
      {
         iterator tmp(*this);
         p--;
         return tmp;
      }
};

/**************************************************
 * VECTOR CONSTANT ITERATOR
 * A constant iterator through vector
 *************************************************/
template <class T>
class vector <T> :: const_iterator
{
   private:
      T * p;

   public:
      // constructors, destructors, and assignment operator
      const_iterator()      : p(NULL)      {              }
      const_iterator(T * p) : p(p)         {              }
      const_iterator(const const_iterator & rhs) { *this = rhs; }
      const_iterator & operator = (const const_iterator & rhs)
      {
         this->p = rhs.p;
         return *this;
      }

      // equals, not equals operator
      bool operator != (const const_iterator & rhs) const { return rhs.p != this->p; }
      bool operator == (const const_iterator & rhs) const { return rhs.p == this->p; }

      // dereference operator
            T & operator * ()       { return *p; }
      const T & operator * () const { return *p; }

      // prefix increment
      const_iterator & operator ++ ()
      {
         p++;
         return *this;
      }

      // postfix increment
      const_iterator operator ++ (int postfix)
      {
         const_iterator tmp(*this);
         p++;
         return tmp;
      }

      // prefix decrement
      const_iterator & operator -- ()
      {
         p--;
         return *this;
      }

      // postfix decrement
      const_iterator operator -- (int postfix)
      {
         const_iterator tmp(*this);
         p--;
         return tmp;
      }
};

/********************************************
 * VECTOR : DISPLAY
 * A debug utility to display the contents of the vector
 *******************************************/
template <class T>
void vector <T> :: display() const
{
#ifndef NDEBUG
   std::cerr << "vector<T>::display()\n";
   std::cerr << "\tnum = " << this->numElements << "\n";
   for (int i = 0; i < this->numElements; i++)
      std::cerr << "\tbuffer[" << i << "] = " << buffer[i] << "\n";
#endif // NDEBUG
}

}; // namespace custom

#endif // VECTOR_H