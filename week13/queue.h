/***********************************************************************
 * Header:
 *    QUEUE
 * Summary:
 *    This will contain the implementation of the templated
 *    abstract data structure for a "queue".
 * Author
 *    Evandro Camargo, Alexandre Paixao & Caleb Georgeson
 ************************************************************************/

#ifndef QUEUE_H
#define QUEUE_H // QUEUE_H

#define ALLOC_SIZE_STD 1

#include <iostream>

namespace custom
{
   template<class T>
   class queue
   {
      private:
         // Variables
         T* array;
         int numPush;
         int numPop;
         int numCapacity;
         // Methods
         void resize(int _numCapacity);
         int iHead() const;
         int iTail() const;

      public:
         // Constructors
         queue();
         queue(int _numCapacity);
         queue(const queue &rhs);
         // Destructor
         ~queue();
         // Overloaded operators
         queue<T>& operator=(const queue &rhs);
         // Methods
         int size() const;
         void clear();
         bool empty() const;
         void push(T t);
         void pop();
         T front() const;
         T* pFront() const;
         T back() const;
         T* pBack() const;
         void display() const;
   };

   // Constructors
   
   /******************************************************************
    * queue :: queue
    * Default Constructor
    *******************************************************************/
   template<class T>
   queue <T> :: queue()
   {
      this->numPush = 0;
      this->numPop = 0;
      this->numCapacity = ALLOC_SIZE_STD;
      this->array = new T[this->numCapacity];
   }

   /*******************************************************************
    * queue :: queue
    * Non-default Constructor
    ********************************************************************/
   template<class T>
   queue <T> :: queue(int _numCapacity)
   {
      try
      {
         this->numPush = 0;
         this->numPop = 0;
         this->numCapacity = _numCapacity;
         this->array = new T[this->numCapacity];
      }
      catch (std::bad_alloc)
      {
          throw "ERROR: unable to allocate a new buffer for Queue.";
      }
   }

   /*******************************************************************
    * queue :: queue
    * Copy Constructor
    ********************************************************************/
   template<class T>
   queue <T> :: queue(const queue &rhs)
   {
      try
      {
         this->numPop = rhs.numPop;
         this->numPush = rhs.numPush;
         this->numCapacity = rhs.numCapacity;
         this->array = new T[rhs.numCapacity];

         for (int i = 0; i < rhs.numCapacity; i++)
         {
            this->array[i] = rhs.array[i];
         }
      }
      catch (std::bad_alloc)
      {
          throw "ERROR: unable to allocate a new buffer for Queue.";
      }
   }
   
   /*******************************************************************
    * queue :: ~queue
    * Destructor
    ********************************************************************/
   template<class T>
   queue <T> :: ~queue()
   {
      delete[] this->array;

      this->array = NULL;
   }

   // Overloaded operators
   /*******************************************************************
    * queue :: operator =
    * Assignment Operator Overload
    ********************************************************************/
   template<class T>
   queue<T>& queue<T> :: operator=(const queue &rhs)
   {
      try
      {
         if (this->numCapacity < rhs.size())
         {
            this->numCapacity = rhs.size();
            this->resize(rhs.size());
         }
        
        this->numPop = 0;
        this->numPush = 0;

         for (int i = rhs.numPop; i < rhs.numPush; i++)
         {
            this->push(rhs.array[i % rhs.numCapacity]);
         }
      }
      catch (std::bad_alloc)
      {
         throw "ERROR: unable to allocate a new buffer for Queue.";
      }

      return *this;
   }

   // Public member functions
   /*******************************************************************
    * queue :: size
    * Returns the size of the queue
    ********************************************************************/
   template<class T>
   int queue <T> :: size() const
   {
      return this->numPush - this->numPop;
   }

   /*******************************************************************
    * queue :: clear
    * Empties the queue of all elements
    ********************************************************************/
   template<class T>
   void queue <T> :: clear()
   {
      delete [] this->array;
      this->array = new T[this->numCapacity];
      this->numPush = 0;
      this->numPop = 0;
   }

   /*******************************************************************
    * queue :: empty
    * Test whether the queue is empty
    ********************************************************************/
   template<class T>
   bool queue <T> :: empty() const
   {
      if (this->size() == 0)
      {
         return true;
      }
      else
      {
         return false;
      }
   }

   /*******************************************************************
    * queue :: push
    * Adds an element to the queue
    ********************************************************************/
   template<class T>
   void queue <T> :: push(T t)
   {
      if (this->size() == this->numCapacity)
      {     
         resize(this->numCapacity);
      }

      this->numPush++;
      this->array[this->iTail()] = t;
   }

   /*******************************************************************
    * queue :: pop
    * Removes an element from the head of the queue
    ********************************************************************/
   template<class T>
   void queue <T> :: pop()
   {
      if (!this->empty())
      {
         this->numPop++;
      }
   }

   /*******************************************************************
    * queue :: front
    * Returns the element that is currently at the front of the queue
    ********************************************************************/
   template<class T>
   T queue <T> :: front() const
   {
      if (this->empty())
      {
         throw "ERROR: attempting to access an element in an empty queue";
      }
      else
      {
         return this->array[this->iHead()];
      }
   }

   /*******************************************************************
    * queue :: front
    * Returns the reference to the element that is currently at the front of the queue
    ********************************************************************/
   template<class T>
   T* queue <T> :: pFront() const
   {
      if (this->empty())
      {
         throw "ERROR: attempting to access an element in an empty queue";
      }
      else
      {
         return this->array + this->iHead();
      }
   }

   /*******************************************************************
    * queue :: back
    * Returns the element that is currently at the back of the queue
    ********************************************************************/
   template<class T>
   T queue <T> :: back() const
   {
      if (this->empty())
      {
         throw "ERROR: attempting to access an element in an empty queue";
      }
      else
      {
         return this->array[this->iTail()];
      }
   }

   /*******************************************************************
    * queue :: back
    * Returns the reference to the element that is currently at the back of the queue
    ********************************************************************/
   template<class T>
   T* queue <T> :: pBack() const
   {
      if (this->empty())
      {
         throw "ERROR: attempting to access an element in an empty queue";
      }
      else
      {
         return this->array + this->iTail();
      }
   }

   // Private member functions
   /*******************************************************************
    * queue :: resize
    * Allocates more memory as needed
    ********************************************************************/
   template<class T>
   void queue <T> :: resize(int _numCapacity)
   {
      // calc capacity size
      int remainder = _numCapacity % ALLOC_SIZE_STD;
      int _capacity = 0;

      if (this->numCapacity >= ALLOC_SIZE_STD)
      {
         _capacity = this->numCapacity * 2;
      }
      else if (remainder == 0)
      {
         // keep _capacity the same
      }
      else
      {
         _capacity = _numCapacity + ALLOC_SIZE_STD - remainder;
      }

      try
      {
         T* _array = new T[_capacity]; // allocate new temporary array

         // move data from old array to new tmp array
         for (int i = this->numPop, j = 0; i < this->numPush; i++, j++)
         {
            _array[j] = this->array[i % this->numCapacity];
         }
         delete[] this->array;

         // initialize new array with more capacity
         this->array = _array;
         this->numPop = 0;
         this->numPush = this->numCapacity;
         this->numCapacity = _capacity;
      }
      catch (std::bad_alloc)
      {
         throw "ERROR: unable to allocate a new buffer for Queue.";
      }
   }

   /*******************************************************************
    * queue :: iHead
    * Returns the index of the first item of the queue
    ********************************************************************/
   template<class T>
   int queue <T> :: iHead() const
   {
      if (this->numCapacity == 0)
      {
         return 0;
      }

      return this->numPop % this->numCapacity;
   }

   /*******************************************************************
    * queue :: iTail
    * Returns the index of the last item of the queue
    ********************************************************************/
   template<class T>
   int queue <T> :: iTail() const
   {
      if (this->numCapacity == 0)
      {
         return 0;
      }
      return (this->numPush - 1) % this->numCapacity;
   }

   /*******************************************************
    * debug display
    *******************************************************/
   template <class T>
   void queue <T> :: display() const
   {
      // display the contents of the array
      std::cerr << "data = ";
      for (int i = 0; i < numCapacity; i++)
      {
         if (iHead() > iTail() && (i <= iTail() || i >= iHead()))
            std::cerr << array[i] <<  ' ';
         else if (iHead() <= iTail() && i >= iHead() && i <= iTail())
            std::cerr << array[i] << ' ';
         else
            std::cerr << "? ";
      }
      std::cerr << "\n";

      // display the contents of the member variables
      std::cerr << "numPush     " << numPush << "\n";
      std::cerr << "numPop      " << numPop  << "\n";
      std::cerr << "numCapacity " << numCapacity << "\n";

      // display the contents of the header and tail
      if (numCapacity)
      {
         std::cerr << "iHead()     " << iHead() << "\n";
         std::cerr << "iTail()     " << iTail()  << "\n";
      }
   }
}

#endif // QUEUE_H