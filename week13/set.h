/***********************************************************************
 * Header:
 *    SET
 * Summary:
 *    This will contain the implementation of the templated
 *    abstract data structure for a "SET".
 * Author
 *    Evandro Camargo, Alexandre Paixao & Caleb Georgeson
 ************************************************************************/

#ifndef SET_H
#define SET_H // SET_H

#include <iostream>

#define ALLOC_SIZE_STD 4

namespace custom
{
   // Set Class
   template<class T>
   class set
   {
      private:
         // Private data
         T* data;
         int numElements;
         int numCapacity;
         // Private methods
         int findIndex(T t);
         void resize(int _numCapacity);

      public:
         // Iterator class
         class iterator;
         class const_iterator;
         // Constructors
         set();                  // Default
         set(int _numCapacity);  // Non-Default
         set(const set & rhs);   // Copy
         //Destructor
         ~set();
         //Overloaded operators
         set<T> operator =    (const set<T> & rhs);   // Assignment
         set<T> operator ||   (const set<T> & rhs);   // Union
         set<T> operator &&   (const set<T> & rhs);   // Intersection
         set<T> operator -    (const set<T> & rhs);   // Difference
         // Public methods
         int size() const;
         bool empty() const;
         void clear();
         iterator find(T t);
         void insert(T t);
         iterator erase(iterator it);
         iterator begin() const;
         iterator end() const;
         const_iterator cbegin() const;
         const_iterator cend() const;
   };

   // Set Iterator Class
   template<class T>
   class set<T> :: iterator
   {
      private:
         T * p;
      public:
         // Constructors
         iterator(                    ) : p(NULL)  {};
         iterator(T * _p              ) : p(_p)    {};
         iterator(T & data            ) : p(&data) {};
         iterator(const iterator & rhs) : p(rhs.p) {};
         // Overloaded operators
         iterator & operator = (T *p)        { this->p = p;                                }
         bool operator == (const iterator &it) const { return this->p == it.p;             }
         bool operator != (const iterator &it) const { return this->p != it.p;             }
         iterator & operator ++ ()           { p++; return *this;                          }
         iterator & operator -- ()           { p--; return *this;                          }
         iterator operator ++(int postfix)   { iterator it(*this); p++; return it;         }
         iterator operator +(int rhs)        { iterator it(*this); it.p += rhs; return it; }
         T & operator * ()                   { return *p;                                  }
   };

   // Set Const Iterator Class
   template<class T>
   class set<T> :: const_iterator
   {
      private:
         T * p;
      public:
         // Constructors
         const_iterator(                    ) : p(NULL)  {};
         const_iterator(T * _p              ) : p(_p)    {};
         const_iterator(T & data            ) : p(&data) {};
         const_iterator(const iterator & rhs) : p(rhs.p) {};
         // Overloaded operators
         iterator & operator = (T *p)                       { this->p = p;                                      }
         bool operator == (const const_iterator &it) const  { return this->p == it.p;                           }
         bool operator != (const const_iterator &it) const  { return this->p != it.p;                           }
         const_iterator & operator ++ ()                    { p++; return *this;                                }
         const_iterator & operator -- ()                    { p--; return *this;                                }
         const_iterator operator ++(int postfix)            { const_iterator it(*this); p++; return it;         }
         const_iterator operator +(int rhs)                 { const_iterator it(*this); it.p += rhs; return it; }
         T & operator * ()                                  { return *p;                                        }
   };

   /******************************************************************
    * set :: set
    * Default Constructor
    *******************************************************************/
   template<class T>
   set<T> :: set()
   {
      try
      {
         this->numElements = 0;
         this->numCapacity = ALLOC_SIZE_STD;
         this->data = new T[this->numCapacity];
      }
      catch (std::bad_alloc)
      {
          throw "ERROR: Unable to allocate a new buffer for Set.";
      }
   }

   /******************************************************************
    * set :: set
    * Non-default Constructor
    *******************************************************************/
   template<class T>   
   set<T> :: set(int _numCapacity)
   {
      try
      {
         this->numElements = 0;
         this->numCapacity = numCapacity;
         this->data = new T[this->numCapacity];
      }
      catch (std::bad_alloc)
      {
          throw "ERROR: Unable to allocate a new buffer for Set.";
      }
   }

   /******************************************************************
    * set :: set
    * Copy Constructor
    *******************************************************************/
   template<class T>
   set<T> :: set(const set &rhs)
   {
      try
      {
         this->numElements = rhs.numElements;
         this->numCapacity = rhs.numCapacity;
         this->data = new T[rhs.numCapacity];

         // Copies data from current array
         // into the right hand side array 
         for (int i = 0; i < rhs.numCapacity; i++)
         {
            this->data[i] = rhs.data[i];
         }
      }
      catch (std::bad_alloc)
      {
          throw "ERROR: Unable to allocate a new buffer for Set.";
      }
   }

   /******************************************************************
    * set :: ~set
    * Destructor
    *******************************************************************/
   template<class T>
   set<T> :: ~set()
   {
      delete[] this->data;
      this->data = NULL;
   }

   /*******************************************************************
    * set :: operator =
    * Assignment Operator Overload
    * *****************************************************************/   
   template<class T>
   set<T> set<T> :: operator = (const set<T> &rhs)
   {
      try
      {
         if (this->numCapacity < rhs.size())
         {
            this->numCapacity = rhs.size();
            this->resize(rhs.size());
         }
         
         this->clear();

         for (int i = 0; i < rhs.size(); i++)
         {
            this->insert(rhs.data[i]);
         }
      }
      catch (std::bad_alloc)
      {
         throw "ERROR: Unable to allocate a new buffer for Set.";
      }

      return *this;
   } 

   /*******************************************************************
    * set :: operator ||
    * Union Operator Overload 
    * *****************************************************************/
   template<class T>
   set<T> set<T> :: operator || (const set<T> &rhs)
   {
      set<T> setReturn(*this);

      // Merges data from &rhs into current array
      for (int i = 0; i < rhs.numElements; i++)
      {
         setReturn.insert(rhs.data[i]);
      }

      return setReturn;
   }

   /*******************************************************************
    * set :: operator &&
    * Intersection Operator Overload 
    * *****************************************************************/ 
   template<class T>
   set<T> set<T> :: operator && (const set<T> &rhs)
   {
      set<T> setReturn;
      
      int iLhs = 0;
      int iRhs = 0;
      
      while ((iLhs < this->numElements) || (iRhs < rhs.numElements))
      {
         if ((iLhs == this->numElements) || (iRhs == rhs.numElements))
         {
            return setReturn;
         }
         // Only selects data if they exist at both arrays
         else if (this->data[iLhs] == rhs.data[iRhs])
         {
            setReturn.insert(this->data[iLhs]);
            iLhs++;
            iRhs++;
         } 
         else if (this->data[iLhs] < rhs.data[iRhs])
         {
            iLhs++;
         }      
         else
         {
            iRhs++;
         }
      }
   }

   /*******************************************************************
    * set :: operator -
    * Difference Operator Overload 
    * *****************************************************************/
   template<class T>
   set<T> set<T> :: operator - (const set<T> &rhs)
   {
      set<T> setReturn;

      for (size_t i = 0; i < this->size(); i++)
      {
         // Placeholder to flag a difference match
         bool _isFound = false;

         for (size_t j = 0; j < rhs.size(); j++)
         {
            if (rhs.data[j] == this->data[i])
            {
               // Flag a difference
               _isFound = true;
            }
         }

         // Only add the flagged difference
         // outside the inner for loop to avoid
         // errors and overwriting
         if (!_isFound)
         {
            setReturn.insert(this->data[i]);
         }
      }

      return setReturn;
   }

   /*******************************************************************
    * set :: resize
    * Allocates more memory as needed
    ********************************************************************/
   template<class T>
   void set <T> :: resize(int _numCapacity)
   {
      // calc capacity size
      int remainder = _numCapacity % ALLOC_SIZE_STD;
      int _capacity = 0;

      if (this->numCapacity >= ALLOC_SIZE_STD)
      {
         _capacity = this->numCapacity * 2;
      }
      else if (remainder == 0)
      {
         // keep _capacity the same
      }
      else
      {
         _capacity = _numCapacity + ALLOC_SIZE_STD - remainder;
      }

      try
      {
         T* _data = new T[_capacity]; // allocate new temporary data

         // move content from old data to new tmp data
         for (int i = 0; i < this->size(); i++)
         {
            _data[i] = this->data[i];
         }
         delete[] this->data;

         // initialize new data with more capacity
         this->data = _data;
         this->numCapacity = _capacity;
      }
      catch (std::bad_alloc)
      {
         throw "ERROR: Unable to allocate a new buffer for Set.";
      }
   }

   /*******************************************************************
    * set :: findIndex
    * Returns the index of an element. If the element isn't found, 
    * then it returns the index of the end.
    * *****************************************************************/
   template<class T>
   int set<T> :: findIndex(T t)
   {
      int _index = this->numElements;

      // Find where the item is in the array
      for (int i = 0; i < this->numElements; i++)
      {
         if (this->data[i] == t)
         {
            _index = i;
            break;
         }
         else if (this->data[i] > t)
         {
            i == 0 ? _index = 0 : _index = i;
            break;
         }
      }
      
      // If it still can't be found, return the index of the end
      return _index;
   }

   
   /*******************************************************************
    * set :: find
    * Retrieves an item in the Set and returns its iterator
    * *****************************************************************/
   template<class T>   
   typename set<T> :: iterator set<T> :: find(T t)
   {
      int iBegin = 0;
      int iEnd = this->numElements - 1;

      while (iBegin <= iEnd)
      {
         int iMiddle = (iBegin + iEnd) / 2;

         if (t == this->data[iMiddle])
         {
            return iterator(this->data[iMiddle]);
         }
         else if (t < this->data[iMiddle])
         {
            iEnd = iMiddle - 1;
         }
         else
         {
            iBegin = iMiddle + 1;
         }
      }

      return iterator(this->data[this->numElements]);
   }
   
   /*******************************************************************
    * set :: size
    * Returns the size of the Set
    * *****************************************************************/
   template<class T>
   int set<T> :: size() const
   {
      return this->numElements;
   }

   /*******************************************************************
    * set :: empty
    * Test whether the Set is empty
    * *****************************************************************/
   template<class T>   
   bool set<T> :: empty() const
   {
      if (this->numElements == 0)
      {
         return true;
      }
      
      return false;
   }

   /*******************************************************************
    * set :: clear
    * Empties the Set of all elements
    * *****************************************************************/
   template<class T>   
   void set<T> :: clear()
   {
      this->numElements = 0;
      this->data = new T[this->numCapacity];
   }

   /*******************************************************************
    * set :: insert
    * Inserts a new element to the Set
    * *****************************************************************/
   template<class T>   
   void set<T> :: insert(T t)
   {
      if (this->numElements + 1 == this->numCapacity)
      {
         this->resize(this->numCapacity * 2);
      }
      
      int iInsert = 0;

      if (this->numElements == 0)
      {
         this->data[0] = t;
         this->numElements++;
      }
      else
      {
         iInsert = findIndex(t);

         if (this->data[iInsert] != t)
         {
            for (int i = this->numElements; i >= iInsert; i--)
            {
               this->data[i + 1] = this->data[i];
            }
            this->data[iInsert] = t;
            this->numElements++;
         }
      }
   }

   /*******************************************************************
    * set :: erase
    * Deletes an element from the Set
    * *****************************************************************/
   template<class T>   
   typename set<T> :: iterator set<T> :: erase(iterator it)
   {
      T t = *it;

      int iErase = findIndex(t);

      if (this->data[iErase] == t)
      {
         for (int i = iErase; i < this->numElements; i++)
         {
            this->data[i] = this->data[i + 1];
         }
         this->numElements--;
      }
      
      return iterator(this->data[iErase]);
   }

   /*******************************************************************
    * set :: begin
    * Returns the start position iterator
    * *****************************************************************/
   template<class T>   
   typename set<T> :: iterator set<T> :: begin() const
   {
      return iterator(data[0]);
   }

   /*******************************************************************
    * set :: end
    * Returns the final position iterator
    * *****************************************************************/
   template<class T>   
   typename set<T> :: iterator set<T> :: end() const
   {
      return iterator(data[this->numElements]);
   }

   /*******************************************************************
    * set :: cbegin
    * Returns the start position const_iterator
    * *****************************************************************/
   template<class T>   
   typename set<T> :: const_iterator set<T> :: cbegin() const
   {
      return const_iterator(data[0]);
   }

   /*******************************************************************
    * set :: cend
    * Returns the final position const_iterator
    * *****************************************************************/
   template<class T>   
   typename set<T> :: const_iterator set<T> :: cend() const
   {
      return const_iterator(data[this->numElements]);
   }
}

#endif // SET_H