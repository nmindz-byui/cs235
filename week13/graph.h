/********************************************************
 * This file contains the definition of the graph class
 ********************************************************/

#ifndef GRAPH_H
#define GRAPH_H

#include "vertex.h"
#include "vector.h"
#include "set.h"
#include "queue.h"

class Graph
{
   private:
      bool * matrix;
      int numVert;
   public:
      //Non-default constructor
      Graph(int size);
      //Copy constructor
      Graph(const Graph & rhs);
      //Destructor
      ~Graph();
      //Assignment operator
      Graph operator = (const Graph & rhs);
      int size() const;
      void clear();
      bool isEdge(Vertex v1, Vertex v2) const;
      custom::set<Vertex> findEdges(Vertex v1) const;
      custom::vector<Vertex> findPath(Vertex source, Vertex destination) const;
      void add(Vertex v1, Vertex v2);
      void add(Vertex v1, custom::set<Vertex> destVert);
};

#endif