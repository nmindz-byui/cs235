/*********************************************************************
 * Implementation of the Graph class
 *********************************************************************/

#include "graph.h"


/*******************************************************************
 * Non-default constructor
 *******************************************************************/
Graph :: Graph(int size)
{
   // Allocate the memory
   this->matrix = new bool[size * size];
   // Set the size
   this->numVert = size;
   // Initialize every edge to false
   for (int i = 0; i < this->size() * this->size(); i++)
   {
      this->matrix[i] = false;
   }
}

/********************************************************************
 * Copy constructor
 ********************************************************************/
Graph :: Graph(const Graph & rhs)
{

   // Allocate new memory
   this->matrix = new bool[rhs.size() * rhs.size()];
   // Copy over the size
   this->numVert = rhs.size();
   // Copy over the edges
   for (int i = 0; i < this->size() * this->size(); i++)
   {
      this->matrix[i] = rhs.matrix[i];
   }
}

/*********************************************************************
 * Destructor
 *********************************************************************/
Graph :: ~Graph()
{
   delete matrix;
}

/*********************************************************************
 * Assignment operator overload
 *********************************************************************/
Graph Graph :: operator = (const Graph & rhs)
{
   // Allocate new memory
   this->matrix = new bool[rhs.size() * rhs.size()];
   // Copy over the size
   this->numVert = rhs.size();
   // Copy over the edges
   for (int i = 0; i < this->size() * this->size(); i++)
   {
      this->matrix[i] = rhs.matrix[i];
   }

   return *this;
}

/**********************************************************************
 * Size
 * returns the number of vertices of the graph
 **********************************************************************/
int Graph :: size() const
{
   return this->numVert;
}

/**********************************************************************
 * Clear
 * clears all edges from the vertices
 **********************************************************************/
void Graph :: clear()
{
   // Set every edge to false
   for (int i = 0; i < this->size() * this->size(); i++)
   {
      this->matrix[i] = false;
   }
}

/**********************************************************************
 * isEdge
 * Returns true if there is an edge between the two vertices
 **********************************************************************/
bool Graph :: isEdge(Vertex v1, Vertex v2) const
{
   int index = (v1.index() * this->numVert) + v2.index();
   return matrix[index];
}

/**********************************************************************
 * findEdges
 * Returns a set of edges coming from a vertex
 **********************************************************************/
custom::set<Vertex> Graph :: findEdges(Vertex v1) const
{
   custom::set<Vertex> destVert; // Destination vertices
   for (int i = 0; i < this->numVert; i++)
   {
      int index = (v1.index() * this->numVert) + i;
      if (matrix[index])
      {
         destVert.insert(Vertex(i));
      }
   }

   return destVert;
}

/**********************************************************************
 * findPath
 * Determines the shortest path between two vertices and returns a 
 * vector of vertices representing the path
 **********************************************************************/
custom::vector<Vertex> Graph :: findPath(Vertex source, Vertex destination) const
{  
    int distance = 0;
    custom::vector<int> distances;
    custom::queue<Vertex> toVisit;
    custom::vector<Vertex> predecessor(this->numVert);
    toVisit.push(source);
    //Put -1 in every slot in the vector
    for (int i = 0; i < this->numVert; i++)
    {
       distances.push_back(-1);
    }
   
   //Loop until the path has been found
   while (!toVisit.empty() && distances[destination.index()] == -1)
    {
       Vertex v = toVisit.front();
       toVisit.pop();
      if (distances[v.index()] > distance)
       {
          distance++;
       }
      custom::set<Vertex> s = this->findEdges(v);
       custom::set<Vertex>::iterator it = s.begin();
       for (it; it != s.end(); ++it)
       {
          if (distances[(*it).index()] == -1)
          {
             distances[(*it).index()] = distance + 1;
             predecessor[(*it).index()] = v;
             toVisit.push(*it);
          }
          distance++;
       }
    }
   custom::vector<Vertex> path;
    path.push_back(destination);
    for (int i = 1; i < distance; i++)
    {
       path.push_back(predecessor[path[i-1].index()]);
    }
    return path;
}


/**********************************************************************
 * Add
 * Adds an edge from one vertex to another
 **********************************************************************/
void Graph :: add(Vertex v1, Vertex v2)
{
   int index = (v1.index() * this->numVert) + v2.index();
   matrix[index] = true;
}

/**********************************************************************
 * Add
 * Adds edges from the source vertex to the vertexes in the set
 **********************************************************************/
void Graph :: add(Vertex v1, custom::set<Vertex> destVert)
{
   custom::set<Vertex>::iterator it;
   for (it = destVert.begin(); it != destVert.end(); ++it)
   {
      int index = (v1.index() * this->numVert) + (*it).index();
      matrix[index] = true;
   }
}