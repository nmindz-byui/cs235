// This program will read a file and give specs on the words found

#include <iostream>
#include <fstream>
#include <string>
#include <list>

using namespace std;

class wordLength
{
   public:
      int sum;
      int occurences;
      wordLength() : sum(0), occurences(0) {};
      wordLength(int sum) : sum(sum), occurences(1) {};
};

int getSum(string word)
{
   int sum = 0;
   for (int i = 0; i < word.size(); i++)
   {
      sum += word[i]; // Get the sum of the ASCII values of the string
   }
   return sum;
}
int getBucket(int sum, int capacity)
{
   int index = sum % capacity;
   return index;
}

int main()
{
   string word;
   list<wordLength> wordSpecs;

   ifstream fin("week12/dictionary.txt");

   while(!fin.eof())
   {
      fin >> word;
      if (wordSpecs.size() == 0)
      {
         wordSpecs.push_back(wordLength(getSum(word)));
      }
      else
      {
         bool found = false;
         list<wordLength>::iterator it = wordSpecs.begin();
         for (it; it != wordSpecs.end(); ++it)
         {
            if (getSum(word) == (*it).sum)
            {
               (*it).occurences += 1;
               found = true;
               break;
            }
         }
         if (!found)
         {
            wordSpecs.push_back(wordLength(getSum(word)));
         }
      }
   }

   cout << wordSpecs.size();

   list<wordLength>::iterator it = wordSpecs.begin();
   for (it; it != wordSpecs.end(); ++it)
   {
      cout << "Sum: " << (*it).sum << endl
            << "Occurences: " << (*it).occurences << endl << endl;
   }

   for (int i = 1; i < 100; i++)
   {
      list<int> buckets;
      list<wordLength>::iterator it = wordSpecs.begin();
      for (it; it != wordSpecs.end(); ++it)
      {
         buckets.push_back(getBucket((*it).sum, i));
      }

      
      cout << "With " << i << " buckets:" << endl;
      for (int iBucket = 1; iBucket <= i; iBucket++)
      {
         int count = 0;
         list<int>::iterator intIt = buckets.begin();
         for (intIt; intIt != buckets.end(); ++intIt)
         {
            if (*intIt == iBucket)
               count++;
         }
         cout << "Bucket " << iBucket << " has " << count << " elements.\n";
      }
     
   }

}