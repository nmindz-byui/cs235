/***********************************************************************
 * Module:
 *    Assignment 12, Spell Check
 *    Brother Kirby, CS 235
 * Author:
 *    Caleb Georgeson, Evandro Camargo, Alexandre Paixao
 * Summary:
 *    This program will implement the spellCheck() function
 ************************************************************************/

#include <iostream>
#include <fstream>
#include <algorithm>
#include <cctype>
#include <string>
#include "spellCheck.h"
#include "hash.h"
#include "queue.h"

using namespace std;

/************************************************************************************
 * strToLower
 * A simple, just for means of this assignment, way of replacing
 * uppercase letters in a word with their lowercase equivalent.
 * By no means comprehensive and/or meant to deal with anything
 * out of the ASCII range of regular alphabetic characters.
 * 
 * Implemented based on an answer from StackOverflow:
 * https://stackoverflow.com/questions/313970/how-to-convert-stdstring-to-lower-case
 * 
 ***********************************************************************************/
std::string strToLower(std::string s)
{
   std::string _s = s;
   std::transform(_s.begin(), _s.end(), _s.begin(),
    [](unsigned char c){ return std::tolower(c); });

   return _s;
}

/******************************************************************
 * inititializeHash
 * Reads a dictionary from a file and inserts it into the hash
 *****************************************************************/
void initializeHash(SHash & dictionary)
{
   std::ifstream fin("/home/cs235/week12/dictionary.txt");
   //std::ifstream fin("dictionary.txt");

   if (!fin.fail()) 
   {
      while (!fin.eof())
      {
         std::string word;
         fin >> word;

         // Just to make sure, store all words as lowercase
         dictionary.insert(strToLower(word));
      }
   }

}

/******************************************************************
 * spellCheck
 * Prompt the user for a file to spell-check
 *****************************************************************/
void spellCheck()
{
   /*********************************************
   * Our Hash for spell checking was given
   * the capacity of 99. We created a test
   * function that simulated the implementation
   * of a hash, counting how many of the words
   * in the dictionary would go into each bucket.
   * It tested what the hash would look like
   * from having 1 bucket to 99 buckets. In all
   * of the possible number of buckets, the hash 
   * stayed relatively balanced. Having more buckets
   * caused the buckets to have less collisions,
   * thus causing the hash to be faster. A hash should
   * be able to return values in constant O(1) time.
   * With the list class, that time is changed to O(n),
   * where n is the number of elements in the bucket.
   * Having less elements in each bucket allows the 
   * hash to be faster.
   *********************************************/

   SHash _dictionary(99);
   SHash _duplicates(99);
   custom::queue<std::string> _misspelled;
   
   initializeHash(_dictionary);

   std::string fileName;
   std::cout << "What file do you want to check? ";
   std::cin >> fileName;

   // Read the file
   std::ifstream fin(fileName);

   // Unless we find an error
   if (!fin.fail())
   {
      // And if we're good, loop until the end (of the file)
      while (!fin.eof())
      {
         // The extracted word
         std::string word;
         fin >> word;

         // Have a lowercase copy of the word for comparisons
         std::string _lword = strToLower(word);

         // Check if the word is in the dictionary
         if (!_dictionary.find(_lword))
         {
            // Check if not duplicate
            if (!_duplicates.find(_lword))
            {
               // If it is not in the dictionary
               // and not duplicate, place it in _misspelled
               // and save the lowercase version in _duplicates
               _misspelled.push(word);
               _duplicates.insert(_lword);
            }
         }
      }
   }

   // If the file has no errors, display and exit
   if (_misspelled.empty())
   {
      std::cout << "File contains no spelling errors\n";
      return;
   }

   // Misspelled words were found, display and exit
   std::cout << "Misspelled: ";
   for (_misspelled; !_misspelled.empty(); _misspelled.pop())
   {
      // Print the duplicate. Yay! :)
      std::cout << _misspelled.front();

      // If we have enough words, place a comma
      if (_misspelled.size() > 1)
      {
         std::cout << ", ";
      }
   }

   std::cout << "\n";
}