/***********************************************************************
 * Module:
 *    Week 12, Hash
 *    Brother Kirby, CS 235
 * Author:
 *    Caleb Georgeson, Evandro Camargo, Alexandre Paixao
 * Summary:
 *    This program will implement the Hash
 ************************************************************************/

#ifndef HASH_H
#define HASH_H

#include "list.h"
#include <cassert>

/*****************************************************
 * HASH
 ****************************************************/
template<class T>
class Hash
{
   private:
      custom::list<T> * table;
      int numElements;
      int numBuckets;

   public:
      Hash(int numBuckets);
      Hash(const Hash & rhs);
      ~Hash();
      Hash assignment(Hash * rhs);
      int size() const;
      int capacity() const;
      void clear();
      bool empty() const;
      bool find(T t) const;
      void insert(T t);
      virtual int hash(T t) const { return 0; }
};

/*****************************************************
 * HASH
 * Non-default constructor
 *****************************************************/
template<class T>
Hash <T> :: Hash(int numBuckets)
{
   this->numBuckets = numBuckets;
   this->numElements = 0;
   this->table = new custom::list<T>[numBuckets];
}

/*****************************************************
 * HASH
 * Copy constructor
 *****************************************************/
template<class T>
Hash <T> :: Hash(const Hash & rhs)
{
   this->numBuckets = rhs.numBuckets;
   this->numElements = rhs.numElements;
   this->table = rhs.table;
}

/*****************************************************
 * ~HASH
 * Destructor
 *****************************************************/
template<class T>
Hash <T> :: ~Hash()
{
   for (int i; i < numBuckets; i++)
   {
      table[i].clear();
   }
}

/*****************************************************
 * HASH :: SIZE
 * Returns the number of elements in the hash table
 *****************************************************/
template<class T>
int Hash <T> :: size() const
{
   return this->numElements;
}

/*****************************************************
 * HASH :: CAPACITY
 * Returns the capacity of the hash table
 *****************************************************/
template<class T>
int Hash <T> :: capacity() const
{
   return this->numBuckets;
}

/*****************************************************
 * HASH :: CLEAR
 * Clears all of the elements from the hash table
 *****************************************************/
template<class T>
void Hash <T> :: clear()
{
   for (int i = 0; i < numBuckets; i++)
   {
      // Clear each element
      this->table[i].clear();
   }
}

/*****************************************************
 * HASH :: EMPTY
 * Returns true if the hash table is empty
 *****************************************************/
template<class T>
bool Hash <T> :: empty() const
{
   return this->numElements == 0;
}

/*****************************************************
 * HASH :: FIND
 * Returns true if the passed value is found in the 
 * hash table
 *****************************************************/
template<class T>
bool Hash <T> :: find(T t) const
{
   // Determine which bucket the value should be in
   int i = hash(t);

   // NULL iterator
   typename custom::list<T>::iterator nullIt;

   if (this->table[i].find(t) == nullIt)
   {
      // The value is not found in the hash
      return false;
   } 
   else
   {
      // The value is found in the hash
      return true;
   }
}

/*****************************************************
 * HASH :: INSERT
 * Returns the number of elements in the hash table
 *****************************************************/
template<class T>
void Hash <T> :: insert(T t)
{
   // Determine which bucket to deposit the value
   int i = hash(t);

   // Deposit the value into the bucket
   this->table[i].push_back(t);

   numElements++;
}

#endif // HASH_H