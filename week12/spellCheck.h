/***********************************************************************
 * Module:
 *    Assignment 12, Spell Check
 *    Brother Kirby, CS 235
 * Author:
 *    Caleb Georgeson, Evandro Camargo, Alexandre Paixao
 * Summary:
 *    This program will implement the spellCheck() function
 ************************************************************************/

#ifndef SPELL_CHECK_H
#define SPELL_CHECK_H

#include <string>
#include "hash.h"

class SHash : public Hash <std::string>
{
   public:
      SHash(int numBuckets) : Hash <std::string> (numBuckets) {};
      SHash(const Hash<std::string> & rhs) : Hash <std::string> (rhs) {};

      virtual int hash(const std::string &word)
      {
         int sum = 0;
         for (int i = 0; i < word.size(); i++)
         {
            sum += word[i]; // Get the sum of the ASCII values of the string
         }

         int index = sum % capacity();
         return index;
      }
};

std::string strToLower(std::string s);
void initializeHash(Hash<std::string> dictionary);
void spellCheck();

#endif // SPELL_CHECK_H
